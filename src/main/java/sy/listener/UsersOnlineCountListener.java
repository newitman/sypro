package sy.listener;

import sy.pageModel.User;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import java.util.*;

/**
 * Created by thinkpad on 14-5-7.用户登陆监听
 */
public class UsersOnlineCountListener implements HttpSessionBindingListener {

    private User user;

    public UsersOnlineCountListener(User user){
        this.user=user;
    }
    public void valueBound(HttpSessionBindingEvent arg0) {
        HttpSession session=arg0.getSession();
        ServletContext context=session.getServletContext();
        List<User> onlineUserList=(List)context.getAttribute("onlineList");
        if(onlineUserList==null){
            onlineUserList=new ArrayList<User>();

        }
        onlineUserList.add(this.user);
        context.setAttribute("onlineList",onlineUserList);

    }
    public void valueUnbound(HttpSessionBindingEvent arg0) {

        HttpSession session=arg0.getSession();
        ServletContext context=session.getServletContext();
        List<User> onlineUserList=(List)context.getAttribute("onlineList");
        onlineUserList.remove(this.user);
        context.setAttribute("onlineList",onlineUserList);
    }
}
