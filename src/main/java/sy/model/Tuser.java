package sy.model;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "tuser")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tuser implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private Date	    createdatetime;
	private Date 	    modifydatetime;
	private String      name;
	private String 	    pwd;
	private Integer     totalscore;
	private Integer     changedscore;
	private Integer     unchangedscore;
	private Integer     taskcount;
	private Integer     changedcount;
	private Integer     invitecount;
	private Integer     scorefrominvite;
	private String 	    phonenumber;
	private String 	    qq;
	private String 	    alipay;
	private Integer		levl;
	private String 	   statu;
	  
	private Set<Trole> troles = new HashSet<Trole>(0);
	
	//用户能使用的刮刮奖次数
	private int ggjnunber;
	
	@Column(name = "levl",  length = 50)
	public Integer getLevl() {
		return levl;
	}

	public void setLevl(Integer levl) {
		this.levl = levl;
	}
	@Column(name = "totalscore",  length = 50)
	public Integer getTotalscore() {
		return totalscore;
	}
	
	public int getGgjnunber() {
		return ggjnunber;
	}

	public void setGgjnunber(int ggjnunber) {
		this.ggjnunber = ggjnunber;
	}

	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}
	
	@Column(name = "userid",  length = 50)
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	
	@Column(name = "changedscore",  length = 50)
	public Integer getChangedscore() {
		return changedscore;
	}

	public void setChangedscore(Integer changedscore) {
		this.changedscore = changedscore;
	}
	@Column(name = "unchangedscore",  length = 50)
	public Integer getUnchangedscore() {
		return unchangedscore;
	}

	public void setUnchangedscore(Integer unchangedscore) {
		this.unchangedscore = unchangedscore;
	}
	@Column(name = "taskcount",  length = 50)
	public Integer getTaskcount() {
		return taskcount;
	}

	public void setTaskcount(Integer taskcount) {
		this.taskcount = taskcount;
	}
	@Column(name = "changedcount",  length = 50)
	public Integer getChangedcount() {
		return changedcount;
	}

	public void setChangedcount(Integer changedcount) {
		this.changedcount = changedcount;
	}
	@Column(name = "invitecount",  length = 50)
	public Integer getInvitecount() {
		return invitecount;
	}

	public void setInvitecount(Integer invitecount) {
		this.invitecount = invitecount;
	}
	@Column(name = "scorefrominvite",  length = 50)
	public Integer getScorefrominvite() {
		return scorefrominvite;
	}

	public void setScorefrominvite(Integer scorefrominvite) {
		this.scorefrominvite = scorefrominvite;
	}
	@Column(name = "phonenumber",  length = 50)
	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	@Column(name = "qq",  length = 50)
	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
	@Column(name = "alipay",  length = 50)
	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	
	

	public Tuser() {
	}

	public Tuser(String id, String name, String pwd) {
		this.id = id;
		this.name = name;
		this.pwd = pwd;
	}

	public Tuser(String id, Date createdatetime, Date modifydatetime, String name, String pwd, Set<Trole> troles) {
		this.id = id;
		this.createdatetime = createdatetime;
		this.modifydatetime = modifydatetime;
		this.name = name;
		this.pwd = pwd;
		this.troles = troles;
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdatetime", length = 19)
	public Date getCreatedatetime() {
		return this.createdatetime;
	}
	public void setCreatedatetime(Date createdatetime) {
		this.createdatetime = createdatetime;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifydatetime", length = 19)
	public Date getModifydatetime() {
		return this.modifydatetime;
	}

	public void setModifydatetime(Date modifydatetime) {
		this.modifydatetime = modifydatetime;
	}

	@Column(name = "name", unique = true, nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "pwd", nullable = false, length = 100)
	public String getPwd() {
		return this.pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	@Column(name = "statu",  length = 50)
    public String getStatu() {
		return statu;
	}

	public void setStatu(String statu) {
		this.statu = statu;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tuser_trole", joinColumns = { @JoinColumn(name = "TUSER_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "TROLE_ID", nullable = false, updatable = false) })
	public Set<Trole> getTroles() {
		return this.troles;
	}
	public void setTroles(Set<Trole> troles) {
		this.troles = troles;
	}
	public Tuser(String id, Long userid, Date createdatetime,
			Date modifydatetime, String name, String pwd, Integer totalscore,
			Integer changedscore, Integer unchangedscore, Integer taskcount,
			Integer changedcount, Integer invitecount, Integer scorefrominvite,
			String phonenumber, String qq, String alipay, Set<Trole> troles,Integer levl,
			String statu) {
		super();
		this.id = id;
		this.userid = userid;
		this.createdatetime = createdatetime;
		this.modifydatetime = modifydatetime;
		this.name = name;
		this.pwd = pwd;
		this.totalscore = totalscore;
		this.changedscore = changedscore;
		this.unchangedscore = unchangedscore;
		this.taskcount = taskcount;
		this.changedcount = changedcount;
		this.invitecount = invitecount;
		this.scorefrominvite = scorefrominvite;
		this.phonenumber = phonenumber;
		this.qq = qq;
		this.alipay = alipay;
		this.troles = troles;
		this.levl = levl;
		this.statu = statu;
	}
}
