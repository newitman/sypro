package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "taskrecord")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Taskrecord implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private String	    taskType;
	private Date 	    finishTime;
	private Integer     score;
	private Integer		giflev;
	private String		displaytime;
	private Date		finishDate;
	
	@Column(name = "finishdate",length = 19)
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "displaytime",  length = 20)
	public String getDisplaytime() {
		return displaytime;
	}
	public void setDisplaytime(String displaytime) {
		this.displaytime = displaytime;
	}
	
	
	
	@Column(name = "giflev",  length = 10)
	public Integer getGiflev() {
		return giflev;
	}
	public void setGiflev(Integer giflev) {
		this.giflev = giflev;
	}
	@Column(name = "userid",  length = 50)
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	@Column(name = "tasktype",  length = 100)
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	@Column(name = "finishtime",length = 19)
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Taskrecord() {
		super();
	}
	public Taskrecord(String id, Long userid, String taskType, Date finishTime,
			Integer score,Integer giflev,String displaytime) {
		super();
		this.id = id;
		this.userid = userid;
		this.taskType = taskType;
		this.finishTime = finishTime;
		this.score = score;
		this.giflev = giflev;
		this.displaytime = displaytime;
	}
}
