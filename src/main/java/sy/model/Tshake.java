package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tshake")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tshake implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private Integer		score;
	private Date	    time;
	public Tshake() {
		super();
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	
	@Column(name = "userid")
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	
	@Column(name = "time",length = 19)
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	public Tshake(String id, Long userid, Integer score, Date time) {
		super();
		this.id = id;
		this.userid = userid;
		this.score = score;
		this.time = time;
	}
}
