package sy.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name = "tappinfo")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tappinfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 100)
	private String appAddress;//app链接地址
	@Column(length = 36)
	private String appname;//app名字
	@Column(length = 1000)
	private String description;//app描述
	private String integral;//app积分
	@Column(length = 36)
	private String createtime;//创建时间
    @Column(length = 100)
    private String imageurl;//logo 地址

    @Column(length = 100)
    private String version;//版本号

    @Column(length = 500)
    private String detailaddress;//详细页

    public String getDetailaddress() {
        return detailaddress;
    }

    public void setDetailaddress(String detailaddress) {
        this.detailaddress = detailaddress;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Column(length = 100)
    private String appsize;//app 大小

    public String getAppsize() {
        return appsize;
    }

    public void setAppsize(String appsize) {
        this.appsize = appsize;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAppAddress() {
		return appAddress;
	}
	public void setAppAddress(String appAddress) {
		this.appAddress = appAddress;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIntegral() {
		return integral;
	}
	public void setIntegral(String integral) {
		this.integral = integral;
	}


	
}
