package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tasktype")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tasktype implements java.io.Serializable {
	private String 		id;
	private String		typeid;
	private String 	    typecontent;
	private Integer		score;
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "typeid",length = 36)
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	@Column(name = "typecontent",  length = 100)
	public String getTypecontent() {
		return typecontent;
	}
	public void setTypecontent(String typecontent) {
		this.typecontent = typecontent;
	}
	
	public Tasktype() {
		super();
	}
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	
	public Tasktype(String id, String typeid, String typecontent,Integer score) {
		super();
		this.id = id;
		this.typeid = typeid;
		this.typecontent = typecontent;
		this.score = score;
	}

}
