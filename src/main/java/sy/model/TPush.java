package sy.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tpush")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TPush implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 100)
	private String linkAddress;//推送消息内容文件

    @Column( length = 3000)
    private String content;//推送消息内容不包括html结构

	@Column(length = 100)
	private String publistime;//发布时间
    @Column(length = 100)
    private String serchstarttime;//搜索开始时间

    @Column(length = 100)
    private String serchendtime;//搜索结束时间

    @Column(length = 100)
    private String title;//标题

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public String getPublistime() {
        return publistime;
    }

    public void setPublistime(String publistime) {
        this.publistime = publistime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSerchstarttime() {
        return serchstarttime;
    }

    public void setSerchstarttime(String serchstarttime) {
        this.serchstarttime = serchstarttime;
    }

    public String getSerchendtime() {
        return serchendtime;
    }

    public void setSerchendtime(String serchendtime) {
        this.serchendtime = serchendtime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
