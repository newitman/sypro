package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tapp")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tappcheck implements java.io.Serializable {
	private String 		id;
	private String		voish;
	private String      linkadress;
	private Date	    publishtime;
	public Tappcheck() {
		super();
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "voish")
	public String getVoish() {
		return voish;
	}
	public void setVoish(String voish) {
		this.voish = voish;
	}
	@Column(name = "linkadress")
	public String getLinkadress() {
		return linkadress;
	}
	public void setLinkadress(String linkadress) {
		this.linkadress = linkadress;
	}
	@Column(name = "publishtime")
	public Date getPublishtime() {
		return publishtime;
	}
	public void setPublishtime(Date publishtime) {
		this.publishtime = publishtime;
	}
	public Tappcheck(String id, String voish, String linkadress,
			Date publishtime) {
		super();
		this.id = id;
		this.voish = voish;
		this.linkadress = linkadress;
		this.publishtime = publishtime;
	}
}
