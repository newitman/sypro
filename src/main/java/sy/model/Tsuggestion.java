package sy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tsuggestion")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tsuggestion implements Serializable {

	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 1000)
	private String parm;
	@Column(length = 1000)
	private String sug;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParm() {
		return parm;
	}
	public void setParm(String parm) {
		this.parm = parm;
	}
	public String getSug() {
		return sug;
	}
	public void setSug(String sug) {
		this.sug = sug;
	}
	
	
}
