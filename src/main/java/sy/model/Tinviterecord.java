package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tinviterecord")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tinviterecord implements java.io.Serializable {
	private String 		id;
	private String		userid;
	private String		inviteuserid;
	private Integer     score;
	private Integer     realscore;
	private String 		imei;
	private String		nickname;
	private String		registertime;
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getRegistertime() {
		return registertime;
	}
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}
	public Tinviterecord() {
		super();
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "imei")
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	@Column(name = "userid")
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	@Column(name = "inviteuserid")
	public String getInviteuserid() {
		return inviteuserid;
	}
	public void setInviteuserid(String inviteuserid) {
		this.inviteuserid = inviteuserid;
	}
	
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	@Column(name = "realscore")
	public Integer getRealscore() {
		return realscore;
	}
	public void setRealscore(Integer realscore) {
		this.realscore = realscore;
	}
	public Tinviterecord(String id, String userid, String inviteuserid,
			 Integer score, Integer realscore,String imei,String nickname,String registertime) {
		super();
		this.imei = imei;
		this.id = id;
		this.userid = userid;
		this.inviteuserid = inviteuserid;
		this.score = score;
		this.realscore = realscore;
		this.nickname = nickname;
		this.registertime = registertime;
	}
}
