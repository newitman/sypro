package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tcontact")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tcontact implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private String	    content;
	private Date		contime;
	@Column(name = "contime",length=19)
	public Date getContime() {
		return contime;
	}
	public void setContime(Date contime) {
		this.contime = contime;
	}
	@Column(name = "content",length=200)
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Tcontact() {
		super();
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	 
	
	@Column(name = "userid")
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	
	public Tcontact(String id, Long userid, String content,Date contime) {
		super();
		this.id = id;
		this.userid = userid;
		this.content = content;
		this.contime = contime;
	}
	 
}
