package sy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tcomment")
@DynamicInsert(true)
@DynamicUpdate(true)
//互动广场评论
public class TComment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 36)
	private String userid;//评论人id
	@Column(length = 36)
	private String iid;//评论这条信息的id
	private int islike;//0:未评论 1:喜欢
	@Column(length=36)
	private String comenttime;//评论时间
	
	@Column(length=500)
	private String commentmsg;//评论信息
	
	@Column(length=50)
	private String username;//评论人 姓名
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCommentmsg() {
		return commentmsg;
	}
	public void setCommentmsg(String commentmsg) {
		this.commentmsg = commentmsg;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	public int getIslike() {
		return islike;
	}
	public void setIslike(int islike) {
		this.islike = islike;
	}
	public String getComenttime() {
		return comenttime;
	}
	public void setComenttime(String comenttime) {
		this.comenttime = comenttime;
	}
	
	
}
