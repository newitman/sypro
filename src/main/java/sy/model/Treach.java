package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "treach")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Treach implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private Date	    reachTime;
	private Integer		score;
	public Treach() {
		super();
	}
	
	@Column(name = "score" )
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "userid")
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	
	@Column(name = "reachtime",length = 19)
	public Date getReachtime() {
		return reachTime;
	}
	public void setreachtime(Date reachTime) {
		this.reachTime = reachTime;
	}
	public Treach(String id, Long userid, Date reachTime,Integer score) {
		super();
		this.id = id;
		this.userid = userid;
		this.reachTime = reachTime;
		this.score = score;
	}

	
}
