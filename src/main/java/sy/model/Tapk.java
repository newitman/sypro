package sy.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tapk")//上传apk版本升级
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tapk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 100)
	private String filename;//apk文件名

    @Column( length = 100)
    private String createtiem;//文件上传时间

	@Column(length = 20)
	private String version;//apk版本

    @Column(length = 100)
    private String filepath;//apk上传路劲

    @Column(length = 1000)
    private String bugcontent;//修复bug内容信息

    public String getBugcontent() {
        return bugcontent;
    }

    public void setBugcontent(String bugcontent) {
        this.bugcontent = bugcontent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCreatetiem() {
        return createtiem;
    }

    public void setCreatetiem(String createtiem) {
        this.createtiem = createtiem;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }
}
