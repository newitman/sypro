package sy.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "texchangetype")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Texchangetype implements java.io.Serializable {
	private String 		id;
	private String		typeid;
	private String 	    typecontent;
	private String 		unit;
	
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "unit",length = 10)
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	@Column(name = "typeid",length = 36)
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	@Column(name = "typecontent",  length = 100)
	public String getTypecontent() {
		return typecontent;
	}
	public void setTypecontent(String typecontent) {
		this.typecontent = typecontent;
	}
	
	public Texchangetype() {
		super();
	}
	
	public Texchangetype(String id, String typeid, String typecontent,String unit) {
		super();
		this.id = id;
		this.typeid = typeid;
		this.typecontent = typecontent;
		this.unit = unit;
	}

}
