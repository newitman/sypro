package sy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tquiz")
@DynamicInsert(true)
@DynamicUpdate(true)
//竞猜
public class TQuiz implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 36)
	private String userid;//用户id
	@Column(length = 1000)
	private String number;//竞猜号
	@Column( length = 19)
	private String createdatetime; 
	@Column(length = 36)
	private String periodnumber;//竞猜期号
	private int status;//中奖状态 0：为中奖 1：中奖 -1:未开奖
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPeriodnumber() {
		return periodnumber;
	}
	public void setPeriodnumber(String periodnumber) {
		this.periodnumber = periodnumber;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}

	public String getCreatedatetime() {
		return createdatetime;
	}
	public void setCreatedatetime(String createdatetime) {
		this.createdatetime = createdatetime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
