package sy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tquizmanager")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TQuizManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 30)
	private String starttime;//竞猜开始时间
	
	@Column( length = 30)
	private String endtime;//竞猜日期 用来存放当前竞猜期
	
	@Column(length = 36)
	private String userid;
	
	@Column(length = 36)
	private String periodnumber;//竞猜期号
	
	@Column(length = 20)
	private String number;//中奖号码
	@Column(length = 100)
	private String gold;//金币
	@Column( length = 30)
	private String createdatetime;//创建时间
	public String getGold() {
		return gold;
	}

	public String getCreatedatetime() {
		return createdatetime;
	}


	public void setCreatedatetime(String createdatetime) {
		this.createdatetime = createdatetime;
	}


	public void setGold(String gold) {
		this.gold = gold;
	}


	public String getUserid() {
		return userid;
	}
	
	
	public String getPeriodnumber() {
		return periodnumber;
	}
	public void setPeriodnumber(String periodnumber) {
		this.periodnumber = periodnumber;
	}


	//状态
	private int status;//0:开始 1:结束
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	

	
}
