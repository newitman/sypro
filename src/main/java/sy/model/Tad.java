package sy.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name = "tad")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 100)
	private String linkAddress;//图片链接文件

    @Column( length = 3000)
    private String content;//文件内容 不包括html结构

	@Column(length = 1000)
	private String imagePath;//幻灯片广告图片
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLinkAddress() {
		return linkAddress;
	}
	public void setLinkAddress(String linkAddress) {
		this.linkAddress = linkAddress;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
