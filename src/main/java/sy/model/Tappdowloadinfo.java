package sy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tappdowloadinfo")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tappdowloadinfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 1000)
	private String userid;//用戶id
	@Column(length = 1000)
	private String downloadtime;//下載時間
	@Column(length=36)
	private String appid;//appid
	
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getDownloadtime() {
		return downloadtime;
	}
	public void setDownloadtime(String downloadtime) {
		this.downloadtime = downloadtime;
	}
	
	
	
}
