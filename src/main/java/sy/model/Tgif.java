package sy.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tgif")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tgif implements java.io.Serializable {
	private String 		id;
	private Integer		lev;
	private Integer		score;
 
	public Tgif() {
		super();
	}
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	
	@Column(name = "lev")
	public Integer getLev() {
		return lev;
	}
	public void setLev(Integer lev) {
		this.lev = lev;
	}
	public Tgif(String id, Integer lev, Integer score) {
		super();
		this.id = id;
		this.lev = lev;
		this.score = score;
	}
}
