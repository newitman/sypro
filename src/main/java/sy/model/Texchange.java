package sy.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "texchange")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Texchange implements java.io.Serializable {
	private String 		id;
	private String		userid;
	private String	    type;
	private String 	    qq;
	private String      cellPhone;
	private Integer     goods;
	private String 		alipay;
	private Date 		exchangetime;
	private String 		exchangecontent;
	private String 		nickname;
	private Integer		score;
	private String		displaytime;
	private String 		changestatus;
	
	private String 		nicknamecp;
	private String 		useridcp;
	
	@Column(name = "useridcp")
	public String getUseridcp() {
		return useridcp;
	}

	public void setUseridcp(String useridcp) {
		this.useridcp = useridcp;
	}
	@Column(name = "nicknamecp")
	public String getNicknamecp() {
		return nicknamecp;
	}
	public void setNicknamecp(String nicknamecp) {
		this.nicknamecp = nicknamecp;
	}

	public Texchange() {
		super();
	}
	

	
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "displaytime",  length = 20)
	public String getDisplaytime() {
		return displaytime;
	}
	public void setDisplaytime(String displaytime) {
		this.displaytime = displaytime;
	}
	
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}

	
	@Column(name = "exchangetime")
	public Date getExchangetime() {
		return exchangetime;
	}

	public void setExchangetime(Date exchangetime) {
		this.exchangetime = exchangetime;
	}

	@Column(name = "changestatus")
	public String getChangestatus() {
		return changestatus;
	}
	public void setChangestatus(String changestatus) {
		this.changestatus = changestatus;
	}
	
	@Column(name = "nickname")
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	@Column(name = "exchangecontent")
	public String getExchangecontent() {
		return exchangecontent;
	}

	public void setExchangecontent(String exchangecontent) {
		this.exchangecontent = exchangecontent;
	}
	@Column(name = "userid")
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	@Column(name = "type", length = 50)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(name = "qq", length = 50)
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	@Column(name = "cellPhone", length = 50)
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	@Column(name = "goods", length = 50)
	public Integer getGoods() {
		return goods;
	}
	public void setGoods(Integer goods) {
		this.goods = goods;
	}
	@Column(name = "alipay", length = 50)
	public String getAlipay() {
		return alipay;
	}
	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}
	public Texchange(String id, String userid, String type, String qq,
			String cellPhone, Integer goods, String alipay,Date exchangetime,String exchangecontent,Integer score,String displaytime) {
		super();
		this.id = id;
		this.userid = userid;
		this.type = type;
		this.qq = qq;
		this.cellPhone = cellPhone;
		this.goods = goods;
		this.score = score;
		this.alipay = alipay;
		this.exchangetime = exchangetime;
		this.exchangecontent = exchangecontent;
		this.displaytime = displaytime;
	}
	
	
	
}
