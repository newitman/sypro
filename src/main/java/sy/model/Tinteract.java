package sy.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Entity
@Table(name = "tinteract")
@DynamicInsert(true)
@DynamicUpdate(true)
//互动广场实体
public class Tinteract implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7569956229755959853L;
	@Id
	@Column(length = 36)
	private String id;
	@Column( length = 5000)
	private String content;//内容
	@Column(length = 36)
	private String publishtime;//发布时间
	@Column(length=100)//标题
	private String title;
	@Column(length=1000)
	private String imageurl;

    @Transient
    private List<TComment> comments;//对这条互动广场信息评论
    public  List<TComment> getComments() {
        return comments;
    }
    public void setComments( List<TComment> comments) {
        this.comments = comments;
    }

    @Transient
	private long likecount;//喜欢总数
	@Transient
	private long commentcout;//评论总数
    @Transient
    private int islike;//是否喜欢
    public int getIslike() {
        return islike;
    }
    public void setIslike(int islike) {
        this.islike = islike;
    }
	
	@Transient
	private long cout;//总共多少条
	
	public long getCout() {
		return cout;
	}
	public void setCout(long cout) {
		this.cout = cout;
	}
	public long getLikecount() {
		return likecount;
	}
	public void setLikecount(long likecount) {
		this.likecount = likecount;
	}
	public long getCommentcout() {
		return commentcout;
	}
	public void setCommentcout(long commentcout) {
		this.commentcout = commentcout;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPublishtime() {
		return publishtime;
	}
	public void setPublishtime(String publishtime) {
		this.publishtime = publishtime;
	}
	
	
}
