package sy.task;

import sy.model.TQuiz;
import sy.model.TQuizManager;
import sy.pageModel.User;
import sy.service.QuizManagerServiceI;
import sy.service.QuizServiceI;
import sy.service.UserServiceI;
import sy.util.DateCompare;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaskServers implements Runnable {

	private QuizManagerServiceI quizManagerService;
	private QuizServiceI quizService;
	private TQuizManager quizManager;
	private UserServiceI userService;
	
	SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd hh:mm");
	
	public TaskServers(UserServiceI userService,QuizServiceI quizService,QuizManagerServiceI quizManagerService,TQuizManager quizManager){
	
		this.quizManagerService=quizManagerService;
		this.quizService=quizService;
		this.quizManager=quizManager;
		this.userService=userService;
	}
	
	
	@Override
	public void run() {
		try {
			long _todaytime;
			while (true) {
				_todaytime=new Date().getTime();
			
				if(DateCompare.compare_date(format.format(new Date()),quizManager.getEndtime())>0){
					//任务结束
					//更新任务状态
					TQuizManager quizManager=	quizManagerService.get(this.quizManager.getId());
					if(quizManager!=null){
						quizManager.setStatus(1);
						this.quizManagerService.update(quizManager);
						
					}
					
					//更新这起竞猜用户的中奖状态
					quizService.updateAllStatus(quizManager.getPeriodnumber(),quizManager.getNumber());
					
					//给中奖用户添加几分
					//1.通过用户ID 获得这一期用户的
					List<TQuiz> list=quizService.getQuizByPN(quizManager.getPeriodnumber(), quizManager.getNumber());
					User user=null;
					//每个用户获得的积分
					int countnumber=new Integer(quizManager.getGold());
					int countperson=list.size();
					int ecount=countnumber%countperson ==0? countnumber/countperson : countnumber/countperson+1;
					int oldcount=0;
					int oldunchange=0;
					if(countperson>0&&countnumber>0){
						
						for(TQuiz quiz:list){
							user=userService.get(quiz.getUserid());


                            if(user.getTotalscore()==null){
                                oldcount=ecount;
                            }else{
                                oldcount=user.getTotalscore().intValue()+ecount;
                            }

                            if(user.getUnchangedscore()==null){
                                oldunchange=ecount;
                            }else{
                                oldunchange=user.getUnchangedscore()+ecount;
                            }
							user.setTotalscore(oldcount);
							user.setUnchangedscore(oldunchange);
							userService.edit(user);
							//更新中奖用户状态
							quizService.updateStatus(quizManager.getPeriodnumber(), "1", quiz.getUserid());
							
						}
					}
					
					//更新未中奖用户状态
					List<TQuiz> nos=quizService.getQuizByPU(quizManager.getPeriodnumber(),quizManager.getNumber());
					for(TQuiz quiz:nos){
						
						quizService.updateStatus(quizManager.getPeriodnumber(), "0",quiz.getUserid());
						
					}
					System.out.println("end-thread");
					return;
				}
				Thread.sleep(1000);
				System.out.println("time:"+_todaytime);
			}
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
