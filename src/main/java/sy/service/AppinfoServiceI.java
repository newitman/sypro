package sy.service;
import java.util.List;

import sy.model.Tappinfo;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface AppinfoServiceI {
	public void add(Tappinfo tappinfo) throws Exception;
	public DataGrid dataGrid(Tappinfo tappinfo, PageHelper ph);
	public void delete(String id);
	public List<Tappinfo> getTapps();
	public Tappinfo get(String id);
	public void update(Tappinfo tappinfo);
	public List<Tappinfo> getTapps(String maxsize,String curentpage);
}
