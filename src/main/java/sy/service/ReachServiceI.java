package sy.service;
import java.util.List;

import sy.model.Treach;
import sy.pageModel.Mgif;
import sy.pageModel.Mreach;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface ReachServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mreach red) throws Exception;
	
	public List<Treach> getexchange(Long userid);
	
	
	
}
