package sy.service;
import sy.model.Tad;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

import java.util.List;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface AdServiceI {
	public void add(Tad ad) throws Exception;
	public DataGrid dataGrid(Tad tad, PageHelper ph);
	public void delete(String id);
	public List<Tad> getads();
    public Tad get(String id);
    public void update(Tad tad);
}
