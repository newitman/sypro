package sy.service.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.InviteRecordDaoI;
import sy.model.Tinviterecord;
import sy.pageModel.Rinviterecord;
import sy.service.InviteServiceI;
@Service
public class InviteServiceImpl implements InviteServiceI {
	@Autowired
	private InviteRecordDaoI inviteDao;
	public int i=0;

	 
	public void add(Rinviterecord inviterecord) throws Exception {
		Tinviterecord u = new Tinviterecord();
		u.setId(UUID.randomUUID().toString());
		u.setUserid(inviterecord.getUserid());				//自己
		u.setInviteuserid(inviterecord.getInviteuserid());	//别人
		u.setScore(inviterecord.getScore());
		u.setRealscore(inviterecord.getRealscore());
		u.setImei(inviterecord.getImei());
        inviteDao.save(u);
	}

    public void delete(String id) throws  Exception{
      String hql="delete from Tinviterecord t where t.id = '"+id+"'";
      inviteDao.executeHql(hql);

    }
	 
	public List<Tinviterecord> gettask(String userid) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Tinviterecord> t = inviteDao.find("from Tinviterecord t where t.userid = :userid ", params);
		return t;
	}
    /*
    查询当前用户上一级用户邀请记录
 */
     
    public Tinviterecord getUpClass(String userid) throws  Exception{

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userid", userid);
        List<Tinviterecord> t = inviteDao.find("from Tinviterecord t where t.inviteuserid = :userid ", params);
        if(t.size()>0){
            return t.get(0);
        }
        return null;
    }

    /*
   查询是否已经有过邀请记录
*/
     
    public List<Tinviterecord> getTinviterecord(Map<String,Object> parames) throws  Exception{

        List<Tinviterecord> t = inviteDao.find("from Tinviterecord t where t.imei = :imei", parames);

        return  t;
    }
    /*
    查询是否已经的人数
 */
	public String getCount(String userid) throws Exception {
		    Map<String, Object> params = new HashMap<String, Object>();
	        params.put("userid", userid);
	        List<Tinviterecord> t = inviteDao.find("from Tinviterecord t where t.userid = :userid ", params);
	        return t.size()+"";
	}

	//不能相互邀请
	public boolean inviteme(String userid, String invited) throws Exception{
		String hql="from Tinviterecord t where t.inviteuserid ='"+userid+"' and t.userid ='"+invited+"'";
		List<Tinviterecord> list=inviteDao.find(hql);
		if(list.size()>0){
			return true;//存在重复邀请
		}
		else{
			return false;//不存在重复邀请
		}
	}
    //查询结果
    public static    List<Tinviterecord> reulsts=new ArrayList<Tinviterecord>();
    public static    List<Tinviterecord> reulstweek=new ArrayList<Tinviterecord>();

    //获取该用户下面所有的推广下线
    public List<Tinviterecord> getTinviterecordCount(String userid){

        List<Tinviterecord> tinviterecords = inviteDao.find("from Tinviterecord t where t.userid =  '"+userid+"'");

        reulsts.addAll(tinviterecords);

        for(Tinviterecord tinviterecord:tinviterecords){

            getTinviterecordCount(tinviterecord.getInviteuserid());

        }

        return  reulsts;

    }
    
   
    
	public int getTinviterecordWeekCount(String userid,String tartget) {
		    List<Tinviterecord> tinviterecordsm = inviteDao.find("from Tinviterecord t where t.userid =  '"+userid+"' and  DATE_FORMAT(t.registertime,'%U')= DATE_FORMAT(now(),'%U') ");
		   if(tartget.equals("start")){
			  i = 0 ;
		   }
		   for(Tinviterecord tinviterecord:tinviterecordsm){
				i++;
				getTinviterecordWeekCount(tinviterecord.getInviteuserid(),"end");
	        }  
		 
		return  i;
		
	}

	public int QueryUnlineCount(String userid,String target) throws Exception {
		 List<Tinviterecord> tinviterecordsm = inviteDao.find("from Tinviterecord t where t.userid =  '"+userid+"'");
		   if(target.equals("start")){
			  i = 0 ;
		   }
		   for(Tinviterecord tinviterecord:tinviterecordsm){
				i++;
				getTinviterecordWeekCount(tinviterecord.getInviteuserid(),"end");
	        }  
		 
		return  i;
	}

}
