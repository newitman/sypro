package sy.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.AppDowloadInfoDaoI;
import sy.model.Tappdowloadinfo;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.AppDowloadInfoServiceI;
@Service
public class AppdowloadinfoServiceImpl implements AppDowloadInfoServiceI {
	@Autowired
	private AppDowloadInfoDaoI appDowloadInfoDao;
	
	@Override
	public void delete(String id) {
		appDowloadInfoDao.delete(appDowloadInfoDao.get(Tappdowloadinfo.class, id));
	}

	@Override
	public void add(Tappdowloadinfo  tappdowloadinfo) throws Exception {
		Tappdowloadinfo u = new Tappdowloadinfo();
		u.setDownloadtime(tappdowloadinfo.getDownloadtime());
		u.setUserid(tappdowloadinfo.getUserid());
		u.setAppid(tappdowloadinfo.getAppid());
		
		u.setId(UUID.randomUUID().toString());
		
		appDowloadInfoDao.save(u);
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(Tappdowloadinfo tappdowloadinfo, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Tappdowloadinfo> ul = new ArrayList<Tappdowloadinfo>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Tappdowloadinfo t ";
		List<Tappdowloadinfo> l = appDowloadInfoDao.find(hql + whereHql(tappdowloadinfo, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (Tappdowloadinfo t : l) {
				Tappdowloadinfo u = new Tappdowloadinfo();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(appDowloadInfoDao.count("select count(*) " + hql + whereHql(tappdowloadinfo, params), params));
		return dg;
	}
	private String whereHql(Tappdowloadinfo tappdowloadinfo, Map<String, Object> params) {
		String hql = "";
		if (tappdowloadinfo != null) {
			hql += " where 1=1 ";
			if (tappdowloadinfo.getUserid() != null) {
				hql += " and t.userid = :userid";
				params.put("userid", tappdowloadinfo.getUserid());
			}
			if (tappdowloadinfo.getAppid() != null) {
				hql += " and t.appid = :appid";
				params.put("appid", tappdowloadinfo.getAppid());
			}
		}
		return hql;
	}

	@Override
	public List<Tappdowloadinfo> getTappdowloadinfos() {
		
		String hql = " from Tappdowloadinfo t ";
		List<Tappdowloadinfo> l = appDowloadInfoDao.find(hql);
		
		return l;
	}
	@Override
	public List<Tappdowloadinfo> getTappdowloadinfo(String userid,String appid){
		String hql = " from Tappdowloadinfo t where t.appid ='"+appid+"' and t.userid='"+userid+"'";
		List<Tappdowloadinfo> tappdowloadinfos=appDowloadInfoDao.find(hql);
		
		return tappdowloadinfos;
		
	}
}
