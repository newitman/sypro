package sy.service.impl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sy.dao.QuizDaoI;
import sy.model.TQuiz;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.QuizServiceI;

import java.util.*;
@Service
public class QuizServiceImpl implements QuizServiceI {
	@Autowired
	private QuizDaoI quizDao;
	
	@Override
	public void delete(String id) {
		quizDao.delete(quizDao.get(TQuiz.class, id));
		
	}
	@Override
	public int getPeriodnumber(String periodnumber){
		String hql="from TQuiz t where t.periodnumber ='"+periodnumber+"'";
		
		return quizDao.find(hql).size();
	}
	public List<TQuiz> getQuizByPN(String periodnumber,String number){
		
		String hql="from TQuiz t where t.periodnumber ='"+periodnumber+"' and t.number ='"+number+"'";
		return quizDao.find(hql);
	}
	@Override
	public void add(TQuiz quiz) throws Exception {
		TQuiz u = new TQuiz();
		u.setPeriodnumber(quiz.getPeriodnumber());
		u.setStatus(0);
		u.setNumber(quiz.getNumber());
		u.setUserid(quiz.getUserid());
		u.setCreatedatetime(quiz.getCreatedatetime());
		u.setId(UUID.randomUUID().toString());
		//u.settime(new Date());
		quizDao.save(u);
	}
	@Override
	public int updateAllStatus(String periodnumber,String number){
		
		String hql="update TQuiz t set t.status =1 where t.number = '"+number+"' and t.periodnumber = '"+periodnumber+"'";
		return quizDao.executeHql(hql);
		
	}
	
	@Override
	public int updateStatus(String periodnumber,String status,String userid){
		String hql="update TQuiz t set t.status ="+status+" where t. periodnumber='"+periodnumber+"' and t.userid ='"+userid+"'";
		return quizDao.executeHql(hql);
		
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(TQuiz  user, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<TQuiz> ul = new ArrayList<TQuiz>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TQuiz t ";
		List<TQuiz> l = quizDao.find(hql + whereHql(user, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (TQuiz t : l) {
				TQuiz u = new TQuiz();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(quizDao.count("select count(*) " + hql + whereHql(user, params), params));
		return dg;
	}
	
	private String whereHql(TQuiz quiz, Map<String, Object> params) {
		String hql = "";
		if (quiz != null) {
			hql += " where 1=1 ";
			if (quiz.getNumber()!= null) {
				hql += " and t.number = :number";
				params.put("number",  quiz.getNumber());
			}
			if(quiz.getPeriodnumber()!=null){
				hql += " and t.periodnumber = :periodnumber";
				params.put("periodnumber",  quiz.getPeriodnumber());
			}
			if (quiz.getUserid()!= null) {
				hql += " and t.userid = :userid";
				params.put("userid",  quiz.getUserid() );
			}
				
		}
		return hql;
	}

	@Override
	public List<TQuiz> getQuiz(String userid,String createdatetime){
		
		String hql = " from TQuiz t where t.userid = '"+userid+"' and  t.createdatetime = '"+createdatetime+"'";
		List<TQuiz> l = quizDao.find(hql);
		
		return l;
	}
	
	@Override
	public List<TQuiz> getQuizByPU(String periodnumber,String lastnumber){
		
		String hql="from TQuiz t where  t.periodnumber='"+periodnumber+"' and number <> '"+lastnumber+"'";
		List<TQuiz> l = quizDao.find(hql);
		return l;
	}
}
