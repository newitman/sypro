package sy.service.impl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sy.dao.PushDaoI;
import sy.model.TPush;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.PushServiceI;

import java.util.*;
@Service
public class PushServiceImpl implements PushServiceI {
    @Autowired
    private PushDaoI pushDao;

    @Override
    public void delete(String id) {

        pushDao.delete(pushDao.get(TPush.class, id));
    }

    @Override
    public void add(TPush tPush) throws Exception {
        TPush u = new TPush();
        u.setPublistime(tPush.getPublistime());
        u.setLinkAddress(tPush.getLinkAddress());
        u.setTitle(tPush.getTitle());
        u.setId(UUID.randomUUID().toString());
        u.setContent(tPush.getContent());
        pushDao.save(u);
    }

    private String orderHql(PageHelper ph) {
        String orderString = "";
        if (ph.getSort() != null && ph.getOrder() != null) {
            orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
        }
        return orderString;
    }
    @Override
    public DataGrid dataGrid(TPush tPush, PageHelper ph) {
        DataGrid dg = new DataGrid();
        List<TPush> ul = new ArrayList<TPush>();
        Map<String, Object> params = new HashMap<String, Object>();
        String hql = " from TPush t ";
        List<TPush> l = pushDao.find(hql + whereHql(tPush, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
        if (l != null && l.size() > 0) {
            for (TPush t : l) {
                TPush u = new TPush();
                BeanUtils.copyProperties(t, u);
                ul.add(u);
            }
        }
        dg.setRows(ul);
        dg.setTotal(pushDao.count("select count(*) " + hql + whereHql(tPush, params), params));
        return dg;
    }
    private String whereHql(TPush tPush, Map<String, Object> params) {
        String hql = "";
        if (tPush != null) {
            hql += " where 1=1 ";
            if (tPush.getTitle() != null&&!tPush.getTitle().equals("")) {
                hql += " and t.title like :title";
                params.put("title", "%%" + tPush.getTitle() + "%%");
            }
            if (tPush.getSerchstarttime() != null&& !tPush.getSerchstarttime().equals("")) {
                hql += " and t.publistime >= :serchstarttime";
                params.put("serchstarttime", tPush.getSerchstarttime());
            }
            if (tPush.getSerchendtime()!= null && !tPush.getSerchendtime().equals("")) {
                hql += " and t.publistime <= :serchendtime";
                params.put("serchendtime",tPush.getSerchendtime());
            }

        }
        return hql;
    }


    @Override
    public TPush get(String id){
        String hql = " from TPush t where t.id= '"+id+"'";
        List<TPush> l = pushDao.find(hql);
        if(l!=null&&l.size()>0){
            return l.get(0);
        }
        return null;
    }
}
