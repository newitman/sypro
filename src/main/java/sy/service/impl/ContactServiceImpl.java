package sy.service.impl;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.ContactDaoI;
import sy.dao.GifDaoI;
import sy.model.Tcontact;
import sy.model.Tgif;
import sy.pageModel.Mcontact;
import sy.pageModel.Mgif;
import sy.service.ContactServiceI;
import sy.service.GifServiceI;
@Service
public class ContactServiceImpl implements ContactServiceI {
	@Autowired
	private ContactDaoI contactDao;
	@Override
	public void add(Mcontact contact) throws Exception {
		Tcontact u = new Tcontact();
		u.setUserid(contact.getUserid());
		u.setId(UUID.randomUUID().toString());
		u.setContime(new Date());
		u.setContent(contact.getContent());
		contactDao.save(u);
	}
}
