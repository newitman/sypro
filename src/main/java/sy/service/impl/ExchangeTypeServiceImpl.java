package sy.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.ExchangeTypeDaoI;
import sy.model.Texchangetype;
import sy.model.Tuser;
import sy.pageModel.DataGrid;
import sy.pageModel.MTasktype;
import sy.pageModel.Mexchangetype;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
import sy.service.ExchangetypeServiceI;
import sy.util.DESUtil;
@Service
public class ExchangeTypeServiceImpl implements ExchangetypeServiceI {
	@Autowired
	private ExchangeTypeDaoI changetypeDao;

	 
	public Texchangetype getexchange(String Typeid) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typeid", Typeid);
		Texchangetype t = changetypeDao.get("from Texchangetype t where t.typeid = :typeid ", params);
		return t;
	}
	
	 
	public DataGrid dataGrid(Mexchangetype changetype, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Mexchangetype> ul = new ArrayList<Mexchangetype>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Texchangetype t ";
		List<Texchangetype> l = changetypeDao.find(hql + whereHql(changetype, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for(Texchangetype texchange : l){
			Mexchangetype mex = new Mexchangetype();
			BeanUtils.copyProperties(texchange, mex);
			ul.add(mex); 
		}
		dg.setRows(ul);
		dg.setTotal(changetypeDao.count("select count(*) " + hql + whereHql(changetype, params), params));
		return dg;
	}
	
	private String whereHql(Mexchangetype exchange, Map<String, Object> params) {
		String hql = "";
		if (exchange != null) {
			hql += " where 1=1 ";
			/*if (task.getTypeid() != null) {
				hql += " and t.name like :name";
				params.put("name", "%%" + user.getName() + "%%");
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime >= :createdatetimeStart";
				params.put("createdatetimeStart", user.getCreatedatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime <= :createdatetimeEnd";
				params.put("createdatetimeEnd", user.getCreatedatetimeEnd());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime >= :modifydatetimeStart";
				params.put("modifydatetimeStart", user.getModifydatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime <= :modifydatetimeEnd";
				params.put("modifydatetimeEnd", user.getModifydatetimeEnd());
			}*/
		}
		return hql;
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	 
	public int getexchangesize() throws Exception {
		List<Texchangetype> t = changetypeDao.find("from Texchangetype t ");
		return t.size();
	}

	 
	public void add(Mexchangetype exchange) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typecontent", exchange.getTypecontent());
		if (changetypeDao.count("select count(*) from Texchangetype t where t.typecontent = :typecontent", params) > 0) {
			throw new Exception("添加的兑换名称失败！");
		} else {
			Texchangetype u = new Texchangetype();
			BeanUtils.copyProperties(exchange, u);
			changetypeDao.save(u);
		}
	}

	 
	public void edit(Mexchangetype exchange) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", exchange.getId());
		params.put("name",exchange.getTypecontent());
		if (changetypeDao.count("select count(*) from Texchangetype t where t.typecontent = :name and t.id != :id", params) > 0) {
			throw new Exception("类型名称已存在！");
		} else {
			Texchangetype u = changetypeDao.get(Texchangetype.class, exchange.getId());
			BeanUtils.copyProperties(exchange, u);
		}
	}

	 
	public List<Mexchangetype> Combobox(String q) {
		if (q == null) {
			q = "";
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", "%%" + q.trim() + "%%");
		List<Texchangetype> tl = changetypeDao.find("from Texchangetype t ");
		List<Mexchangetype> ul = new ArrayList<Mexchangetype>();
		Mexchangetype init = new Mexchangetype();
		init.setTypecontent("全部");
		init.setTypeid("0");
		ul.add(init);
		if (tl != null && tl.size() > 0) {
			for (Texchangetype t : tl) {
				Mexchangetype u = new Mexchangetype();
				u.setTypecontent(t.getTypecontent());
				u.setTypeid(t.getTypeid());
				ul.add(u);
			}
		}
		return ul;
	}
}
