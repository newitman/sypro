package sy.service.impl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sy.dao.QuizManagerDaoI;
import sy.model.TQuizManager;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.QuizManagerServiceI;

import java.util.*;
@Service
public class QuizManagerServiceImpl implements QuizManagerServiceI {
	@Autowired
	private QuizManagerDaoI quizManagerDao;
	
	@Override
	public void delete(String id) {
		quizManagerDao.delete(quizManagerDao.get(TQuizManager.class, id));
	}
	@Override
	public TQuizManager get(String tqid){
		String hql="from TQuizManager t where t.id ='"+tqid+"'";
		TQuizManager manager=quizManagerDao.get(hql);
		return manager;
	}
	
	@Override
	public void update(TQuizManager quizManager){
		quizManagerDao.update(quizManager);
	}
	@Override
	public TQuizManager getLastTQuizManager(){
		String hql = " from TQuizManager t order by t.createdatetime desc ";
		List<TQuizManager> l = quizManagerDao.find(hql);
		if(l.size()>0){
			return l.get(0);
		}
		return null;
	}
	@Override
	public TQuizManager getLastAfterTQuizManager(){
		String hql = " from TQuizManager t order by t.createdatetime desc ";
		List<TQuizManager> l = quizManagerDao.find(hql);
		if(l.size()>0){
			if(l.size()>2){
				return l.get(1);
			}else{
				return l.get(0);
			}
		}
		return null;
	}
    @Override//通过期号
    public TQuizManager getQHQuizManager(String qh){
        String hql="from TQuizManager t where t.periodnumber='"+qh+"'";
        List<TQuizManager> l = quizManagerDao.find(hql);
        if(l.size()>0){
            return l.get(0);
        }else {
            return null;
        }
     }
	@Override
	public TQuizManager add(TQuizManager ad) throws Exception {
		TQuizManager u = new TQuizManager();
		
		u.setEndtime(u.getEndtime());
		u.setNumber(ad.getNumber());
		u.setStarttime(ad.getStarttime());
		u.setEndtime(ad.getEndtime());
		u.setUserid(ad.getUserid());
		u.setGold(ad.getGold());
		u.setPeriodnumber(ad.getPeriodnumber());
		u.setId(UUID.randomUUID().toString());
		u.setCreatedatetime(new Date().getTime()+"");
		quizManagerDao.save(u);
		
		return u;
		
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(TQuizManager user, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<TQuizManager> ul = new ArrayList<TQuizManager>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TQuizManager t ";
		List<TQuizManager> l = quizManagerDao.find(hql + whereHql(user, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		
		
		if (l != null && l.size() > 0) {
			for (TQuizManager t : l) {
				TQuizManager u = new TQuizManager();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(quizManagerDao.count("select count(*) " + hql + whereHql(user, params), params));
		return dg;
	}
	//是否可以发布竞猜 如果返回为空 则可以发布
	public List<TQuizManager> getQuiz(String number,String starttime,String endtime){
		String hql = " from TQuizManager t ";
		String where="";
		if(starttime!=null&&!"".equals(starttime)){
			where+="  endtime<= '"+starttime+"'";
		}
		
		
		List<TQuizManager> l = quizManagerDao.find(hql +" where "+where );
	
		return l;
	}
	private String whereHql(TQuizManager tad, Map<String, Object> params) {
		String hql = "";
		if (tad != null) {
			hql += " where 1=1 ";
			if (tad.getNumber() != null) {
				hql += " and t.number = :number";
				params.put("number",  tad.getNumber() );
			}
			if ( tad.getStarttime() != null && !"".equals(tad.getStarttime())) {
				hql += " and t.starttime >= :starttime";
				params.put("starttime",  tad.getStarttime() );
			}
			if (tad.getPeriodnumber() != null &&!"".equals(tad.getPeriodnumber())) {
				hql += " and t.periodnumber = :periodnumber";
				params.put("periodnumber",tad.getStarttime() );
			}
			if (tad.getEndtime() != null && !"".equals(tad.getEndtime() )) {
				hql += " and t.endtime <= :endtime";
				params.put("endtime",  tad.getEndtime() );
			}
			if (tad.getUserid() != null && !"".equals(tad.getUserid() )) {
				hql += " and t.userid = :userid";
				params.put("userid",  tad.getUserid() );
			}
		}
		System.out.println(hql);
		return hql;
	}

	@Override
	public List<TQuizManager> getQuizManagers() {
		
		String hql = " from TQuizManager t ";
		List<TQuizManager> l = quizManagerDao.find(hql);
		
		return l;
	}
}
