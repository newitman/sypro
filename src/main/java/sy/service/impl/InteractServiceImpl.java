package sy.service.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.InteractDaoI;
import sy.model.Tinteract;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.InteractServiceI;
@Service
public class InteractServiceImpl implements InteractServiceI {
	@Autowired
	private InteractDaoI interactDao;
	
	@Override
	public void delete(String id) {
		interactDao.delete(interactDao.get(Tinteract.class, id));
	}
    @Override
    public Tinteract get(String id){
        String hql=" from Tinteract t where t.id = '"+id+"'";
        return interactDao.get(hql);
    }
	@Override
	public void add(Tinteract tinteract) throws Exception {
		Tinteract u = new Tinteract();
		u.setPublishtime(tinteract.getPublishtime());
		u.setContent(tinteract.getContent());
		u.setImageurl(tinteract.getImageurl());
		u.setId(UUID.randomUUID().toString());
		u.setTitle(tinteract.getTitle());
		interactDao.save(u);
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(Tinteract tinteract, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Tinteract> ul = new ArrayList<Tinteract>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Tinteract t ";
		List<Tinteract> l = interactDao.find(hql + whereHql(tinteract, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (Tinteract t : l) {
				Tinteract u = new Tinteract();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(interactDao.count("select count(*) " + hql + whereHql(tinteract, params), params));
		return dg;
	}
	private String whereHql(Tinteract tinteract, Map<String, Object> params) {
		String hql = "";
		if (tinteract != null) {
			hql += " where 1=1 ";
			if (tinteract.getPublishtime() != null) {
				hql += " and t.publishtime >= :publishtime";
				params.put("publishtime",  tinteract.getPublishtime() );
			}
			
		}
		return hql;
	}

	@Override
	public List<Tinteract> getTinteracts(String maxsize,String curentpage) {
		
		PageHelper pageHelper=new PageHelper();
		pageHelper.setSort(" order by t.publishtime desc ");
		pageHelper.setPage(Integer.parseInt(curentpage));
		pageHelper.setRows(Integer.parseInt(maxsize));
		
		
		String hql = " from Tinteract t ";
		String groupby ="  group by  publishtime ";
		
		List<Tinteract> ul = new ArrayList<Tinteract>();
		List<Tinteract> l = interactDao.find(hql + orderHql(pageHelper)+groupby, pageHelper.getPage(), pageHelper.getRows());
		if (l != null && l.size() > 0) {
			for (Tinteract t : l) {
				Tinteract u = new Tinteract();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		
		return ul;
	}

    @Override
    public List<Tinteract> getTinteracts(String maxsize,String curentpage,String publishtime){
        PageHelper pageHelper=new PageHelper();
        pageHelper.setSort(" order by t.publishtime desc ");
        pageHelper.setPage(Integer.parseInt(curentpage));
        pageHelper.setRows(Integer.parseInt(maxsize));


        String hql = " from Tinteract t where t.publishtime ='"+publishtime+"' ";

        List<Tinteract> ul = new ArrayList<Tinteract>();
        List<Tinteract> l = interactDao.find(hql + orderHql(pageHelper), pageHelper.getPage(), pageHelper.getRows());
        if (l != null && l.size() > 0) {
            for (Tinteract t : l) {
                Tinteract u = new Tinteract();
                BeanUtils.copyProperties(t, u);
                ul.add(u);
            }
        }

        return ul;
    }
}
