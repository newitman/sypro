package sy.service.impl;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.GifDaoI;
import sy.dao.ShakeDaoI;
import sy.model.Tgif;
import sy.model.Tshake;
import sy.pageModel.Mgif;
import sy.pageModel.Mshake;
import sy.service.GifServiceI;
import sy.service.ShakeServiceI;
@Service
public class ShakeServiceImpl implements ShakeServiceI {
	@Autowired
	private ShakeDaoI shakeDao;
	@Override
	public void add(Mshake shake) throws Exception {
		Tshake u = new Tshake();
		u.setId(UUID.randomUUID().toString());
		u.setUserid(shake.getUserid());
		u.setScore(shake.getScore());
		u.setTime(new Date());
		shakeDao.save(u);
	}
}
