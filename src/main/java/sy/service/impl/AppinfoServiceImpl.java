package sy.service.impl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sy.dao.AppinfoDaoI;
import sy.model.Tappinfo;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.AppinfoServiceI;

import java.util.*;
@Service
public class AppinfoServiceImpl implements AppinfoServiceI {
	@Autowired
	private AppinfoDaoI appinfoDao;
	
	@Override
	public void delete(String id) {
		appinfoDao.delete(appinfoDao.get(Tappinfo.class, id));
	}

	@Override
	public void add(Tappinfo tappinfo) throws Exception {
		Tappinfo u = new Tappinfo();
	
		u.setId(UUID.randomUUID().toString());
		u.setAppAddress(tappinfo.getAppAddress());
		u.setAppname(tappinfo.getAppname());
		u.setDescription(tappinfo.getDescription());
		u.setIntegral(tappinfo.getIntegral());
		u.setCreatetime(tappinfo.getCreatetime());
        u.setImageurl(tappinfo.getImageurl());
        u.setAppsize(tappinfo.getAppsize());
        u.setVersion(tappinfo.getVersion());
        u.setDetailaddress(tappinfo.getDetailaddress());
		appinfoDao.save(u);
	}
	
	@Override
	public List<Tappinfo> getTapps(String maxsize,String curentpage){
		PageHelper pageHelper=new PageHelper();
		pageHelper.setSort(" order by t.createtime desc ");
		pageHelper.setPage(Integer.parseInt(curentpage));
		pageHelper.setRows(Integer.parseInt(maxsize));
		
		String hql = " from Tappinfo t ";
		List<Tappinfo> ul = new ArrayList<Tappinfo>();
		List<Tappinfo> l = appinfoDao.find(hql + orderHql(pageHelper), pageHelper.getPage(), pageHelper.getRows());
		if (l != null && l.size() > 0) {
			for (Tappinfo t : l) {
				Tappinfo u = new Tappinfo();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		
		return ul;
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(Tappinfo tappinfo, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Tappinfo> ul = new ArrayList<Tappinfo>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Tappinfo t ";
		List<Tappinfo> l = appinfoDao.find(hql + whereHql(tappinfo, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (Tappinfo t : l) {
				Tappinfo u = new Tappinfo();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(appinfoDao.count("select count(*) " + hql + whereHql(tappinfo, params), params));
		return dg;
	}
	private String whereHql(Tappinfo tappinfo, Map<String, Object> params) {
		String hql = "";
		if (tappinfo != null) {
			hql += " where 1=1 ";
			if (tappinfo.getAppname()!= null) {
				hql += " and t.appname like :appname";
				params.put("appname", "%%" + tappinfo.getAppname()+ "%%");
			}
			if (tappinfo.getDescription() != null) {
				hql += " and t.description like :description";
				params.put("description", "%%" +tappinfo.getDescription() + "%%");
			}
		}
		return hql;
	}

	@Override
	public List<Tappinfo> getTapps(){
		
		String hql = " from Tappinfo t ";
		List<Tappinfo> l = appinfoDao.find(hql);
		
		return l;
	}
	@Override
	public Tappinfo get(String id){
		String hql = " from Tappinfo t  where t.id='"+id+"'";
		List<Tappinfo> l = appinfoDao.find(hql);
		if(l.size()>0){
			return l.get(0);
		}
		return null;
		
	}
	@Override
	public void update(Tappinfo tappinfo){
		
		appinfoDao.update(tappinfo);
		
		
	}
}
