package sy.service.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.TaskTypeDaoI;
import sy.model.Taskrecord;
import sy.model.Tasktype;
import sy.model.Texchangetype;
import sy.pageModel.DataGrid;
import sy.pageModel.MTasktype;
import sy.pageModel.Mexchangetype;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
import sy.service.TaskTypeServiceI;
@Service
public class TasktypeServiceImpl implements TaskTypeServiceI {
	@Autowired
	private TaskTypeDaoI tasktypeDao;
	
	@Override
	public Tasktype gettasktype(String Typeid) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typeid", Typeid);
		Tasktype t = tasktypeDao.get("from Tasktype t where t.typeid = :typeid ", params);
		return t;
	}
	
	@Override
	public DataGrid dataGrid(MTasktype task, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<MTasktype> ul = new ArrayList<MTasktype>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Tasktype t ";
		List<Tasktype> l = tasktypeDao.find(hql + whereHql(task, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for(Tasktype tasks : l){
			MTasktype mex = new MTasktype();
			BeanUtils.copyProperties(tasks, mex);
			ul.add(mex); 
		}
		dg.setRows(ul);
		dg.setTotal(tasktypeDao.count("select count(*) " + hql + whereHql(task, params), params));
		return dg;
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(MTasktype task, Map<String, Object> params) {
		String hql = "";
		if (task != null) {
			hql += " where 1=1 ";
			/*if (task.getTypeid() != null) {
				hql += " and t.name like :name";
				params.put("name", "%%" + user.getName() + "%%");
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime >= :createdatetimeStart";
				params.put("createdatetimeStart", user.getCreatedatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime <= :createdatetimeEnd";
				params.put("createdatetimeEnd", user.getCreatedatetimeEnd());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime >= :modifydatetimeStart";
				params.put("modifydatetimeStart", user.getModifydatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime <= :modifydatetimeEnd";
				params.put("modifydatetimeEnd", user.getModifydatetimeEnd());
			}*/
		}
		return hql;
	}
	@Override
	public int gettasksize() throws Exception {
		List<Tasktype> t = tasktypeDao.find("from Tasktype t ");
		return t.size();
	}

	@Override
	public void add(MTasktype tasktype) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typecontent", tasktype.getTypecontent());
		if (tasktypeDao.count("select count(*) from Tasktype t where t.typecontent = :typecontent", params) > 0) {
			throw new Exception("添加的兑换名称失败！");
		} else {
			Tasktype u = new Tasktype();
			BeanUtils.copyProperties(tasktype, u);
			tasktypeDao.save(u);
		}
		
	}

	@Override
	public void edit(MTasktype tasktype) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", tasktype.getId());
		params.put("name",tasktype.getTypecontent());
		if (tasktypeDao.count("select count(*) from Tasktype t where t.typecontent = :name and t.id != :id", params) > 0) {
			throw new Exception("类型名称已存在！");
		} else {
			Tasktype u = tasktypeDao.get(Tasktype.class, tasktype.getId());
			BeanUtils.copyProperties(tasktype, u);
		}
	}
	
}
