package sy.service.impl;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.ExchangeDaoI;
import sy.model.Taskrecord;
import sy.model.Texchange;
import sy.model.Tuser;
import sy.pageModel.DataGrid;
import sy.pageModel.Mexchange;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
import sy.service.ExchangeServiceI;
import sy.service.ExchangetypeServiceI;
import sy.service.UserServiceI;
@Service
public class ExchangeServiceImpl implements ExchangeServiceI {
	@Autowired
	private ExchangeDaoI changeDao;
	
	@Autowired
	private ExchangetypeServiceI changetype;
	
	
	@Autowired
	private UserServiceI userService;
	
	
	 
	public void add(Mexchange exchange) throws Exception {
		Texchange u = new Texchange();
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatterstr = new SimpleDateFormat("MM-dd HH:mm");
		Date dateString =formatter.parse(formatter.format(currentTime));
	    u.setId(UUID.randomUUID().toString());
		u.setExchangetime(dateString);
		u.setUserid(exchange.getUserid());
		u.setAlipay(exchange.getAlipay());
		u.setCellPhone(exchange.getCellPhone());
		u.setDisplaytime(formatterstr.format(currentTime));
		u.setGoods(exchange.getGoods());
		u.setType(exchange.getType());
		u.setQq(exchange.getQq());
		u.setScore(exchange.getScore());
		u.setChangestatus(exchange.getChangestatus());
		u.setUseridcp(userService.get(exchange.getUserid()).getUserid()+"");
		u.setNicknamecp(userService.get(exchange.getUserid()).getName());
		changeDao.save(u);
	}
	 
	public List<Texchange> getNewExchange() throws Exception {
		return changeDao.find("from Texchange order by exchangetime desc");
	}
	 
	public List<Texchange> getexchange(String userid) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Texchange> t = changeDao.find("from Texchange t where t.userid = :userid ", params);
		return t;
	}
	 
	public DataGrid dataGrid(Mexchange change, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Mexchange> ul = new ArrayList<Mexchange>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Texchange t ";
		List<Texchange> l = changeDao.find(hql + whereHql(change, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for(Texchange texchange : l){
			Mexchange mex = new Mexchange();
			BeanUtils.copyProperties(texchange, mex);				//设置属性赋值
			String contont;
			try {
				contont = changetype.getexchange(texchange.getType()).getTypecontent();
				mex.setExchangecontent(contont);
			} catch (Exception e) {
				e.printStackTrace();
			}
			ul.add(mex); 
		}
		dg.setRows(ul);
		dg.setTotal(changeDao.count("select count(*) " + hql + whereHql(change, params), params));
		return dg; 
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(Mexchange change, Map<String, Object> params) {
		String hql = "";
		if (change != null) {
			hql += " where 1=1 ";
			if (change.getType() != null) {
				if("0".equals(change.getType())){
					
				}else{
					hql += " and t.type =  :type";
					params.put("type",  change.getType());
				}
			}
			if (change.getQq() != null) {
				hql += " and t.qq like :qq";
				params.put("qq", "%%" + change.getQq() + "%%");
			}
			
			if (change.getCellPhone() != null) {
				hql += " and t.cellPhone like :cellPhone";
				params.put("cellPhone", "%%" + change.getCellPhone() + "%%");
			}
			
			if (change.getAlipay() != null) {
				hql += " and t.alipay like :alipay";
				params.put("alipay", "%%" + change.getAlipay() + "%%");
			}
			
			if (change.getUseridcp() != null) {
				hql += " and t.useridcp like :useridcp";
				params.put("useridcp", "%%" + change.getUseridcp() + "%%");
			}
			
			if (change.getNicknamecp() != null) {
				hql += " and t.nicknamecp like :nicknamecp";
				params.put("nicknamecp", "%%" + change.getNicknamecp() + "%%");
			}
			if(change.getChangestatus() !=null){
				if(!"".equals(change.getChangestatus())){
					hql += " and t.changestatus = :changestatus";
					params.put("changestatus",change.getChangestatus());
				}
			}
			
			
		}
		return hql;
	}
	public void updatestatu(String id, String statu) {
		String hql="update Texchange t set t.changestatus = "+statu+" where t.id= '"+id+"'";
		changeDao.executeHql(hql);
	}

	public int getCheckCount(String userid, String typeid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);//用户ID
		params.put("type" ,typeid);//类型ID
		List<Texchange> t = changeDao.find("from Texchange t where t.changestatus = 0 and  t.userid = :userid and t.type = :type", params);
		return t.size();
	}
	public void delete(String id) {
		changeDao.delete(changeDao.get(Texchange.class, id));
	}

	public Texchange getTexchangedetial(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		List<Texchange> t = changeDao.find("from Texchange t where t.id = :id ", params);
		return t.get(0);
	}

}
