package sy.service.impl;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.TaskRecordDaoI;
import sy.model.Taskrecord;
import sy.model.Texchange;
import sy.model.Tuser;
import sy.pageModel.DataGrid;
import sy.pageModel.Mexchange;
import sy.pageModel.Mexchangetype;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.service.TaskRecordServiceI;
import sy.service.TaskTypeServiceI;
import sy.util.MD5Util;
@Service
public class TaskrecordServiceImpl implements TaskRecordServiceI {
	@Autowired
	private TaskRecordDaoI taskDao;
	
	@Autowired
	private TaskTypeServiceI   taskTypeService;
	
	 
	public void add(Mtaskrecord taskrecord) throws Exception {
		Date currentTime = new Date();
		SimpleDateFormat formatterstr = new SimpleDateFormat("MM-dd HH:mm");
		//日期格式
		SimpleDateFormat formatter  = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
		//日期格式某天
		SimpleDateFormat formatterday  = new SimpleDateFormat("yyyy-MM-dd");
		Date dateString =formatter.parse(formatter.format(currentTime));
		Date dateStringday =formatterday.parse(formatterday.format(currentTime));
		Taskrecord u = new Taskrecord();
		u.setId(UUID.randomUUID().toString());
		u.setUserid(taskrecord.getUserid());
		u.setDisplaytime(formatterstr.format(currentTime));
		u.setScore(taskrecord.getScore());
		u.setTaskType(taskrecord.getTaskType());
		u.setFinishTime(dateString);
		u.setFinishDate(dateStringday);
		if("6".equalsIgnoreCase(taskrecord.getTaskType())){
			u.setGiflev(taskrecord.getGiflev());
		}
		taskDao.save(u);
	}
	 
	public List<Taskrecord> gettask(Long userid) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where t.userid = :userid ", params);
		return t;
	}
	 
	public int getexistday(Long userid, Date date) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		params.put("finishdate", date);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where  t.taskType=3 and t.userid = :userid and t.finishDate = :finishdate", params);
		return t.size();
	 
	}
	 
	public int getexistshake(Long userid, Date date) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		params.put("finishdate", date);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where   t.taskType=4 and t.userid = :userid and t.finishDate = :finishdate", params);
		return t.size();
	}
	 
	public int getexistrock(Long userid, Date date) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		params.put("finishdate", date);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where   t.taskType=5 and t.userid = :userid and t.finishDate = :finishdate", params);
		return t.size();
	}
	 
	public int checklevl(Long userid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where   t.taskType=3 and t.userid = :userid ", params);
		return t.size();
	}
	
	public int checklevlws(Long userid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where   t.taskType=2 and t.userid = :userid ", params);
		return t.size();
	}
	 
	public int checkgif(Long userid,Integer lev) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		params.put("giflev", lev);
		List<Taskrecord> t = taskDao.find("from Taskrecord t where  t.taskType=6 and t.giflev = :giflev and t.userid = :userid ", params);
		return t.size();
	}
	 
	public DataGrid dataGrid(Mtaskrecord task, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<Mtaskrecord> ul = new ArrayList<Mtaskrecord>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Taskrecord t ";
		List<Taskrecord> l = taskDao.find(hql + whereHql(task, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for(Taskrecord tasks : l){
			Mtaskrecord mex = new Mtaskrecord();
			BeanUtils.copyProperties(tasks, mex);
			String tasttype;
			try {
				tasttype = taskTypeService.gettasktype(tasks.getTaskType()).getTypecontent();
				mex.setTaskType(tasttype);
			} catch (Exception e) {
				e.printStackTrace();
			}
			ul.add(mex); 
		}
		dg.setRows(ul);
		dg.setTotal(taskDao.count("select count(*) " + hql + whereHql(task, params), params));
		return dg;
	}
	
	
	private String whereHql(Mtaskrecord task, Map<String, Object> params) {
		String hql = "";
		if (task != null) {
			hql += " where 1=1 ";
			if (task.getUserid() != null) {
				hql += " and t.userid like :userid";
				params.put("userid",task.getUserid());
			}
			/*if (task.getTypeid() != null) {
				hql += " and t.name like :name";
				params.put("name", "%%" + user.getName() + "%%");
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime >= :createdatetimeStart";
				params.put("createdatetimeStart", user.getCreatedatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.createdatetime <= :createdatetimeEnd";
				params.put("createdatetimeEnd", user.getCreatedatetimeEnd());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime >= :modifydatetimeStart";
				params.put("modifydatetimeStart", user.getModifydatetimeStart());
			}
			if (task.getTypeid() != null) {
				hql += " and t.modifydatetime <= :modifydatetimeEnd";
				params.put("modifydatetimeEnd", user.getModifydatetimeEnd());
			}*/
		}
		return hql;
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	public int checkscore(Long userid, String typeid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		params.put("taskType", typeid);
		List<Taskrecord> t = taskDao.find("from Taskrecord where userid= :userid and taskType= :taskType", params);
		int score = 0;
		for(Taskrecord m:t){
			score=m.getScore()+score;
		}
		return score;
	}

	 
	public void delete(String id) {
		taskDao.delete(taskDao.get(Taskrecord.class, id));
	}

	@Override
	public int excetionscore(Long userid, String typeid) {
		List<Taskrecord> t = taskDao.find("from Taskrecord where userid= "+userid+" and  DATEDIFF(NOW(),finishDate)=0 and taskType= "+typeid);
		return t.size();
	}
}
