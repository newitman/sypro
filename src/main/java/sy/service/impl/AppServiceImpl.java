package sy.service.impl;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.AppDaoI;
import sy.dao.GifDaoI;
import sy.model.Tappcheck;
import sy.model.Tgif;
import sy.pageModel.Mappcheck;
import sy.pageModel.Mgif;
import sy.service.AppServiceI;
import sy.service.GifServiceI;
@Service
public class AppServiceImpl implements AppServiceI {
	@Autowired
	private AppDaoI appDao;
	@Override
	public void add(Mappcheck app) throws Exception {
		Tappcheck u = new Tappcheck();
		u.setId(UUID.randomUUID().toString());
		//u.settime(new Date());
		appDao.save(u);
	}
}
