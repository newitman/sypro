package sy.service.impl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.GifDaoI;
import sy.dao.ReachfDaoI;
import sy.model.Taskrecord;
import sy.model.Tgif;
import sy.model.Treach;
import sy.pageModel.Mgif;
import sy.pageModel.Mreach;
import sy.service.GifServiceI;
import sy.service.ReachServiceI;
@Service
public class ReachServiceImpl implements ReachServiceI {
	@Autowired
	private ReachfDaoI reachDao;
	@Override
	public void add(Mreach reac) throws Exception {
		Treach u = new Treach();
		u.setId(UUID.randomUUID().toString());
		u.setreachtime(new Date());
		u.setUserid(reac.getUserid());
		u.setScore(2000);
		reachDao.save(u);
	}
	@Override
	public List<Treach> getexchange(Long userid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		List<Treach> t = reachDao.find("from Treach t where t.userid = :userid ", params);
		return t;
	}
}
