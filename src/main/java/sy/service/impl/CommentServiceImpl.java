package sy.service.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sy.dao.CommentDaoI;
import sy.model.TComment;
import sy.model.Tinteract;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.service.CommentServiceI;
@Service
public class CommentServiceImpl implements CommentServiceI {
	@Autowired
	private CommentDaoI commentDao;
	
	@Override
	public void delete(String id) {
		commentDao.delete(commentDao.get(TComment.class, id));
	}
	@Override
	public int getlikecount(String iid){
		String hql = " from TComment t where t.iid ='"+iid+"' and t.islike = 1";
		List<TComment> list=commentDao.find(hql);
		
		return list.size();
	}
	@Override
	public int getcommentcount(String iid){
		String hql = " from TComment t where t.iid ='"+iid+"'";
		List<TComment> list=commentDao.find(hql);
		return list.size();
	}

    @Override
    public List<TComment> getTComments(String iid){
        String hql = " from TComment t where t.iid ='"+iid+"'";
        List<TComment> list=commentDao.find(hql);
        return list;
    }
    @Override
    public TComment getTComment(String uid,String iid){
        String hql = " from TComment t where t.userid ='"+uid+"'  and t.iid='"+iid+"'";
        List<TComment> list=commentDao.find(hql);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
	@Override
	public void add(TComment comment) throws Exception {
		TComment u = new TComment();
		u.setComenttime(comment.getComenttime());
		u.setIid(comment.getIid());
		u.setIslike(comment.getIslike());
		u.setUserid(comment.getUserid());
		u.setCommentmsg(comment.getCommentmsg());
		u.setUsername(comment.getUsername());
		u.setId(UUID.randomUUID().toString());
		//u.settime(new Date());
		commentDao.save(u);
	}
	
	private String orderHql(PageHelper ph) {
		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	@Override
	public DataGrid dataGrid(TComment user, PageHelper ph) {
		DataGrid dg = new DataGrid();
		List<TComment> ul = new ArrayList<TComment>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TComment t ";
		List<TComment> l = commentDao.find(hql + whereHql(user, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (TComment t : l) {
				TComment u = new TComment();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(commentDao.count("select count(*) " + hql + whereHql(user, params), params));
		return dg;
	}
	private String whereHql(TComment comment, Map<String, Object> params) {
		String hql = "";
		if (comment != null) {
			hql += " where 1=1 ";
			if (comment.getIid() != null) {
				hql += " and t.iid = :iid";
				params.put("iid", comment.getIid());
			}
			if (comment.getUserid() != null) {
				hql += " and t.userid = :userid";
				params.put("userid", comment.getUserid());
			}
		}
		return hql;
	}

	@Override
	public List<TComment> getComments() {
		
		String hql = " from TComment t ";
		List<TComment> l = commentDao.find(hql);
		
		return l;
	}

    @Override
    public List<TComment> getbyiid(String userid,String iid){
        String hql="from TComment t where t.iid ='"+iid+"' and t.userid ="+userid+"";
        List<TComment> comments=commentDao.find(hql);

        return comments;
    }
    @Override
    public List<TComment> getTComments(String maxsize,String curentpage,String iid){
        PageHelper pageHelper=new PageHelper();
        pageHelper.setSort(" order by t.comenttime desc ");
        pageHelper.setPage(Integer.parseInt(curentpage));
        pageHelper.setRows(Integer.parseInt(maxsize));

        String hql = " from TComment t where t.iid='"+iid+"'";
        List<TComment> ul = new ArrayList<TComment>();
        List<TComment> l = commentDao.find(hql + orderHql(pageHelper), pageHelper.getPage(), pageHelper.getRows());
        if (l != null && l.size() > 0) {
            for (TComment t : l) {
                TComment u = new TComment();
                BeanUtils.copyProperties(t, u);
                ul.add(u);
            }
        }

        return ul;
    }
	@Override
	public List<TComment> getTComments(String maxsize,String curentpage){
		
		PageHelper pageHelper=new PageHelper();
		pageHelper.setSort(" order by t.comenttime desc ");
		pageHelper.setPage(Integer.parseInt(curentpage));
		pageHelper.setRows(Integer.parseInt(maxsize));
		
		String hql = " from TComment t ";
		List<TComment> ul = new ArrayList<TComment>();
		List<TComment> l = commentDao.find(hql + orderHql(pageHelper), pageHelper.getPage(), pageHelper.getRows());
		if (l != null && l.size() > 0) {
			for (TComment t : l) {
				TComment u = new TComment();
				BeanUtils.copyProperties(t, u);
				ul.add(u);
			}
		}
		
		return ul;
	}

	@Override
	public TComment get(String id) {

		String hql="from TComment t where t.id ='"+id+"'";
		List<TComment> comments=commentDao.find(hql);
		if(comments.size()>0){
			return comments.get(0);
		}
		return null;
	}
	@Override
	public void update(TComment comment){
		commentDao.update(comment);
		
	}
}
	