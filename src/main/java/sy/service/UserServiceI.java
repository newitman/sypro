package sy.service;

import java.util.List;

import sy.model.Tuser;
import sy.pageModel.DataGrid;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;

/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface UserServiceI {

    /*
    通过userid获取用户
     */
    public Tuser getUserByUserId(String userid);
    
    /*
    通过name获取用户
     */
    public  Tuser TusergetUserByNickname(String nickname);
	/**
	 * 用户登录
	 * 
	 * @param user
	 *            里面包含登录名和密码
	 * @return 用户对象
	 */
	public User login(User user);
	
	public User loginadmin(User user);

	/**
	 * 用户注册
	 * 
	 * @param user
	 *            里面包含登录名和密码
	 * @throws Exception
	 */
	public void regonce(User user) throws Exception;

	//判断用户是否绑定支付宝账户和手机号码
	public boolean jugeAcount(User user) throws Exception;
	
	/**
	 * 获取用户数据表格
	 * 
	 * @param user
	 * @return
	 */
	public DataGrid dataGrid(User user, PageHelper ph);

	/**
	 * 添加用户
	 * 
	 * @param user
	 */
	public void add(User user) throws Exception;
	/**
	 * 获得用户对象
	 * 
	 * @param id
	 * @return
	 */
	public User get(String id);

	/**
	 * 编辑用户
	 * 
	 * @param user
	 */
	public void edit(User user) throws Exception;
	/**
	 * 后台编辑用户
	 * 
	 * @param user
	 */
	public void editadmin(User user) throws Exception;
	
	/**
	 * 后台编辑状态
	 * 
	 * @param user
	 */
	public void editstatu(User user) throws Exception;
	/**
	 * 编辑用户任务次数
	 * 
	 * @param user
	 */
	public void edittaskcount(User user) throws Exception;
	
	/**
	 * 编辑用户的级别
	 * 
	 * @param user
	 */
	public void editlevl(User user) throws Exception;
	
	/**
	 * 编辑用户增加积分
	 * 
	 * @param user
	 */
	public void editotlescore(User user) throws Exception;
	
	/**
	 * 编辑用户兑换积分
	 * 
	 * @param user
	 */
	public void editexchangescore(User user) throws Exception;
	
	
	/**
	 * 编辑用户邀请积分
	 * 
	 * @param user
	 */
	public void editinvitescore(User user) throws Exception;

	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	public void delete(String id);

	/**
	 * 用户授权
	 * 
	 * @param ids
	 * @param user
	 *            需要user.roleIds的属性值
	 */
	public void grant(String ids, User user);

	/**
	 * 获得用户能访问的资源地址
	 * 
	 * @param id
	 *            用户ID
	 * @return
	 */
	public List<String> resourceList(String id);

	/**
	 * 编辑用户密码
	 * 
	 * @param user
	 */
	public void editPwd(User user);

	/**
	 * 修改用户自己的密码
	 * 
	 * @param sessionInfo
	 * @param oldPwd
	 * @param pwd
	 * @return
	 */
	public boolean editCurrentUserPwd(SessionInfo sessionInfo, String oldPwd, String pwd);

	/**
	 * 用户登录时的autocomplete
	 * 
	 * @param q
	 *            参数
	 * @return
	 */
	public List<User> loginCombobox(String q);

	/**
	 * 用户登录时的combogrid
	 * 
	 * @param q
	 * @param ph
	 * @return
	 */
	public DataGrid loginCombogrid(String q, PageHelper ph);

	/**
	 * 用户创建时间图表
	 * 
	 * @return
	 */
	public List<Long> userCreateDatetimeChart();

	/**
	 * 用户统计注册数
	 * 
	 * @return
	 */
	
	public Long regonce();
	
	/**
	 * 用户忠诚榜
	 * 
	 * @return
	 */
	
	public List<User> fealtyrank();
	
	/**
	 * 用户富豪榜
	 * 
	 * @return
	 */
	
	
	public List<User> regalrank();
	
	
	/**
	 * 用户精英周榜
	 * 
	 * @return
	 */
	
	
	public List<User> outstandrank();

	/**
	 * 大神周榜
	 * 
	 * @return
	 */
	
	
	public List<User> weekrank();
	
	/**
	 * 查询密码
	 * 
	 * @return
	 */
	public String getpwd(User user);
	
	
	/**
	 * 老虎机
	 * 
	 * @return
	 */
	public void editrockscore(User user);
	
	/*
	 * 修改用户刮奖次数
	 */
	public void updateGglCount(String userid,long count);
	public Tuser getGglCount(String userid);
}
