package sy.service;

import sy.model.Tgonggao;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

import java.util.List;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */

/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface GonggaoServiceI {
	public void add(Tgonggao tgonggao) throws Exception;
	public DataGrid dataGrid(Tgonggao tgonggao, PageHelper ph);
	public void delete(String id);
	public List<Tgonggao> getTgonggaos();
    public Tgonggao get(String id);
    public void update(Tgonggao tgonggao);
}
