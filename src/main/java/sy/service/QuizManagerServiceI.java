package sy.service;
import sy.model.TQuizManager;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

import java.util.List;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface QuizManagerServiceI {
	public TQuizManager add(TQuizManager quizManager) throws Exception;
	public DataGrid dataGrid(TQuizManager quizManager, PageHelper ph);
	public void delete(String id);
	public TQuizManager get(String tqid);
	public void update(TQuizManager quizManager);
	public List<TQuizManager> getQuizManagers();
	public List<TQuizManager> getQuiz(String number,String starttime,String endtime);
	public TQuizManager getLastTQuizManager();
	public TQuizManager getLastAfterTQuizManager();
    public TQuizManager getQHQuizManager(String qh);//通过期号
}
