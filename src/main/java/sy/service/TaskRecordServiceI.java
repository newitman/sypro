package sy.service;
import java.util.Date;
import java.util.List;

import sy.model.Taskrecord;
import sy.pageModel.DataGrid;
import sy.pageModel.MTasktype;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface TaskRecordServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mtaskrecord taskrecord) throws Exception;
	/**
	 * 查询任务记录
	 * @param taskrecord
	 */
	public List<Taskrecord> gettask(Long userid) throws Exception;
	/**
	 * 检查签到当天记录是否存在
	 * @param taskrecord
	 */
	public int getexistday(Long userid,Date date);
	/**
	 * 检查签到当天是否摇一摇
	 * @param taskrecord
	 */
	public int getexistshake(Long userid,Date date);
	/**
	 * 检查摇杆机每天不超过两百次
	 * @param taskrecord
	 */
	public int getexistrock(Long userid,Date date);
	/**
	 * 检查用户升级
	 * @param taskrecord
	 */
	public int checklevl(Long userid);
	
	/**
	 * 检查礼包是否已领取
	 * @param taskrecord
	 */
	public int checkgif(Long userid,Integer lev);
	
	/**
	 * 查询数据
	 * @param taskrecord
	 */
	
	public DataGrid dataGrid(Mtaskrecord task, PageHelper ph);
	
	/**
	 * 检查用户升级
	 * @param taskrecord
	 */
	public int checkscore(Long userid,String typeid);
	
	/**
	 * 删除任务
	 * 
	 * @param id
	 */
	public void delete(String id);
	
	public int checklevlws(Long userid);
	
	/**
	 * 每天控制积分量
	 * 
	 * @param id
	 */
	public int excetionscore(Long userid,String typeid);
}
