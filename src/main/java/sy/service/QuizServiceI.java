package sy.service;
import sy.model.TQuiz;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

import java.util.List;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface QuizServiceI {
	public void add(TQuiz quiz) throws Exception;
	public DataGrid dataGrid(TQuiz quiz, PageHelper ph);
	public void delete(String id);
	public List<TQuiz> getQuiz(String userid,String createdatetime);
	public int updateAllStatus(String periodnumber,String number);
	public int getPeriodnumber(String periodnumber);
	public List<TQuiz> getQuizByPN(String periodnumber,String number);
	public List<TQuiz> getQuizByPU(String periodnumber,String lastnumber);
	public int updateStatus(String periodnumber,String status,String userid);
}
