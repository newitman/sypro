package sy.service;
import java.util.List;

import sy.model.Texchange;
import sy.model.Texchangetype;
import sy.pageModel.DataGrid;
import sy.pageModel.Mexchange;
import sy.pageModel.Mexchangetype;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
/**
 * 兑换类型Service
 * 
 * @author 甘涛
 * 
 */
public interface ExchangetypeServiceI {
	
	public Texchangetype getexchange(String  Typeid) throws Exception;
	
	public DataGrid dataGrid(Mexchangetype  changetype, PageHelper ph);
	
	public int 	getexchangesize() throws Exception;
	
	public void add(Mexchangetype exchange) throws Exception;
	
	public void edit(Mexchangetype exchange) throws Exception;
	
	public List<Mexchangetype> Combobox(String q);
	
}
