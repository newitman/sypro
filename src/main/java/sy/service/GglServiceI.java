package sy.service;
import java.util.List;

import sy.model.Tggl;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface GglServiceI {
	public void add(Tggl tggl) throws Exception;
	public DataGrid dataGrid(Tggl tggl, PageHelper ph);
	public void delete(String id);
	public List<Tggl> getTggl();
	public Tggl getLastGgl();
	public void updateGgl(Tggl tggl);
}
