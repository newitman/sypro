package sy.service;
import sy.pageModel.Mshake;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface ShakeServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mshake shake) throws Exception;
	
}
