package sy.service;

/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */

import sy.model.TPush;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface PushServiceI {
    public void add(TPush tPush) throws Exception;
    public DataGrid dataGrid(TPush tPush, PageHelper ph);
    public void delete(String id);
    public TPush get(String id);
}
