package sy.service;
import java.util.List;

import sy.model.Tinteract;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface InteractServiceI {
	public void add(Tinteract tinteract) throws Exception;
	public DataGrid dataGrid(Tinteract tinteract, PageHelper ph);
	public void delete(String id);
	public List<Tinteract> getTinteracts(String maxsize,String curentpage);
    public List<Tinteract> getTinteracts(String maxsize,String curentpage,String date);
    public Tinteract get(String id);

}
