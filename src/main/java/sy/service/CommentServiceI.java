package sy.service;
import java.util.List;

import sy.model.TComment;
import sy.model.Tappinfo;
import sy.model.Tinteract;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface CommentServiceI {
	public void add(TComment comment) throws Exception;
	public DataGrid dataGrid(TComment comment, PageHelper ph);
	public void delete(String id);
	public List<TComment> getComments();
	public List<TComment> getTComments(String maxsize,String curentpage);
    public List<TComment> getTComments(String maxsize,String curentpage,String iid);
	public TComment get(String id);
    public List<TComment> getbyiid(String userid,String iid);
	public void update(TComment comment);
	public int getcommentcount(String iid);
	public int getlikecount(String iid);
    public TComment getTComment(String uid,String iid);
    public List<TComment> getTComments(String iid);
}
