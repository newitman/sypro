package sy.service;
import sy.model.Tsuggestion;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface SuggestionAdServiceI {
	public void add(Tsuggestion tsuggestion) throws Exception;
	public DataGrid dataGrid(Tsuggestion tsuggestion, PageHelper ph);
	public void delete(String id);

}
