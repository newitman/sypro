package sy.service;
import sy.pageModel.Mcontact;
import sy.pageModel.Mgif;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface ContactServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mcontact contact) throws Exception;
	
}
