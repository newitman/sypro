package sy.service;
import java.util.Date;
import java.util.List;

import sy.model.Taskrecord;
import sy.model.Tasktype;
import sy.model.Texchangetype;
import sy.pageModel.DataGrid;
import sy.pageModel.MTasktype;
import sy.pageModel.Mexchangetype;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
/**
 * 任务类型Service
 * 
 * @author 孙宇
 * 
 */
public interface TaskTypeServiceI {
	/**
	 * 查询任务类型
	 * @param taskrecord
	 */
	public Tasktype gettasktype(String  Typeid) throws Exception;
	
	public DataGrid dataGrid(MTasktype task, PageHelper ph);
	
	public int 			 gettasksize() throws Exception;
	
	public void add(MTasktype tasktype) throws Exception;
	
	public void edit(MTasktype tasktype) throws Exception;
	
}
