package sy.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import sy.model.Taskrecord;
import sy.model.Tinviterecord;
import sy.pageModel.Rinviterecord;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface InviteServiceI {


    //获取该用户下面所有的推广下线
    public List<Tinviterecord> getTinviterecordCount(String userid);
    
    //获取该用户下面本周的推广人数
    public int getTinviterecordWeekCount(String userid,String target);
	/**
	 * 添加邀请记录
	 * @param inviterecord
	 */
	public void add(Rinviterecord inviterecord) throws Exception;
	
	/*
	 * 不能相互邀请
	 * */
	public boolean inviteme(String userid, String invited) throws Exception;
	/**
	 * 查询任务记录
	 * @param taskrecord
	 */
	public List<Tinviterecord> gettask(String userid) throws Exception;

    /*
        查询当前用户上一级用户邀请记录
     */
	public Tinviterecord getUpClass(String userid) throws  Exception;

    /*
       查询是否已经有过邀请记录
    */
    public List<Tinviterecord> getTinviterecord(Map<String, Object> parames) throws  Exception;
    
    /*
      查询根据用户查询邀请人数
    */
    public String getCount(String userid) throws  Exception;

    public void delete(String id) throws  Exception;
    
    public int QueryUnlineCount(String userid,String target) throws  Exception;
}
