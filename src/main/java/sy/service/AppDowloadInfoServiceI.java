package sy.service;
import java.util.List;

import sy.model.Tappdowloadinfo;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface AppDowloadInfoServiceI {
	public void add(Tappdowloadinfo tappdowloadinfo) throws Exception;
	public DataGrid dataGrid(Tappdowloadinfo tappdowloadinfo, PageHelper ph);
	public void delete(String id);
	public List<Tappdowloadinfo> getTappdowloadinfos();
	public List<Tappdowloadinfo> getTappdowloadinfo(String userid,String appid);
}
