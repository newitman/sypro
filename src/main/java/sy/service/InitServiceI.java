package sy.service;

/**
 * 初始化数据库服务
 * 
 * @author 孙宇
 * 
 */
public interface InitServiceI {

	/**
	 * 初始化数据库
	 */
	public void init();
    //获取福彩3D彩票结果
    public void query3d();
}
