package sy.service;
import java.util.List;

import sy.model.Texchange;
import sy.pageModel.DataGrid;
import sy.pageModel.Mexchange;
import sy.pageModel.PageHelper;
import sy.pageModel.User;
/**
 * 用户Service
 * 
 * @author 甘涛
 * 
 */
public interface ExchangeServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mexchange exchange) throws Exception;
	/**
	 * 查询最近兑换记录
	 * @param 无
	 */
	public List<Texchange> getNewExchange() throws Exception;
	
	public List<Texchange> getexchange(String  userid) throws Exception;
	
	public DataGrid dataGrid(Mexchange change, PageHelper ph);
	
	public void updatestatu(String userid,String statu);
	
	public int  getCheckCount(String userid,String typeid);

	public void delete(String id);
	
	public Texchange getTexchangedetial(String id);
}
