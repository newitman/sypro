package sy.service;
import sy.model.Tgif;
import sy.pageModel.Mgif;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface GifServiceI {
	/**
	 * 添加栏目任务
	 * @param taskrecord
	 */
	public void add(Mgif gif) throws Exception;
	/**
	 * 查询礼包等级信息
	 * @param taskrecord
	 */
	public Tgif gifdetial(Integer lev) throws Exception;
	
}
