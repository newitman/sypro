package sy.service;
import sy.model.Tapk;
import sy.pageModel.DataGrid;
import sy.pageModel.PageHelper;

import java.util.List;
/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */

/**
 * 用户Service
 * 
 * @author 孙宇
 * 
 */
public interface ApkServiceI {
	public void add(Tapk tapk) throws Exception;
	public DataGrid dataGrid(Tapk tapk, PageHelper ph);
	public void delete(String id);
	public List<Tapk> getapks();
    public Tapk get(String id);
    public void update(Tapk tapk);
    public Tapk getLastTapk();
}
