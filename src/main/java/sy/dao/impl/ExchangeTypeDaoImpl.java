package sy.dao.impl;
import org.springframework.stereotype.Repository;

import sy.dao.ExchangeDaoI;
import sy.dao.ExchangeTypeDaoI;
import sy.dao.TaskRecordDaoI;
import sy.model.Taskrecord;
import sy.model.Texchange;
import sy.model.Texchangetype;
@Repository
public class ExchangeTypeDaoImpl extends BaseDaoImpl<Texchangetype> implements ExchangeTypeDaoI {

}
