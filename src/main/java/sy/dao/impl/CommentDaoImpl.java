package sy.dao.impl;
import org.springframework.stereotype.Repository;

import sy.dao.CommentDaoI;
import sy.model.TComment;
 
@Repository
public class CommentDaoImpl extends BaseDaoImpl<TComment> implements CommentDaoI {
}
