package sy.dao.impl;
import org.springframework.stereotype.Repository;

import sy.dao.TsuggestionDaoI;
import sy.model.Tsuggestion;
 
@Repository
public class TsuggestionDaoImpl extends BaseDaoImpl<Tsuggestion> implements TsuggestionDaoI {
}
