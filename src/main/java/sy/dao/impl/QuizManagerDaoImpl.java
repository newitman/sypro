package sy.dao.impl;
import org.springframework.stereotype.Repository;

import sy.dao.QuizManagerDaoI;
import sy.model.TQuizManager;
 
@Repository
public class QuizManagerDaoImpl extends BaseDaoImpl<TQuizManager> implements QuizManagerDaoI {
}
