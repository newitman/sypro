package sy.dao.impl;
import org.springframework.stereotype.Repository;
import sy.dao.PushDaoI;
import sy.model.TPush;

/**
 * 用户数据库操作类
 * 
 * @author 
 * 
 */
@Repository
public class PushDaoImpl extends BaseDaoImpl<TPush> implements PushDaoI {

}
