package sy.dao.impl;
import org.springframework.stereotype.Repository;

import sy.dao.TaskRecordDaoI;
import sy.dao.TaskTypeDaoI;
import sy.model.Taskrecord;
import sy.model.Tasktype;
@Repository
public class TaskTypeDaoImpl extends BaseDaoImpl<Tasktype> implements TaskTypeDaoI {

}
