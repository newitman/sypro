package sy.dao;

import sy.model.Tresource;

/**
 * 资源数据库操作类
 * 
 * @author ly
 * 
 */
public interface ResourceDaoI extends BaseDaoI<Tresource> {

}
