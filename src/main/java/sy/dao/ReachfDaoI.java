package sy.dao;
import sy.model.Taskrecord;
import sy.model.Tgif;
import sy.model.Treach;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface ReachfDaoI extends BaseDaoI<Treach> {
}
