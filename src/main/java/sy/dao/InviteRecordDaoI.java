package sy.dao;
import sy.model.Taskrecord;
import sy.model.Tinviterecord;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface InviteRecordDaoI extends BaseDaoI<Tinviterecord> {
}
