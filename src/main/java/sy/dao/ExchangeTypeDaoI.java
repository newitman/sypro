package sy.dao;
import sy.model.Texchange;
import sy.model.Texchangetype;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface ExchangeTypeDaoI extends BaseDaoI<Texchangetype> {
}
