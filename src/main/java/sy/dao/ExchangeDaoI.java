package sy.dao;
import sy.model.Texchange;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface ExchangeDaoI extends BaseDaoI<Texchange> {
}
