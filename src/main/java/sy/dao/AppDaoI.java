package sy.dao;
import sy.model.Tappcheck;
import sy.model.Taskrecord;
import sy.model.Tgif;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface AppDaoI extends BaseDaoI<Tappcheck> {
}
