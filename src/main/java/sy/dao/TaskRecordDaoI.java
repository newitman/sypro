package sy.dao;
import sy.model.Taskrecord;
/**
 * 用户数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface TaskRecordDaoI extends BaseDaoI<Taskrecord> {
}
