package sy.pageModel;
public class Mgif implements java.io.Serializable {
	private String 		id;
	private Integer		lev;
	private Integer		score;
	public Mgif() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	 
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getLev() {
		return lev;
	}
	public void setLev(Integer lev) {
		this.lev = lev;
	}
	public Mgif(String id, Integer lev, Integer score) {
		super();
		this.id = id;
		this.lev = lev;
		this.score = score;
	}
}
