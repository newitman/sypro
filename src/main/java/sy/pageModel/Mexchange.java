package sy.pageModel;

import java.util.Date;

import javax.persistence.Column;
 
public class Mexchange implements java.io.Serializable {
	private String 		id;
	private String		userid;
	private String	    type;
	private String 	    qq;
	private String      cellPhone;
	private Integer     goods;
	private String 		alipay;
	private Date		exchangetime;
	private String 		exchangecontent;
	private String 		nickname;
	private Integer		score;
	private String		displaytime;
	private String 		changestatus;
	
	private String 		nicknamecp;
	private String 		useridcp;
	public String getUseridcp() {
		return useridcp;
	}

	public void setUseridcp(String useridcp) {
		this.useridcp = useridcp;
	}
	 
	public String getNicknamecp() {
		return nicknamecp;
	}
	public void setNicknamecp(String nicknamecp) {
		this.nicknamecp = nicknamecp;
	}

	
	public String getChangestatus() {
		return changestatus;
	}
	public void setChangestatus(String changestatus) {
		this.changestatus = changestatus;
	}
	public String getDisplaytime() {
		return displaytime;
	}
	public void setDisplaytime(String displaytime) {
		this.displaytime = displaytime;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getExchangecontent() {
		return exchangecontent;
	}
	public void setExchangecontent(String exchangecontent) {
		this.exchangecontent = exchangecontent;
	}
	public Date getExchangetime() {
		return exchangetime;
	}
	public void setExchangetime(Date exchangetime) {
		this.exchangetime = exchangetime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public Integer getGoods() {
		return goods;
	}
	public void setGoods(Integer goods) {
		this.goods = goods;
	}
	public String getAlipay() {
		return alipay;
	}
	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}
	
	
	
}
