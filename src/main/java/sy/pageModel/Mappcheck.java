package sy.pageModel;
import java.util.Date;
public class Mappcheck implements java.io.Serializable {
	private String 		id;
	private String		voish;
	private String      linkadress;
	private Date	    publishtime;
	public Mappcheck() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	 
	public String getVoish() {
		return voish;
	}
	public void setVoish(String voish) {
		this.voish = voish;
	}
	 
	public String getLinkadress() {
		return linkadress;
	}
	public void setLinkadress(String linkadress) {
		this.linkadress = linkadress;
	}
	 
	public Date getPublishtime() {
		return publishtime;
	}
	public void setPublishtime(Date publishtime) {
		this.publishtime = publishtime;
	}
	public Mappcheck(String id, String voish, String linkadress,
			Date publishtime) {
		super();
		this.id = id;
		this.voish = voish;
		this.linkadress = linkadress;
		this.publishtime = publishtime;
	}
}
