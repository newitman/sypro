package sy.pageModel;

public class ApkVersion implements java.io.Serializable {

	private String ver;
	private String path;
    private String bugcontent;//修复bug内容信息
    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getBugcontent() {
        return bugcontent;
    }

    public void setBugcontent(String bugcontent) {
        this.bugcontent = bugcontent;
    }
}
