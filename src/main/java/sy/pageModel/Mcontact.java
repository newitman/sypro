package sy.pageModel;
import java.util.Date;
public class Mcontact implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private String	    content;
	private Date		contime;
	public Date getContime() {
		return contime;
	}
	public void setContime(Date contime) {
		this.contime = contime;
	}
 
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Mcontact() {
		super();
	}
	 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
}
