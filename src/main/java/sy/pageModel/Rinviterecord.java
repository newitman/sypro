package sy.pageModel;
public class Rinviterecord implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private String 		id;
	private String		userid;
	private String		inviteuserid;
	private Integer     score;
	private Integer     realscore;
	private String 		imei;
	private String		nickname;
	private String		registertime;
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getRegistertime() {
		return registertime;
	}
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}
	public Rinviterecord() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getInviteuserid() {
		return inviteuserid;
	}
	public void setInviteuserid(String inviteuserid) {
		this.inviteuserid = inviteuserid;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getRealscore() {
		return realscore;
	}
	public void setRealscore(Integer realscore) {
		this.realscore = realscore;
	}
	public Rinviterecord(String id, String userid, String inviteuserid,
			 Integer score, Integer realscore,String imei,String nickname,String registertime) {
		super();
		this.imei = imei;
		this.id = id;
		this.userid = userid;
		this.inviteuserid = inviteuserid;
		this.score = score;
		this.realscore = realscore;
		this.nickname = nickname;
		this.registertime = registertime;
	}
}
