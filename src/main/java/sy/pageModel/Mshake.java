package sy.pageModel;
import java.util.Date;
public class Mshake implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private Integer		score;
	private Date	    shaketime;
	public Mshake() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Date getShaketime() {
		return shaketime;
	}
	public void setShaketime(Date shaketime) {
		this.shaketime = shaketime;
	}
	public Mshake(String id, Long userid, Integer score, Date shaketime) {
		super();
		this.id = id;
		this.userid = userid;
		this.score = score;
		this.shaketime = shaketime;
	}
}
