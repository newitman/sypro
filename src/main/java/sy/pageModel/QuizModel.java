package sy.pageModel;

public class QuizModel implements java.io.Serializable {

	private static final long serialVersionUID = -273287374512951877L;

	//当前轮信息
	private String endtime;//结束时间 
	private String gold;//奖池学币
	private String personcount;//参与人数 
	private int isquiz;//是否可以竞猜 0:竞猜结束 1:可以竞猜
	private String periodnumber;//当前期号
	
	//上一轮中奖信息
	private String  number;//中奖号码 
	private String lastgold;//上一轮奖励 
	private String lastperson;//上一轮中奖奖励 
	private String getgolds;//中奖用户ID s
	
	
	public String getPeriodnumber() {
		return periodnumber;
	}
	public void setPeriodnumber(String periodnumber) {
		this.periodnumber = periodnumber;
	}
	public int getIsquiz() {
		return isquiz;
	}
	public void setIsquiz(int isquiz) {
		this.isquiz = isquiz;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getGold() {
		return gold;
	}
	public void setGold(String gold) {
		this.gold = gold;
	}
	public String getPersoncount() {
		return personcount;
	}
	public void setPersoncount(String personcount) {
		this.personcount = personcount;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getLastgold() {
		return lastgold;
	}
	public void setLastgold(String lastgold) {
		this.lastgold = lastgold;
	}
	public String getLastperson() {
		return lastperson;
	}
	public void setLastperson(String lastperson) {
		this.lastperson = lastperson;
	}
	public String getGetgolds() {
		return getgolds;
	}
	public void setGetgolds(String getgolds) {
		this.getgolds = getgolds;
	}
	
	
}
