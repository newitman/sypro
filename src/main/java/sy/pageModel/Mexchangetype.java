package sy.pageModel;
public class Mexchangetype implements java.io.Serializable {
	private String 		id;
	private String		typeid;
	private String 	    typecontent;
	private String 		unit;
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getTypecontent() {
		return typecontent;
	}
	public void setTypecontent(String typecontent) {
		this.typecontent = typecontent;
	}
	
}
