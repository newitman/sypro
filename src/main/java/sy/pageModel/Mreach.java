package sy.pageModel;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

public class Mreach implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private Date	    reachTime;
	private Integer 	score;
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Mreach() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Date getReachtime() {
		return reachTime;
	}
	public void setreachtime(Date reachTime) {
		this.reachTime = reachTime;
	}
	public Mreach(String id, Long userid, Date reachTime) {
		super();
		this.id = id;
		this.userid = userid;
		this.reachTime = reachTime;
	}

	
}
