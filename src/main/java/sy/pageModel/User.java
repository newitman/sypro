package sy.pageModel;

import java.util.Date;

public class User implements java.io.Serializable {
	private String	   id;
	private Long	   userid;
	private String     name;
	private String 	   pwd;
	private Integer    totalscore;
	private Integer    changedscore;
	private Integer    unchangedscore;
	private Integer    taskcount;
	private Integer    changedcount;
	private Integer    invitecount;
	private Integer    scorefrominvite;
	private String 	   phonenumber;
	private String 	   qq;
	private String 	   alipay;
	private Date 	   createdatetime;
	private Date 	   modifydatetime;
	private Date 	   createdatetimeStart;
	private Date 	   createdatetimeEnd;
	private Date 	   modifydatetimeStart;
	private Date 	   modifydatetimeEnd;
	private String     roleIds;
	private String     roleNames;
	private Integer	   levl;
	private Integer    orderid;
	private String 	   statu;
	private String     unline;
    public String getUnline() {
		return unline;
	}

	public void setUnline(String unline) {
		this.unline = unline;
	}

	public String getStatu() {
		return statu;
	}

	public void setStatu(String statu) {
		this.statu = statu;
	}
    private int clienttype;//客户端类型 0：手机 1：浏览器

    //用户能使用的刮刮奖次数
    private int ggjnunber;

    public int getClienttype() {
        return clienttype;
    }

    public void setClienttype(int clienttype) {
        this.clienttype = clienttype;
    }

    public int getGgjnunber() {
        return ggjnunber;
    }

    public void setGgjnunber(int ggjnunber) {
        this.ggjnunber = ggjnunber;
    }

    public Integer getOrderid() {
		return orderid;
	}
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}
	public Integer getLevl() {
		return levl;
	}
	public void setLevl(Integer levl) {
		this.levl = levl;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Integer getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}

	public Integer getChangedscore() {
		return changedscore;
	}

	public void setChangedscore(Integer changedscore) {
		this.changedscore = changedscore;
	}

	public Integer getUnchangedscore() {
		return unchangedscore;
	}

	public void setUnchangedscore(Integer unchangedscore) {
		this.unchangedscore = unchangedscore;
	}

	public Integer getTaskcount() {
		return taskcount;
	}

	public void setTaskcount(Integer taskcount) {
		this.taskcount = taskcount;
	}
	public Integer getChangedcount() {
		return changedcount;
	}
	public void setChangedcount(Integer changedcount) {
		this.changedcount = changedcount;
	}

	public Integer getInvitecount() {
		return invitecount;
	}

	public void setInvitecount(Integer invitecount) {
		this.invitecount = invitecount;
	}

	public Integer getScorefrominvite() {
		return scorefrominvite;
	}

	public void setScorefrominvite(Integer scorefrominvite) {
		this.scorefrominvite = scorefrominvite;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	
	public Date getCreatedatetimeStart() {
		return createdatetimeStart;
	}

	public void setCreatedatetimeStart(Date createdatetimeStart) {
		this.createdatetimeStart = createdatetimeStart;
	}

	public Date getCreatedatetimeEnd() {
		return createdatetimeEnd;
	}

	public void setCreatedatetimeEnd(Date createdatetimeEnd) {
		this.createdatetimeEnd = createdatetimeEnd;
	}

	public Date getModifydatetimeStart() {
		return modifydatetimeStart;
	}

	public void setModifydatetimeStart(Date modifydatetimeStart) {
		this.modifydatetimeStart = modifydatetimeStart;
	}

	public Date getModifydatetimeEnd() {
		return modifydatetimeEnd;
	}

	public void setModifydatetimeEnd(Date modifydatetimeEnd) {
		this.modifydatetimeEnd = modifydatetimeEnd;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	public String getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Date getCreatedatetime() {
		return createdatetime;
	}

	public void setCreatedatetime(Date createdatetime) {
		this.createdatetime = createdatetime;
	}

	public Date getModifydatetime() {
		return modifydatetime;
	}

	public void setModifydatetime(Date modifydatetime) {
		this.modifydatetime = modifydatetime;
	}

}
