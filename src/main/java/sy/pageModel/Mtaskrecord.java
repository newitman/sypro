package sy.pageModel;
import java.util.Date;
public class Mtaskrecord implements java.io.Serializable {
	private String 		id;
	private Long		userid;
	private String	    taskType;
	private Date 	    finishTime;
	private Integer     score;
	private Integer		giflev;
	private String		displaytime;
	private Date		finishDate;
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	public String getDisplaytime() {
		return displaytime;
	}
	public void setDisplaytime(String displaytime) {
		this.displaytime = displaytime;
	}
	public Integer getGiflev() {
		return giflev;
	}
	public void setGiflev(Integer giflev) {
		this.giflev = giflev;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
}
