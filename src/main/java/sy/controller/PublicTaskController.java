package sy.controller;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sy.model.Taskrecord;
import sy.model.Tinviterecord;
import sy.model.Tuser;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PublicTask;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.InviteServiceI;
import sy.service.ReachServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 公共平台任务记录
 * 
 * @author 孙宇
 * 
 */
@Controller
@RequestMapping("/publictaskController")
public class PublicTaskController extends BaseController {
	@Autowired
	private ReachServiceI reachService;
	
	@Autowired
	private TaskRecordServiceI taskrecordService;
	
	@Autowired
	private UserController usercontrol;

    @Autowired
    private InviteServiceI inviteService;

    @Autowired
    private UserServiceI userService;

	/**
	 * 公共积分处理发方法
	 * 
	 * @param
	 * 
	 */
	@ResponseBody
	@RequestMapping("/addpublictask")
	public String addtaskrecord(PublicTask pub, HttpSession session) {
		JSONObject jsobj = new JSONObject();
		SessionInfo sessionInfo = (SessionInfo)session.getAttribute(ConfigUtil.getSessionInfoName());
		if(sessionInfo == null){
			jsobj.put("status", -1);
			jsobj.put("msg", "登录超时");
		}else{
		if(pub.getVersion()!=null){
			try {
	            //ly-start 判断用户是否被邀请
	            getMoenyTo(pub.getScore(),sessionInfo);
	            //ly -end
	            int score = 0;
	            Mtaskrecord  task = new Mtaskrecord();
	        	//得到总积分减去已经获得的
	        	score = pub.getScore();
	        	if(score > 0){
	        		 if(taskrecordService.excetionscore(sessionInfo.getUserid(), pub.getTypeid())<7){
	        			 task.setTaskType(pub.getTypeid());						//设置类型
		     			 task.setUserid(sessionInfo.getUserid());				//设置用户id
		     			 task.setScore(score);									//设置积分
		     			 taskrecordService.add(task);
		     			 usercontrol.updatetotalscore(sessionInfo.getId(),score, 1);//更新积分
		    			 jsobj.put("status", 1);
		    			 jsobj.put("msg", score); 
	        		 } 
	        	 }else{
	        		 jsobj.put("status", 1);
	    			 jsobj.put("msg", 0);
	        	 } 
			} catch (Exception e) {
				e.printStackTrace();
				jsobj.put("status", 0);
				jsobj.put("msg", "系统错误");
			}
		}else{
			jsobj.put("status", 0);
			jsobj.put("msg", "请更新新版本");
		}
	  }
	  return jsobj.toJSONString();
	}

    //三级分成
    public void getMoenyTo(Integer pub,SessionInfo sessionInfo){
        try{
            //定义三级推广用户所加积分变量
            long gold1=0;

            Tinviterecord tinviterecord=inviteService.getUpClass(sessionInfo.getUserid()+"");
            if(tinviterecord!=null){//改用户是被邀请  第一次做人要 7W
                //判断是否第一次做任务
                List<Taskrecord> taskrecords= taskrecordService.gettask(sessionInfo.getUserid());
                if(taskrecords.size()<=0){//第一次做任务
                    gold1+=70000;

                }
                //获得这个用户推广向上三级 用户
                List<Tinviterecord> tinviterecords= getTGClass(sessionInfo.getUserid()+"");
                long gold=0;
                for(int i=0;i<tinviterecords.size();i++){
                    Tuser u = null;
                    User user = new User();
                    if(i==0){
                        u=userService.getUserByUserId(tinviterecords.get(i).getUserid());
                        double t= pub*0.35;
                        gold= (long)t+gold1;//加上第一次做任务积分 没有加0

                        user.setId(u.getId());
                        user.setTotalscore(u.getTotalscore()+(int)gold);
                        user.setUnchangedscore(u.getUnchangedscore()+(int)gold);
                        userService.editotlescore(user);

                        //     usercontrol.updatetotalscore(u.getId(),(int)gold , 1);//更新积分

                    }else if(i==1){
                        u=userService.getUserByUserId(tinviterecords.get(i).getUserid());
                        double t= pub*0.1;

                        user.setId(u.getId());
                        user.setTotalscore(u.getTotalscore()+(int)t);
                        user.setUnchangedscore(u.getUnchangedscore()+(int)t);
                        userService.editotlescore(user);

                    }else if(i==2){
                        u=userService.getUserByUserId(tinviterecords.get(i).getUserid());
                        double t= pub*0.05;
                        user.setId(u.getId());
                        user.setTotalscore(u.getTotalscore()+(int)t);
                        user.setUnchangedscore(u.getUnchangedscore()+(int)t);
                        userService.editotlescore(user);

                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //userid:邀请人id inviteuserid：被邀请人id 获得被邀请人的所有父级邀请人id 向上面找三级
    public List<Tinviterecord> getTGClass(String userid)throws Exception{

        List<Tinviterecord> tinviterecords=new ArrayList<Tinviterecord>();

        Tinviterecord tinviterecord1=null;
        Tinviterecord tinviterecord2=null;
        Tinviterecord tinviterecord3=null;
        //如果不为空表示存在上级推广  第一级
        tinviterecord1=inviteService.getUpClass(userid);

        if(tinviterecord1!=null){
            tinviterecords.add(tinviterecord1);
            //找第二级
            tinviterecord2= inviteService.getUpClass(tinviterecord1.getUserid());
            if(tinviterecord2!=null){
                tinviterecords.add(tinviterecord2);
                //找第三级
                tinviterecord3= inviteService.getUpClass(tinviterecord2.getUserid());
                if(tinviterecord3!=null){
                    tinviterecords.add(tinviterecord3);
                }
            }

        }

        return tinviterecords;
    }

}
