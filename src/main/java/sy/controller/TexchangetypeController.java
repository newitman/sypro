package sy.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.Taskrecord;
import sy.model.Texchange;
import sy.model.Texchangetype;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.Mexchange;
import sy.pageModel.Mexchangetype;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.ExchangeServiceI;
import sy.service.ExchangetypeServiceI;
import sy.service.ResourceServiceI;
import sy.service.RoleServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;
import sy.util.IpUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 兑换控制器
 * 
 * @author 甘涛
 * 
 */
@Controller
@RequestMapping("/texchangetypeController")
public class TexchangetypeController extends BaseController {
	@Autowired
	private  ExchangetypeServiceI exchangetypeService;
	 
	/**
	 * 获取数据类型管理
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/datatype")
	@ResponseBody
	public DataGrid  mangetypedata(Mexchangetype ex, PageHelper ph) {
		return exchangetypeService.dataGrid(ex, ph);
	}
	 

	/**
	 * 跳转修改类型界面
	 * 
	 * @return
	 */
	@RequestMapping("/editPage")
	public String editPage(HttpServletRequest request, String id) {
		Texchangetype u;
		try {
			u = exchangetypeService.getexchange(id);
			request.setAttribute("exchange", u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/exchange/exchangetype/exchangetypeEdit";
	}
	
	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(Mexchangetype changetype) {
		Json j = new Json();
		try {
			exchangetypeService.edit(changetype);
			j.setSuccess(true);
			j.setMsg("编辑成功！");
			j.setObj(changetype);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
		return j;
	}

	/**
	 * 跳转到添加用户页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		Mexchangetype u = new Mexchangetype();
		u.setId(UUID.randomUUID().toString());
		int pagecount = 0;
		try {
			pagecount = exchangetypeService.getexchangesize();
			pagecount=pagecount+1;
			u.setTypeid(pagecount+"");
			request.setAttribute("exchange", u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/exchange/exchangetype/exchangtypeAdd";
	}

	/**
	 * 添加类型
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Json add(Mexchangetype exchange) {
		Json j = new Json();
		try {
			exchangetypeService.add(exchange);
			j.setSuccess(true);
			j.setMsg("添加成功！");
			j.setObj(exchange);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 获取兑换类型
	 * 
	 * @param q
	 *            参数
	 * @return
	 */
	@RequestMapping("/Combobox")
	@ResponseBody
	public List<Mexchangetype> Combobox(String q) {
		return exchangetypeService.Combobox(q);
	}
}
