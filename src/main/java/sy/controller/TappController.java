package sy.controller;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.Mappcheck;
import sy.pageModel.Mgif;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.AppServiceI;
import sy.service.GifServiceI;
import sy.service.ResourceServiceI;
import sy.service.RoleServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;
import sy.util.IpUtil;

import com.alibaba.fastjson.JSON;

/**
 * 用户控制器
 * 
 * @author 孙宇
 * 
 */
@Controller
@RequestMapping("/tappController")
public class TappController extends BaseController {
	@Autowired
	private AppServiceI appService;
	/**
	 * 添加礼包
	 * 
	 * @param id
	 * 
	 */
	@ResponseBody
	@RequestMapping("/addapp")
	public Json a(Mappcheck app, HttpSession session) {
		Json j = new Json();
		try {
			SessionInfo sessionInfo = (SessionInfo)session.getAttribute(ConfigUtil.getSessionInfoName());
			appService.add(app);
			j.setSuccess(true);
			j.setMsg("注册成功！新注册的用户没有任何权限，请让管理员赋予权限后再使用本系统！");
			//j.setObj(user);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
	  return j;
	}
}
