package sy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sy.service.InitServiceI;

import javax.servlet.http.HttpSession;

/**
 * 初始化数据库控制器
 * 
 * @author 孙宇
 * 
 */
@Controller
@RequestMapping("/initController")
public class InitController {

	@Autowired
	private InitServiceI initService;

	/**
	 * 初始化数据库后转向到首页
	 * 
	 * @return
	 */
	@RequestMapping("/init")
	public String init(HttpSession session) {
		if (session != null) {
			session.invalidate();
		}
		//initService.init();
       // initService.query3d();//每天获取福彩3D结果
		return "redirect:/";
	}

}
