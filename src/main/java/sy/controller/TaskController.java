package sy.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.Taskrecord;
import sy.model.Texchange;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.Mexchange;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.ResourceServiceI;
import sy.service.RoleServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.TaskTypeServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;
import sy.util.IpUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 任务记录控制器
 * 
 * @author 甘涛
 * 
 */
@Controller
@RequestMapping("/taskController")
public class TaskController extends BaseController {
	@Autowired
	private TaskRecordServiceI taskRecordService;
	
	@Autowired
	private TaskTypeServiceI   taskTypeService;
	
	/**
	 * 查询任务记录
	 * 
	 * @param q
	 * @param ph
	 * @return
	 */
	@RequestMapping("/findtask")
	@ResponseBody
	public String getnewexchange(HttpSession session,HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		JSONObject jsobj = new JSONObject();
		SessionInfo sessionInfo = (SessionInfo)session.getAttribute(ConfigUtil.getSessionInfoName());
		if(sessionInfo == null){
			jsobj.put("status", -1);
			jsobj.put("msg", "登录超时");
		}else{
		try {
			List<Taskrecord>  task = taskRecordService.gettask(sessionInfo.getUserid());
			List<Taskrecord>  taskcp = new ArrayList<Taskrecord>();
			for(Taskrecord taskobj :task){
				String tasttype = taskTypeService.gettasktype(taskobj.getTaskType()).getTypecontent();
				Taskrecord taskmy = new Taskrecord();
				taskmy.setFinishTime(taskobj.getFinishTime());
				taskmy.setScore(taskobj.getScore());
				taskmy.setTaskType(tasttype);
				taskmy.setDisplaytime(taskobj.getDisplaytime());
				taskcp.add(taskmy);
			}
			jsobj.put("status", "1");
			jsobj.put("msg", taskcp);
		} catch (Exception e) {
			e.printStackTrace();
			jsobj.put("status", "0");
			jsobj.put("msg","查询消息失败");
		}
		}
		return jsobj.toJSONString();
	}
	
	/**
	 * 获取用兑换数据
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/dataGrid")
	@ResponseBody
	public DataGrid dataGrid(Mtaskrecord task, PageHelper ph) {
		return taskRecordService.dataGrid(task, ph);
	}
	
	/**
	 * 跳转到兑换记录页面
	 * 
	 * @return
	 */
	@RequestMapping("/task")
	public String exchange() {
		return "/task/index";
	}
	
	/**
	 * 兑换类型管理
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/tasktype")
	public String  mangetype() {
		return "/task/tasktype/index";
	}
	
	  /**
     * 批量删除任务
     *
     * @param ids
     *            ('0','1','2')
     * @return
     */
    @RequestMapping("/batchDelete")
    @ResponseBody
    public Json batchDelete(String ids) {
        Json j = new Json();
        if (ids != null && ids.length() > 0) {
            for (String id : ids.split(",")) {
                if (id != null) {
                    this.delete(id);
                }
            }
        }
        j.setMsg("批量删除成功！");
        j.setSuccess(true);
        return j;
    }
	
    
   
    public Json delete(String id) {
   
        Json j = new Json();
        if (id != null) { 
        	taskRecordService.delete(id);
        }
        j.setMsg("删除成功！");
        j.setSuccess(true);
        return j;
    }
}
