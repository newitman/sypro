package sy.controller;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sy.model.Tgif;
import sy.pageModel.Mgif;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.GifServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;
import com.alibaba.fastjson.JSONObject;
/**
 * 添加礼包控制器
 * 
 * @author 甘涛
 * 
 */
@Controller
@RequestMapping("/tgitController")
public class TgitController extends BaseController {
	@Autowired
	private GifServiceI gifService;
	
	@Autowired
	private TaskRecordServiceI taskrecordService;
	
	@Autowired
	private UserController usercontrol;
	
	@Autowired
	private UserServiceI userservice;
	/**
	 * 添加礼包
	 * 
	 * @param id
	 * 
	 */
	@ResponseBody
	@RequestMapping("/addgif")
	public String addtaskrecord(Integer lev,Integer score, HttpSession session) {
		JSONObject jsobj = new JSONObject();
		try {
			SessionInfo sessionInfo = (SessionInfo)session.getAttribute(ConfigUtil.getSessionInfoName());
			if(sessionInfo == null){
				jsobj.put("status", -1);
				jsobj.put("msg", "登录超时");
			}else{
			User user = userservice.get(sessionInfo.getId());	//得到用户信息
			if(lev < user.getLevl()){
				int gifcount = taskrecordService.checkgif(sessionInfo.getUserid(), user.getLevl());
				if(gifcount == 0){
					Mtaskrecord  task = new Mtaskrecord();
					task.setTaskType("6");								//设置类型-领取礼包
					task.setUserid(sessionInfo.getUserid());			//设置用户id
					task.setScore(score);						//设置积分
					task.setGiflev(user.getLevl());
					taskrecordService.add(task);
					usercontrol.updatetotalscore(sessionInfo.getId(), score, 1);//更新积分
					jsobj.put("status", 1);
					jsobj.put("msg", score);	
				}else{
					jsobj.put("status", 0);
					jsobj.put("msg","你已经领取过啦!");	
				}
			}else{
				jsobj.put("status", 1);
				jsobj.put("msg","你的等级不够哟!");	
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsobj.put("status", 0);
			jsobj.put("msg", "系统错误");
		}
		return jsobj.toJSONString();
	}
}
