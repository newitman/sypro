package sy.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.TComment;
import sy.model.Tuser;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.CommentServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;

import com.alibaba.fastjson.JSONObject;
//互动广场评论
@Controller
@RequestMapping("/commentController")
public class CommentController extends BaseController {

	@Autowired
	private UserServiceI userService;
	@Autowired
	private CommentServiceI commentService;
	//获取所有评论 按时间排序
	@RequestMapping("/commentlist")
	@ResponseBody
	public String commentlist(HttpServletResponse response,HttpServletRequest request, HttpSession session) {
		
		JSONObject jsonObject=new JSONObject();
		
		String maxsize=request.getParameter("maxsize");
		String curentpage=request.getParameter("curentpage");

		List<TComment> comments=null;
		
		try {
			
			comments=commentService.getTComments(maxsize, curentpage);
			
			jsonObject.put("msg", comments);
			jsonObject.put("status", 1);
			
		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("status", 0);
		}
		
		return jsonObject.toJSONString();
	}

    @RequestMapping("/commentlistbyid")
    @ResponseBody
    public String commentlistbyid(HttpServletResponse response,HttpServletRequest request, HttpSession session) {

        SimpleDateFormat format=new SimpleDateFormat("MM-dd hh:mm:");

        JSONObject jsonObject=new JSONObject();

        String maxsize=request.getParameter("comentmaxsize");
        String curentpage=request.getParameter("comentcurentpage");
        String iid=request.getParameter("iid");

        List<TComment> comments=null;

        try {

            comments=commentService.getTComments(maxsize, curentpage,iid);



            for(TComment comment:comments){
                Date date1= new Date(Long.parseLong(comment.getComenttime()));
                comment.setComenttime( format.format(date1));
            }
            jsonObject.put("msg", comments);
            jsonObject.put("status", 1);

        } catch (Exception e) {
            e.printStackTrace();
            jsonObject.put("status", 0);
        }

        return jsonObject.toJSONString();
    }


    @RequestMapping("/dataGrid")
	@ResponseBody
	public DataGrid dataGrid(TComment	comment, PageHelper ph) {
		return commentService.dataGrid(comment, ph);
	}
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String id, HttpSession session) {
		Json j = new Json();
		if (id != null ) {// 不能删除自己
			commentService.delete(id);
		}
		j.setMsg("删除成功！");
		j.setSuccess(true);
		return j;
	}
	
	//评论某一条信息
	@RequestMapping("/comentmsg")
	@ResponseBody
	public Json comentmsg(HttpServletRequest request, HttpSession session)throws Exception {
		
		String commentmsg=request.getParameter("content");
		String iid=request.getParameter("iid");
        String userid=request.getParameter("userid");

        Tuser tuser=userService.getUserByUserId(userid);

		Json j = new Json();
		
		if(tuser==null){
			j.setMsg("登陆超时！");
			j.setSuccess(false);
		}else{
			
			TComment comment=new TComment();
			comment.setComenttime(new Date().getTime()+"");
			comment.setIid(iid);
			comment.setUsername(tuser.getName());
			comment.setUserid(tuser.getUserid()+"");
			comment.setCommentmsg(commentmsg);
			
			try {
				commentService.add(comment);

                //首次评论增加积分

                TComment tComment= commentService.getTComment(tuser.getUserid()+"",iid);
                if(tComment==null){
                    //添加积分  评论添加 20积分

                    User user=userService.get(tuser.getId());
                    user.setTotalscore(user.getTotalscore()+20);
                    user.setUnchangedscore(user.getUnchangedscore()+20);
                    userService.editotlescore(user);

                    j.setMsg("获得20金币！");
                    j.setSuccess(true);
                }else {
                    j.setMsg("已获得20金币过了！");
                    j.setSuccess(true);
                }

			} catch (Exception e) {
				e.printStackTrace();

				j.setMsg("评论失败！");
				j.setSuccess(false);
			}
			
		}
		
		return j;
	}
	
	
	//喜欢一条信息
		@RequestMapping("/like")
		@ResponseBody
		public Json like(HttpServletRequest request, HttpSession session)throws Exception {
			
			String iid=request.getParameter("iid");
            String userid=request.getParameter("userid");

            Tuser tuser=userService.getUserByUserId(userid);

			Json j = new Json();
			
			if(tuser==null){
				j.setMsg("登陆超时！");
				j.setSuccess(false);
			}else{

				List<TComment> comments=commentService.getbyiid(tuser.getUserid()+"",iid);
				if(comments!=null&&comments.size()>0){

                    if(comments.get(0).getIslike()==1){//已经评论喜欢
                        j.setMsg("已经喜欢啦@!@");
                        j.setSuccess(false);
                    }else{
                        for(TComment comment:comments){
                            comment.setIslike(1);
                            commentService.update(comment);
                        }
                        User user=userService.get(tuser.getId());
                        if(user.getTotalscore()==null){
                            user.setTotalscore(15);
                        }else{
                            user.setTotalscore(user.getTotalscore()+15);
                        }
                        if(user.getUnchangedscore()==null){
                            user.setUnchangedscore(15);
                        }else{
                            user.setUnchangedscore(user.getTotalscore()+15);
                        }

                        userService.editotlescore(user);
                        j.setMsg("获得15个金币！");
                        j.setSuccess(true);


                    }



					
				}else{
					j.setMsg("信息不存在！");
					j.setSuccess(false);
					
				}
				
			}
			
			return j;
		}
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	public Json batchDelete(String ids, HttpSession session) {
		Json j = new Json();
		if (ids != null && ids.length() > 0) {
			for (String id : ids.split(",")) {
				if (id != null) {
					this.delete(id, session);
				}
			}
		}
		j.setMsg("批量删除成功！");
		j.setSuccess(true);
		return j;
	}

}
