package sy.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.TComment;
import sy.model.Tgonggao;
import sy.model.Tinteract;
import sy.pageModel.*;
import sy.service.CommentServiceI;
import sy.service.InteractServiceI;

import com.alibaba.fastjson.JSONObject;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;

//互动广场
@Controller
@RequestMapping("/interactController")
public class InteractController extends BaseController {
	@Autowired
	private InteractServiceI interactService;
	@Autowired
	private CommentServiceI commentService;
    @Autowired
    private UserServiceI userService;

    @RequestMapping("/editgonggaoPage")
    public String editgonggaoPage(String id, HttpServletRequest request) {
        Tinteract interact = interactService.get(id);
        request.setAttribute("interact", interact);
        return "/interactadmin/edite";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Json delete(String id, HttpSession session) {

        Json j = new Json();
        if (id != null) {
            interactService.delete(id);
        }
        j.setMsg("删除成功！");
        j.setSuccess(true);
        return j;
    }

    /**
     * 批量删除
     *
     * @param ids
     *            ('0','1','2')
     * @return
     */
    @RequestMapping("/batchDelete")
    @ResponseBody
    public Json batchDelete(String ids, HttpSession session) {
        Json j = new Json();
        if (ids != null && ids.length() > 0) {
            for (String id : ids.split(",")) {
                if (id != null) {
                    this.delete(id, session);
                }
            }
        }
        j.setMsg("批量删除成功！");
        j.setSuccess(true);
        return j;
    }

    @RequestMapping("/dataGrid")
    @ResponseBody
    public DataGrid dataGrid(Tinteract tinteract, PageHelper ph) {
        return interactService.dataGrid(tinteract, ph);
    }

	//发布互动广场信息
	@RequestMapping("/uploadcontent")
	@ResponseBody
	public Json uploadcontent(HttpServletResponse response,HttpServletRequest request, HttpSession session)throws Exception {
		Json j = new Json();
		String content=request.getParameter("content");
		String imageurl=request.getParameter("imageurl");
		String title=request.getParameter("title");
		//content=new String(content.getBytes("ISO-8859-1"),"UTF-8");
		Tinteract tinteract=new Tinteract();
		tinteract.setContent(content);
		tinteract.setImageurl(imageurl);
		tinteract.setTitle(title);
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("MM-dd");
		
		String time=dateFormat.format(new Date());
		
		tinteract.setPublishtime(time);
		try {
		interactService.add(tinteract);
		j.setMsg("提交失败！");
		j.setSuccess(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		j.setMsg("提交成功！");
		j.setSuccess(true);
		return j;
	}

    //获得指定日期互动广场信息和信息的评论

    @RequestMapping("/getinteractdetaillist")
    @ResponseBody
    public String getinteractdetaillist(HttpServletResponse response,HttpServletRequest request, HttpSession session)throws Exception {
        JSONObject jsonObject=new JSONObject();

        String maxsize=request.getParameter("maxsize");
        String curentpage=request.getParameter("currentpage");
        String publishtime=request.getParameter("publishtime");
        String userid=request.getParameter("userid");

        List<Tinteract> tinteracts=null;

        SimpleDateFormat format=new SimpleDateFormat("MM-dd hh:mm:");
        try {
            //按时间分类
            tinteracts=interactService.getTinteracts(maxsize, curentpage,publishtime);
            TComment comment=null;
            List<TComment> comments=null;
            for(Tinteract tinteract:tinteracts){

                //获得这个时间的评论总数和总喜欢数

                tinteract.setLikecount(commentService.getlikecount(tinteract.getId()));
                tinteract.setCommentcout(commentService.getcommentcount(tinteract.getId()));


                //查询当前用户是否喜欢
                comment= commentService.getTComment(userid,tinteract.getId());
                if(comment!=null){
                    tinteract.setIslike(comment.getIslike());
                }


                //获得对当前这条互动广场信息评论
                comments= commentService.getTComments(tinteract.getId());
                for(TComment comment1:comments){


                    Date date1= new Date(Long.parseLong(comment1.getComenttime()));
                    comment1.setComenttime( format.format(date1));

                }
                tinteract.setComments(comments);


            }

            jsonObject.put("msg", tinteracts);
            jsonObject.put("status", 1);

        } catch (Exception e) {
            e.printStackTrace();
            jsonObject.put("status", 0);

        }
        return jsonObject.toJSONString();

    }


        //获取互动广场信息
	@RequestMapping("/getinteractlist")
	@ResponseBody
	public String getinteractlist(HttpServletResponse response,HttpServletRequest request, HttpSession session)throws Exception {
		JSONObject jsonObject=new JSONObject();


		String maxsize=request.getParameter("maxsize");
		String curentpage=request.getParameter("currentpage");
		List<Tinteract> tinteracts=null;
		try {
			//按时间分类
			tinteracts=interactService.getTinteracts(maxsize, curentpage);
			
			for(Tinteract tinteract:tinteracts){

                //获得这个时间的评论总数和总喜欢数

                tinteract.setLikecount(commentService.getlikecount(tinteract.getId()));
				tinteract.setCommentcout(commentService.getcommentcount(tinteract.getId()));


			}
			
			jsonObject.put("msg", tinteracts);
			jsonObject.put("status", 1);
			
		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("status", 0);
			
		}
		return jsonObject.toJSONString();
		
	}
	
	
}
