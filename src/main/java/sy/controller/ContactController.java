package sy.controller;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sy.pageModel.Json;
import sy.pageModel.Mcontact;
import sy.pageModel.SessionInfo;
import sy.service.ContactServiceI;
import sy.util.ConfigUtil;
/**
 * 用户控制器
 * 
 * @author 孙宇
 * 
 */
@Controller
@RequestMapping("/contactController")
public class ContactController extends BaseController {
	@Autowired
	private ContactServiceI contactService;
	/**
	 * 添加礼包
	 * 
	 * @param id
	 * 
	 */
	@ResponseBody
	@RequestMapping("/addcontact")
	public Json addContact(Mcontact contact, HttpSession session) {
		Json j = new Json();
		try {
			SessionInfo sessionInfo = (SessionInfo)session.getAttribute(ConfigUtil.getSessionInfoName());
			contact.setUserid(sessionInfo.getUserid());
			contactService.add(contact);
			j.setSuccess(true);
			j.setMsg("注册成功！新注册的用户没有任何权限，请让管理员赋予权限后再使用本系统！");
			//j.setObj(user);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
	  return j;
	}
}
