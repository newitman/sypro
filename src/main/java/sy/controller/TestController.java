package sy.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.pageModel.Json;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;

@Controller
@RequestMapping("/testController")
public class TestController extends BaseController{
	@Autowired
	private UserServiceI userService;
	
	@RequestMapping("/userinfo")
	@ResponseBody
	public Json userinfo(HttpSession session) {
		SessionInfo sessionInfo = (SessionInfo) session.getAttribute(ConfigUtil.getSessionInfoName());
		Json j = new Json();
		User user=userService.get(sessionInfo.getId());
		j.setMsg("user:"+user.getName());
		j.setSuccess(true);
		return j;
	}
}
