package sy.controller;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sy.model.Tasktype;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.MTasktype;
import sy.pageModel.Mexchangetype;
import sy.pageModel.PageHelper;
import sy.service.TaskTypeServiceI;

/**
 * 任务记录控制器
 * 
 * @author 甘涛
 * 
 */
@Controller
@RequestMapping("/tasktypeController")
public class TaskTypeController extends BaseController {
	@Autowired
	private TaskTypeServiceI tasktypeService;
	
	/**
	 * 查看兑换记录类型
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/dataGrid")
	@ResponseBody
	public DataGrid dataGrid(MTasktype task, PageHelper ph) {
		return tasktypeService.dataGrid(task, ph);
	}
	/**
	 * 跳转修改类型界面
	 * 
	 * @return
	 */
	@RequestMapping("/editPage")
	public String editPage(HttpServletRequest request, String id) {
		Tasktype u;
		try {
			u = tasktypeService.gettasktype(id);
			request.setAttribute("task", u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/task/tasktype/tasktypeEdit";
	}
	
	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(MTasktype tasktype) {
		Json j = new Json();
		try {
			tasktypeService.edit(tasktype);
			j.setSuccess(true);
			j.setMsg("编辑成功！");
			j.setObj(tasktype);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
		return j;
	}

	/**
	 * 跳转到添加用户页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		Mexchangetype u = new Mexchangetype();
		u.setId(UUID.randomUUID().toString());
		int pagecount = 0;
		try {
			pagecount = tasktypeService.gettasksize();
			pagecount=pagecount+1;
			u.setTypeid(pagecount+"");
			request.setAttribute("tasktype", u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/task/tasktype/tasktypeAdd";
	}

	/**
	 * 添加类型
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Json add(MTasktype tasktype) {
		Json j = new Json();
		try {
			tasktypeService.add(tasktype);
			j.setSuccess(true);
			j.setMsg("添加成功！");
			j.setObj(tasktype);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return j;
	}
	
}
