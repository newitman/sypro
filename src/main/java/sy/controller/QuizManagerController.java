package sy.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.TQuizManager;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.PageHelper;
import sy.pageModel.SessionInfo;
import sy.service.QuizManagerServiceI;
import sy.service.QuizServiceI;
import sy.service.UserServiceI;
import sy.task.TaskServers;
import sy.util.ConfigUtil;
//竞猜管理
@Controller
@RequestMapping("/quizmanagerController")
public class QuizManagerController extends BaseController {
	@Autowired
	private QuizServiceI quizService;
	@Autowired
	private QuizManagerServiceI quizManagerService;
	@Autowired
	private UserServiceI userService;
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	public Json batchDelete(String ids, HttpSession session) {
		Json j = new Json();
		if (ids != null && ids.length() > 0) {
			for (String id : ids.split(",")) {
				if (id != null) {
					this.delete(id, session);
				}
			}
		}
		j.setMsg("批量删除成功！");
		j.setSuccess(true);
		return j;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String id, HttpSession session) {
		Json j = new Json();
		if (id != null ) {
			quizManagerService.delete(id);
		}
		j.setMsg("删除成功！");
		j.setSuccess(true);
		return j;
	}
	
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public DataGrid dataGrid(TQuizManager tad, PageHelper ph) {
		return quizManagerService.dataGrid(tad, ph);
	}

	//后台发布竞猜
	@RequestMapping("/publisquiz")
	@ResponseBody
	public Json publisquiz(HttpSession session, HttpServletResponse response,HttpServletRequest request)throws Exception{
		response.setCharacterEncoding("utf-8");
		
		SessionInfo sessionInfo = (SessionInfo) session.getAttribute(ConfigUtil.getSessionInfoName());
		String number=request.getParameter("number");
		
		String startDate=request.getParameter("startDate");
		String startTime=request.getParameter("startTime");
		String endDate=request.getParameter("endDate");
		String endTime=request.getParameter("endTime");
		String gold=request.getParameter("gold");
		String starttime=startDate+" "+startTime;
		String endtime= endDate+" "+endTime;
		
		Json j = new Json();
	
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd hh:mm");
		
		long startime=format.parse(starttime).getTime();
		long todaytime=new Date().getTime();
		if(startime<todaytime){
			j.setMsg("发布日期不能小于今天！");
			j.setSuccess(false);
		}
		
		TQuizManager quizManager=new TQuizManager();
		//是否竞猜 在一天内是否有过竞猜 记录日期
		
		List<TQuizManager> quizManagers=quizManagerService.getQuiz(number,starttime ,endtime);
	
		if(quizManagers.size()>0){
			//已经竞猜 当天不能再竞猜
			j.setMsg("当天不能重复发布竞猜！");
			j.setSuccess(false);
			
		}else{
			
			try {
				quizManager.setStatus(0);//开始状态
				quizManager.setNumber(number);
				quizManager.setUserid(sessionInfo.getUserid()+"");
				quizManager.setStarttime(starttime);
				quizManager.setEndtime(endtime);
				quizManager.setGold(gold);
				TQuizManager databasemanager=quizManagerService.add(quizManager);
				
				//开启线程 去更新竞猜状态
				
				new Thread(new TaskServers(userService,quizService,quizManagerService,databasemanager)).start();
				
				j.setMsg("发布成功！");
				j.setSuccess(true);
			} catch (Exception e) {
				j.setMsg("发布失败！");
				j.setSuccess(false);
				
			}
		}
		
		return j;
	}
		
}
