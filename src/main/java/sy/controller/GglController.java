package sy.controller;

import java.awt.Font;
import java.io.File;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sy.model.Tggl;
import sy.model.Tuser;
import sy.pageModel.Mtaskrecord;
import sy.pageModel.SessionInfo;
import sy.pageModel.User;
import sy.service.GglServiceI;
import sy.service.TaskRecordServiceI;
import sy.service.UserServiceI;
import sy.util.ConfigUtil;
import sy.util.CreateImage;

import com.alibaba.fastjson.JSONObject;

//刮刮乐
@Controller
@RequestMapping("/gglController")
public class GglController  extends BaseController {

    @Autowired
    private TaskRecordServiceI taskrecordService;

    @Autowired
	private GglServiceI gglService;
	@Autowired
	private UserServiceI userService;
	
	//第一次请求 用户是否有刮奖机会
	
	@RequestMapping("/ggl")
	public String manager(HttpServletRequest request,HttpServletResponse response,HttpSession session) {

		String number="";
		try {
			

			//判断是否有刮奖机会
			String userid=request.getParameter("userid");
			//SessionInfo sessionInfo = (SessionInfo) session.getAttribute(ConfigUtil.getSessionInfoName());
			//更新用户刮奖次数
			Tuser tuser=userService.getGglCount(userid);

            if(tuser!=null){
                int gglcount=tuser.getGgjnunber();

                if(gglcount>=1){//可以刮奖
                    request.setAttribute("isvalibe", 1);//是否有效刮奖 有效
                    request.setAttribute("gglcount", gglcount);//刮奖次数
                }else{//不可以刮奖

                    request.setAttribute("isvalibe", 0);//是否有效刮奖 0:无效
                }
                request.setAttribute("userid", userid);
            }else{
                System.out.print("用户不存在");
            }

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "/guaguale/ggl";
	}
	
	@RequestMapping("/dui")
	@ResponseBody
	public String dui(HttpServletRequest request,HttpServletResponse response,HttpSession session) {


		JSONObject jsobj = new JSONObject();
		try {

			String userid=request.getParameter("userid");


			if(userid==null||"".equals(userid))
			{
				jsobj.put("msg", "参数错误");
				jsobj.put("status", "error");
			}else{
                //更新用户刮奖次数
                Tuser tuser=userService.getGglCount(userid);
                int gglcount=tuser.getGgjnunber();

                User user=new User();
                 BeanUtils.copyProperties(tuser,user);

                //更新刮奖机会
                if(gglcount>=1){
                    --gglcount;
                    //更新总积分  获奖得到10000金币

                    if(user.getTotalscore()==null){
                        user.setTotalscore(10000);
                    }else{
                        user.setTotalscore(user.getTotalscore()+1000);
                    }
                    if(user.getUnchangedscore()==null){
                        user.setUnchangedscore(10000);
                    }else{
                        user.setUnchangedscore(user.getUnchangedscore()+10000);
                    }

                    userService.editotlescore(user);


                    tuser.setGgjnunber(gglcount);
                    userService.edit(user);

                    Mtaskrecord task = new Mtaskrecord();
                    task.setTaskType("10");		//设置类型
                    task.setUserid(Long.parseLong(userid));		//设置用户id
                    task.setScore(10000);//设置积分
                    try {
                        taskrecordService.add(task);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    userService.updateGglCount(userid, gglcount);

                    jsobj.put("msg", "兑奖成功");
                    jsobj.put("status", "sucess");


                }else{
                    gglcount=0;
                    tuser.setGgjnunber(gglcount);
                    userService.edit(user);

                    userService.updateGglCount(userid, gglcount);

                    jsobj.put("msg", "参数错误");
                    jsobj.put("status", "error");

                }

            }


			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsobj.toJSONString();
	}
	//定义:
	//每隔1天生成一个随机中奖号码 
	public void createNumber() throws Exception{
		
		Tggl tggl=gglService.getLastGgl();
		if(tggl==null){//第一次创建中奖号码
			 
			 tggl=new Tggl();
			 tggl.setCreatetime(new Date().getTime()+"");
			 tggl.setNumber( createRandomNumber());
			 tggl.setStatus(1);
			 gglService.add(tggl);
			
		}else{//获取最后创建中奖号码时间
			long l1=24*60*60*1000;
			long l2=new Long(tggl.getCreatetime()).longValue();
			long l3=l1+l2;//下一次开始创建中奖号码时间
			
			
			if(new Date().getTime()<l3){//不用创建随机号 当前这个号码没有过期
				
				
			}else{//创建新的中奖随机号码
				
				Tggl _tggl=new Tggl();
				_tggl.setCreatetime(new Date().getTime()+"");
				_tggl.setNumber(createRandomNumber());
				_tggl.setStatus(1);
				
				gglService.add(_tggl);
				
				//更新上一个记录状态为无效
				tggl.setStatus(0);
				
				gglService.updateGgl(tggl);
				
				
			}
			
			
		}
		
		
	}
	
	public static synchronized String createRandomNumber(){
		Random random = new Random();
		String result="";
		for(int i=0;i<6;i++){
			result+=random.nextInt(10);
		}
		
		return result;
	}


    @RequestMapping("/flush")
    @ResponseBody
    public String flush(HttpServletRequest request,HttpServletResponse response,HttpSession session) {

        JSONObject jsobj = new JSONObject();

        jsobj.put("msg", "0");

        return jsobj.toJSONString();
    }
}
