package sy.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sy.model.Tgonggao;
import sy.pageModel.DataGrid;
import sy.pageModel.Json;
import sy.pageModel.PageHelper;
import sy.service.GonggaoServiceI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/gonggaoController")
public class GonggaoController extends BaseController {
	@Autowired
	private GonggaoServiceI gonggaoService;

    @RequestMapping("/editgonggaoPage")
    public String editgonggaoPage(String id, HttpServletRequest request) {
        Tgonggao u = gonggaoService.get(id);
        request.setAttribute("gonggao", u);
        return "/gonggao/usergonggao";
    }
	//提交建议
	@RequestMapping("/publicgonggao")
	@ResponseBody
	public Json publicgonggao(HttpServletResponse response,HttpServletRequest request, HttpSession session) {
		Json j = new Json();
		String title=request.getParameter("title");
		String content=request.getParameter("content");
        Tgonggao tgonggao=new Tgonggao();
		
		try {

            tgonggao.setCreatetime(new Date().getTime()+"");
            tgonggao.setTitle(title);
            tgonggao.setIsvailable(1);
            tgonggao.setContent(content);
			
			gonggaoService.add(tgonggao);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		j.setMsg("提交成功！");
		j.setSuccess(true);
		return j;
	}
    @RequestMapping("/eidtegonggao")
    @ResponseBody
    public Json eidtegonggao(HttpSession session, String title, String content,String id,HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");
        Json j = new Json();

        if (session != null) {

            Tgonggao tgonggao=gonggaoService.get(id);
            if(tgonggao!=null){
                tgonggao.setTitle(title);
                tgonggao.setContent(content);
                gonggaoService.update(tgonggao);

                j.setMsg("更新成功！");
                j.setSuccess(true);
            }else{
                j.setMsg("更新失败！");
                j.setSuccess(false);

            }

        } else {

            j.setMsg("登录超时！");
            j.setSuccess(false);
        }
        return j;
    }



    //获取有效一条有效公告
	@RequestMapping("/getgonggao")
	@ResponseBody
	public String getads(HttpServletResponse response,HttpServletRequest request){
		response.setCharacterEncoding("utf-8");
		JSONObject jsobj = new JSONObject();
		try {
			List<Tgonggao>  tads=gonggaoService.getTgonggaos();
            if(tads.size()>0){
                jsobj.put("status", "1");
                jsobj.put("msg",tads.get(0));
            }else{
                jsobj.put("status", "0");
                jsobj.put("msg","没有公告");
            }

		} catch (Exception e) {
			jsobj.put("status", "0");
			jsobj.put("msg","获取失败");
		}
		return jsobj.toJSONString();
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public DataGrid dataGrid(Tgonggao tad, PageHelper ph) {
		return gonggaoService.dataGrid(tad, ph);
	}
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String id, HttpSession session) {
		Json j = new Json();
		if (id != null ) {// 不能删除自己
			gonggaoService.delete(id);
		}
		j.setMsg("删除成功！");
		j.setSuccess(true);
		return j;
	}
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	public Json batchDelete(String ids, HttpSession session) {
		Json j = new Json();
		if (ids != null && ids.length() > 0) {
			for (String id : ids.split(",")) {
				if (id != null) {
					this.delete(id, session);
				}
			}
		}
		j.setMsg("批量删除成功！");
		j.setSuccess(true);
		return j;
	}

}
