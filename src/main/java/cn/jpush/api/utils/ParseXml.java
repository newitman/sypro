package cn.jpush.api.utils;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;
public class ParseXml {
	  public String xmlElements(String xmlDoc) {
	        //创建一个新的字符串
		    String target = "";
	        StringReader read = new StringReader(xmlDoc);
	        //创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
	        InputSource source = new InputSource(read);
	        //创建一个新的SAXBuilder
	        SAXBuilder sb = new SAXBuilder();
	        try {
	            //通过输入源构造一个Document
	            Document doc = sb.build(source);
	            //取的根元素
	            Element root = doc.getRootElement();
	            List jiedian = root.getChildren();
	            target = ((Element)jiedian.get(1)).getValue();
	        } catch (JDOMException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return target;
	  }
	  
	 /* public static void main(String[] args){
		  	ParseXml doc = new ParseXml();
		  	
		  	 String xml = "<?xml version='1.0' encoding='GB2312' ?>" +
		  	 		"<orderinfo>" +
		  	 		"<err_msg></err_msg>" +
    		"<retcode>1</retcode><orderid>11111</orderid>" +
    		"<orderid1>11111</orderid1><orderid2>11111</orderid2>" +
    		"<orderid3>11111</orderid3>" +
    		"</orderinfo>";
	       System.out.println(doc.xmlElements(xml));
	    }*/
}
