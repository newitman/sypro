package cn.jpush.api.utils;
import java.util.Comparator;
public class BaseCompare implements Comparator<Compares>{
	public int compare(Compares o1, Compares o2) {
		return o1.getCount() - o2.getCount();
		}
}
