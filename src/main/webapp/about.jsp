﻿<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>关于我们 - 有钱赚 手机赚钱</title>
<meta name="keywords" content="关于有钱赚，关于手机赚钱, 关于学生赚钱利器" /> 
<link rel="stylesheet" type="text/css" href="<%=basePath%>/css/mini.css" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>

</head>
<body>
	<div class="container">
		<div class="logo"><h1><a id="link1" href="<%=basePath%>" title="有钱赚"><img src="<%=basePath%>/images/logo.png" border="0"/></a></h1></div>
		<div class="menu">
			<ul>
				<li><a id="link2" href="<%=basePath%>">首页</a></li>
				<li ><a id="link8" href="<%=basePath%>download.jsp">下载</a></li>
				<li ><a id="link3" href="<%=basePath%>help.jsp">帮助</a></li>
				<li ><a id="link4" href="<%=basePath%>feedback.jsp">问题反馈</a></li>
				<li ><a id="link7" href="<%=basePath%>promote.jsp">推广有钱赚</a></li>
			</ul>
		</div>
	</div>
	
	<div style="height:630px;">
	<div class="content03">
	<div class="askBox">
		<div class="title01">
			<h2>关于我们</h2>
		</div>
		<div class="help">
		<p>有钱赚是一个北京的创业团队，核心成员来自于清华大学、北京大学、香港科技大学等著名高校，曾任职于微软、诺基亚、网易等国际国内知名IT企业，有多年的互联网、移动互联网工作经验。在智能手机和手机互联网飞速走进我们生活的时代，我们希望把有钱赚打造成一个手机赚钱的平台，让普通用户可以在手机进行简单操作以获取收益，让商家能够方便地接触到更多客户。</p>
		<p>合作请联系：support@mizhuan.me</p>
		<div class="clear"></div>
		</div>
	</div>
	
	</div>
	
	
	</div>

    <div id="footer">
        <div class="limit">
           <div class="footer-nav">
                <a id="link5" href="<%=basePath%>about.jsp">关于有钱赚</a>
                <a id="link6" href="<%=basePath%>jobs.jsp">招贤纳士</a>
                
           </div>
           <div class="copyright">&copy; 2011-2014 重庆瓦力网络科技有限公司 版权所有 (京ICP备11018675号-3) </div>
        </div>
    </div>

  
</body>
</html>
