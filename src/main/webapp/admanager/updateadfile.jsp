<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="<%=basePath%>css/mini.css?v=2014" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<!-- 配置文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="<%=basePath%>interactadmin/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-2.0.0.js"></script>
<script type="text/javascript">
    var editor;
	$(function() {
		parent.$.messager.progress('close');
        $("#submitbutton").click(function(){
            var cotent=editor.getContent();
            var imageurl=getimagurl(cotent);
            var cotent=cotent.replace(/\"/g, "\'");

            if(imageurl===""||imageurl==undefined){
                alert("请上传一张广告图片@!@");
            }else{
                $.post("${pageContext.request.contextPath}/adController/updatecontent",{random:Math.random(),content:cotent,imageurl:imageurl,id:$("#adid").val()},function(rs){
                    var msg=eval("("+rs+")");

                    alert(msg.msg);
                    editor.setContent("");

                });
            }



        });

        $("#cleartbutton").click(function(){

            editor.setContent("");
        });

	});

    function getimagurl(cotent){

        var re=/(http(s)?\:\/\/)?(www\.)?(\w+\:\d+)?(\/\w+)+\.(swf|gif|jpg|bmp|jpeg|png)/gi;
        var arr=cotent.match(re);

        // for(var i=0;i<arr.length;i++){
        //   document.getElementById("txt").value+=arr[i]+"\n";
        //  }

        if(arr!=null&&arr!=undefined){
            if(arr.length>0){
                return arr[0];
            }
        }

        return "";
    }

</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">

			<table class="table table-hover table-condensed">
				<tr>
                    <script id="container" name="content" type="text/plain">
                        ${tad.content}
                    </script>
                    <script type="text/javascript">
                        if(editor==null||editor==undefined){
                            editor = UE.getEditor('container');
                        }else{
                            editor.destroy();
                            editor = UE.getEditor('container');
                        }

                    </script>
				</tr>

			</table>
            <input type="hidden" id="adid" name="id" value="${tad.id}">
            <input type="button" id="submitbutton" value="更新"><input type="button" id="cleartbutton" value="清空">

	</div>
</div>