<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-2.0.0.js"></script>
<script type="text/javascript">
	$(function() {
		parent.$.messager.progress('close');
		
		$("#subbution").click(function(){
			var pushmsg=$("#pushmsg").val();
			if(pushmsg===""){
				alert("推送消息不能为空!");
			}else{
				$.post("<%=basePath%>pushController/pushmsg",{random:Math.random(),pushmsg:$("#pushmsg").val()},function(rs){
					var msg=eval("("+rs+")");
					alert(msg.msg);
					
				});
			}
		});
		
		$("#clearbution").click(function(){
			$("#pushmsg").val("");
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<table class="table table-hover table-condensed">
			<tr>	
			<th>APP消息推送</th>
				<td>
					<textarea rows="10" cols="100" id="pushmsg"></textarea>
				</td>
			</tr>
			<tr>	
				<td align="center">
				<input type="button"  class="span2" id="subbution" value="推送">
				<input type="button"  class="span2" id="clearbution" value="清空">
				</td>
			</tr>
		</table>
	</div>
</div>