<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="<%=basePath%>jslib/jquery-2.0.0.js"></script>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-easyui-1.3.3/easyloader.js"></script>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<link href="<%=basePath%>jslib/jquery-easyui-1.3.3/themes/default/easyui.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>jslib/jquery-easyui-1.3.3/themes/icon.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>site/css/buttons.css" rel="stylesheet">
<title>App表格</title>

<style type="text/css">

.filehead ul li span{margin:5px;margin: 0px;padding: 0px;display: block;height: 30px;width: auto;float: left;text-align: center;line-height:30px;}
.filehead ul li select,span input{height: 30px;border: 1px solid rgb(44,116,219);}

.filehead ul li{
	display: block;float: left;padding: 5px;
}
.filehead ul{float: left;}
</style>
<script type="text/javascript">

	$(function(){
		
		
		parent.$.messager.progress('close');
		getappfile();
		
		//上传按钮
		$("#apk1").click(function(){
			$('#appdialog').show();
			$('#appdialog').dialog({
				modal:true,
					buttons: [{
					text: '上传',
					iconCls: 'icon-ok',
					handler: function() {
						if($("#filename").val()===""){
							$.messager.alert('App名称或文件不能为空!');
						}else{
							$("#uploadfileform").submit();
						}
					}
					}, {
					text: '取消',
					handler: function() {
					iconCls: 'icon-cancel',
						$('#appdialog').dialog('close');
					}
					}] 
			});

		});
		
	});
	
	//获取已经上传的app文件
	function getappfile(){
		$.post("<%=basePath%>/uploadController/applist",{random:Math.random()},function(result){
			
			var list=eval("("+result+")");
			var appliststr="";
			$.each(list,function(index,userbean){
				
				var filename=userbean.filename;
				var filetype="";
				if(filename.indexOf("Android")>-1){
					filetype="Android";
				}else{
					filetype="IOS";
				}
				appliststr+="<ul style='margin: 0px;padding: 0px;list-style: none;display: block;'>";
				appliststr+="<li style='line-height:40px;float: left;width: 150px;height: 40px;background-color: rgb(44,116,219) ;margin: 5px;text-align: center;'>"+filetype+"</li>";
				
				appliststr+="<li style='line-height:40px;float: left;width: 150px;height: 40px;background-color: rgb(44,116,219) ;margin: 5px;text-align: center;'>"+userbean.filename+"</li>";
			
				appliststr+="<li style='line-height:40px;float: left;width: 150px;height: 40px;background-color: rgb(44,116,219) ;margin: 5px;text-align: center;'><a href='javascript:void(0);' onclick='deleteapp(\""+userbean.filename+"\")'>删除</a></li>";
				appliststr+="</ul>";
				
				
			});
			
			$("#applist").html(appliststr);
		});
	}
	
	function deleteapp(appname){
		$.post("<%=basePath%>uploadController/deleteapp",{random:Math.random(),appname:appname},function(result){
			var msg=eval("("+result+")");
			if(msg.msg){
				getappfile();
				$.messager.alert('Warning','文件删除成功!');
			}else{
				$.messager.alert('Warning','文件删除失败!');
			}
		});
	}
</script>
</head>
<body>

	<div class="contentbody" style="height: 500px;">
		<div style="height: 20px;">
			<a id="apk1" class="button yellow" href="javascript:void(0);">上传APK</a>
		</div>
		<div class="myhead" style="height: 53px;width: 500px;">
			<ul style="margin: 0px;padding: 0px;list-style: none;display: block;" >
				<li style="line-height:40px;float: left;width: 150px;height: 40px;background-color:rgb(44,116,219) ;margin: 5px;text-align: center;">app类型</li>
				<li style="line-height:40px;float: left;width: 150px;height: 40px;background-color: rgb(44,116,219) ;margin: 5px;text-align: center;">名称</li>
				<li style="line-height:40px;float: left;width: 150px;height: 40px;background-color: rgb(44,116,219) ;margin: 5px;text-align: center;">操作</li>
			</ul>	
			<div id="applist">
				
			</div>			
		</div>
		
		<div class="content" style="height: 427px;">
			
		</div>
	
	</div>
<div id="appdialog" title="上传App图片" style="display:none;width:800px;height:200px;padding-top: 20px;padding-left: 40px;">
		<div class="filecontent" style="background-color: red;">
			
			<form action="<%=basePath%>uploadController/upload" method="post" enctype="multipart/form-data" id="uploadfileform">
			<div class="filehead">
				
				<ul>
					<li>
						<span style="background-color:rgb(44,116,219);">app名称</span>
						<span><input name="fileappname" type="text" id="fileappname" disabled="disabled"/></span>
					</li>
					<li>
					
						<span style="background-color:rgb(44,116,219);">app类型</span>
						<span>
						<select style="width: 100px;text-align: center;" id="apptype" name="filetype">
							<option value="1">Android</option>
							<option value="2">IOS</option>
							</select>
						</span>
					</li>
					
					<li>
						<span style="background-color:rgb(44,116,219);">文件路径</span>
						<span><input name="file" type="file" id="filename"/></span>
					</li>
				</ul>
				
			
			</div>
			
			</form>
		
		</div>
</div>
</body>
</html>