<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String userid=request.getParameter("userid");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link href="<%=basePath%>style/sypro/codiqa.ext.css" rel="stylesheet">
  <link href="<%=basePath%>jslib/sypro/jquery.mobile-1.4.0.css" rel="stylesheet">
 <style type="text/css">
	.ui-block-a{
	  border-right: 1px  solid blue;
	   border-bottom: 1px  solid blue;
	}
	.ui-block-b{
	 
	   border-bottom: 1px  solid blue;
	}
	.ui-grid-a{
	 border: 1px  solid blue;}
     #dialog div{
         background: none repeat scroll 0 0 #FFFF00;height: 208px;
     }
 </style>
  <script src="<%=basePath%>jslib/sypro/jquery.min.js"></script>
  <script src="<%=basePath%>jslib/sypro/jquery.mobile-1.4.0.min.js"></script>
  <script src="<%=basePath%>jslib/sypro/codiqa.ext.js"></script>

  <script type="text/javascript">

      function closediolag(){
          $("#dialog").hide();
      }

      $(function(){
    	
    	 //获取竞猜信息
    	 $.post("${pageContext.request.contextPath}/quizController/getquizinfo?userid=<%=userid%>",{random:Math.random()},function(rs){
    		 var msg=eval("("+rs+")");
    		
    		 if(msg!=undefined){
                  if(msg.obj.quizModel.endtime==="已结束"){
                      $("#endtime").html(msg.obj.quizModel.endtime);
                  }else{
                      $("#endtime").html("还有"+msg.obj.quizModel.endtime);
                  }

        		 $("#gold").html(msg.obj.quizModel.gold);
        		 $("#personcount").html(msg.obj.quizModel.personcount);
        		 
        		 $("#number").html(msg.obj.quizModel.number);
        		 
        		 $("#lastperson").html(msg.obj.quizModel.lastperson);
        		 $("#getgolds").html(msg.obj.quizModel.getgolds);
        		 
        		 if(msg.obj.quizModel.isquiz==0){
        			 $("#textinput1").hide();
        			 $("#jingcaiid").hide();
        		 }
        		 
        		 //设置期号
        		 $("#periodnumber").val(msg.obj.quizModel.periodnumber);
                 $("#periodnumbershow").html(msg.obj.quizModel.periodnumber);
                 //设置用户id
                 $("#userid").val(msg.obj.userid);
    		 }
    		 
    		 
    	 });


    	 $("#jingcaiid").click(function(){
    		 
    		 var number=$("#textinput1").val();
    		 if(number===""){
    			 //alert("竞猜数不能为空");
                 $("#content").html("竞猜数不能为空");
                 $("#dialog").show();
                 $("#dialog").dialog();
    		 }else{
    			 $.post("${pageContext.request.contextPath}/quizController/quiz",{random:Math.random(),"number":number,"periodnumber":$("#periodnumber").val(),userid: $("#userid").val()},function(rs){
    				 
    				//alert(rs);
                     $("#content").html(rs);
                     $("#dialog").show();
                     $("#dialog").dialog();
    			 });
    		 }
    		 
    	 });
    	 
     });
  
  </script>

</head>
<body>
<div data-role="page" data-control-title="Home" id="page1">
    <div role="main" class="ui-content">
        <h3>
            当前竞猜：<p id="periodnumbershow"></p>
        </h3>
        <div class="ui-grid-a">
            <div class="ui-block-a">
                <h3>
                    结束时间
                </h3>
            </div>
            <div class="ui-block-b">
                <h3 id="endtime">
                    
                </h3>
            </div>
            <div class="ui-block-a">
                <h3>
                    奖池学币
                </h3>
            </div>
            <div class="ui-block-b">
                <h3 style="color:red;" id="gold">
                    
                </h3>
            </div>
            <div class="ui-block-a">
                <h3 >
                    参与人数
                </h3>
            </div>
            <div class="ui-block-b">
                <h3 id="personcount">
                    
                </h3>
            </div>
            <div class="ui-block-a">
                <h3>
                    你的竞猜
                </h3>
            </div>
            <div class="ui-block-b" style="border-bottom: 1px;height: 57px;">
                <div class="ui-field-contain" data-controltype="textinput" >
                    <input name="" id="textinput1" placeholder="" value="" type="text" style="height: 100%; width: 100%;" >
					
                </div>
              
            </div>
			  <div class="ui-block-a" style="border-bottom: 0px;">
                 <a href="javascript:void(0);"  id="jingcaiid" class="ui-btn " >
                    竞猜
				</a>
            </div>
        </div>
        <h3>
            规则说明
        </h3>
        <h3>
            竞猜0-9的之间一个数，开奖号码为当天福彩3D开奖的最后一位数.学币大于10W即可参加，每轮竞猜一个数，押注学币2000.猜中奖金=奖池/2/中奖人数
        </h3>
        <h3>
            上一轮竞猜
        </h3>
        <div class="ui-grid-a">
            <div class="ui-block-a">
                <h3 >
                    中奖号码
                </h3>
            </div>
            <div class="ui-block-b">
                <h3 id="number">
                    
                </h3>
            </div>
            <div class="ui-block-a">
                <h3 >
                    发放奖励
                </h3>
            </div>
            <div class="ui-block-b">
                <h3 id="lastperson">
                    
                </h3>
            </div>
            <div class="ui-block-a" style="border-bottom: 0px">
                <h3>
                    中奖用户ID
                </h3>
            </div>
            <div class="ui-block-b" style="border-bottom: 0px">
                <h3 id="getgolds">
                    
                </h3>
            </div>
        </div>
    </div>
</div>

<div id="dialog" title="div层对话框" >
    <div>

            <span >消息</span>
            <span ><a href="javascript:void(0);" onclick="closediolag();">关闭</a></span>

        <p id="content"></p>
    </div>


</div>
<input type="hidden" name="userid" id="userid"/>
<input type="hidden" name="periodnumber" id="periodnumber"/>
</body>
</html>