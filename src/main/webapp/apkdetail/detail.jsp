<%--
  Created by IntelliJ IDEA.
  User: liyong
  Date: 2014/6/8
  Time: 12:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link href="/css/playground.css" media="all" rel="stylesheet" type="text/css" />
    <style type="text/css" >
        *{
            padding: 0px;
            margin: 0px;
            
        }
        ul,li{
            list-style: none;;
        }
        .head ul li{display: block;float: left;height: 80px;
            width: 50%;}
        .root{
            display: block;
            width: 100%;
            height: 80px;}
        .content{
            width: 100%;height: 100%;
            overflow: hidden;clear: left}
        .left ol li{display: block;float: left;}
    </style>

    <style type="text/css">
        /* Place all stylesheet code here */
        body { text-shadow: 0 1px 1px rgba(0,0,0,.5); }


        @-webkit-keyframes bigAssButtonPulse {
            from { background-color: #749a02; -webkit-box-shadow: 0 0 25px #333; }
            50% { background-color: #91bd09; -webkit-box-shadow: 0 0 50px #91bd09; }
            to { background-color: #749a02; -webkit-box-shadow: 0 0 25px #333; }
        }

        @-webkit-keyframes greenPulse {
            from { background-color: #749a02; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #91bd09; -webkit-box-shadow: 0 0 18px #91bd09; }
            to { background-color: #749a02; -webkit-box-shadow: 0 0 9px #333; }
        }

        @-webkit-keyframes bluePulse {
            from { background-color: #007d9a; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #2daebf; -webkit-box-shadow: 0 0 18px #2daebf; }
            to { background-color: #007d9a; -webkit-box-shadow: 0 0 9px #333; }
        }

        @-webkit-keyframes redPulse {
            from { background-color: #bc330d; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #e33100; -webkit-box-shadow: 0 0 18px #e33100; }
            to { background-color: #bc330d; -webkit-box-shadow: 0 0 9px #333; }
        }

        @-webkit-keyframes magentaPulse {
            from { background-color: #630030; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #a9014b; -webkit-box-shadow: 0 0 18px #a9014b; }
            to { background-color: #630030; -webkit-box-shadow: 0 0 9px #333; }
        }

        @-webkit-keyframes orangePulse {
            from { background-color: #d45500; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #ff5c00; -webkit-box-shadow: 0 0 18px #ff5c00; }
            to { background-color: #d45500; -webkit-box-shadow: 0 0 9px #333; }
        }

        @-webkit-keyframes orangellowPulse {
            from { background-color: #fc9200; -webkit-box-shadow: 0 0 9px #333; }
            50% { background-color: #ffb515; -webkit-box-shadow: 0 0 18px #ffb515; }
            to { background-color: #fc9200; -webkit-box-shadow: 0 0 9px #333; }
        }

        a.button {
            -webkit-animation-duration: 2s;
            -webkit-animation-iteration-count: infinite;
        }

        .green.button { -webkit-animation-name: greenPulse; -webkit-animation-duration: 3s; }
        .blue.button { -webkit-animation-name: bluePulse; -webkit-animation-duration: 4s; }
        .red.button { -webkit-animation-name: redPulse; -webkit-animation-duration: 1s; }
        .magenta.button { -webkit-animation-name: magentaPulse; -webkit-animation-duration: 2s; }
        .orange.button { -webkit-animation-name: orangePulse; -webkit-animation-duration: 3s; }
        .orangellow.button { -webkit-animation-name: orangellowPulse; -webkit-animation-duration: 5s; }

        .wall-of-buttons { width: 100%; height: 180px; text-align: center; }
        .wall-of-buttons a.button { display: inline-block; margin: 0 10px 9px 0; }
    </style>
</head>
<body>
    <div class="head" >
        <ul>
            <li class="left">
                <ol >
                    <li style="display: block;width: 65px;height: 65px;"><img src="/images/apple.png" width="65" height="65"></li>
                    <li style="margin: 5px;">
                        <dl>
                            <dt style="font-weight: bold;font-size: 18px;">攻城掠地</dt>
                            <dd style="color: #dcdcdc">
                                <p>版本号 1.7.0</p>
                                <p>大小 10M</p>
                            </dd>
                        </dl>
                    </li>
                </ol>
            </li>
            <li class="right" style="position:relative;">
                <dl style="width:100px;position: absolute;right: 0px;border-left: 1px solid #d3d3d3;height: 70px;margin-top: 5px;padding-left: 5px;">
                    <dt>送5000金币</dt>
                    <dd>  <a class="large red button">下载</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <div class="content" style="border-top: 1px solid #d3d3d3;">
        xxxx
    </div>
    <div class="root" style="text-align: center;border-top: 1px solid #d3d3d3;padding-top: 10px;">
        <a class="large orange button">立即下载安装>></a>
    </div>
</body>
</html>
