<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

    String url=request.getParameter("url");
    if(url==null||"".equals(url)){
    	url="d/p";
    }
    String d=request.getParameter("d");
    String dopath;
    if(d==null){
    	dopath=url;
    }else{
    	dopath=url+"?d="+d;
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>有钱赚 - 中国第一手机赚钱平台!安卓和iPhone都能用!</title>
<meta name="keywords" content="有钱赚,手机赚钱,中国第一手机赚钱,android手机赚钱,android赚钱,安卓赚钱,安卓手机赚钱,网络赚钱,iPhone赚钱,苹果赚钱,苹果手机赚钱" /> 
<meta name="description" content="有钱赚是一款让你在玩手机的同时还能赚到钱的智能手机应用，支持Android安卓和Apple苹果两个系统。玩手机，用有钱赚，轻松赚小钱。" /> 
<style type="text/css">
html,body {background-color:#fff;font-size:14px; font-family: arial,sans-serif; vertical-align:middle; }
a { color:#0000FF; text-decoration: none;}
a:link{ text-decoration:underline}
form{margin:0;}
.title01{ line-height:30px; font-weight:bold; color:#333; border-bottom:1px #ccc solid; font-size:16px; clear:both;}
.content{margin-bottom:10px;line-height:100%;}
.content a{text-decoration:underline}
.block01{ padding:5px 0;}
.block01 h2{ font-size:14px; margin-bottom:5px; color:#ff6633;}
.block01 p{ font-size:14px; line-height:16px; color:#666;margin-top:0;}
.footer{ color:#999; text-align:center; line-height:24px;}
.green{ color:#709828; font-weight:bold; font-family:Arial}
.gray{ font-size:12px; color:#999;}
.yellow{ color:#ff6633;}
.red{ color:#000000; font-size:14px;}
.redbig{ color:#000000; font-size:20px;}
a.yellowtext:link,a.yellowtext:visited{  color:#ff6633; text-decoration: none;}
a.graytext:link,a.graytext:visited{  color:#666; text-decoration: none;}
.button, a.button:link {
  border: 1px #FF6A36 solid;
  font-size: 16px;
  background: #ffffff;
  color: #FF6A36;
  border-radius: 4px;
  -webkit-border-radius: 4px;
  padding: 5px 6px;
  text-decoration: none;
}

.button:hover, .button:visited:hover{
  text-decoration: none;
}
.button:visited{
  background: #ffffff;
  color: #FF6A36;
  text-decoration: none;
}
</style>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
</head>
  <body style="margin:auto;width:320px;">
<div style="background:#ff6633; margin:0px;"><img src="<%=basePath%>images/logo.png" width="320" border="0"/></div>
<div class="content" style="margin:5px;">

<div class="title01">安装后做任务，动动手指就能赚钱</div>
<div class="block01" style="margin-top:5px;">



  <div style="float:left;padding-left:10px; line-height:20px;text-align:center;">
   <a class="yellowtext" href="#">
     <img src="<%=basePath%>images/apple.png" width="100px" border="0"/><br />
     <span style="">苹果用户点击</span>
   </a>
  </div>
  <div style="float:left;padding-left:20px; line-height:20px;text-align:center;">
   <a class="yellowtext" href="<%=dopath%>">
     <img src="<%=basePath%>images/android.png" width="100px" border="0"/><br />
     <span style="">安卓用户点击</span>
   </a>
  </div>



</div>
<div style="clear:both;"></div>


<br />
<div class="title01">产品特性</div>
<div class="block01">
  <p>有钱赚在2014年5月正式发布，是国内第一个专注于移动威客的手机应用，拥有500万用户，是国内最大的手机赚钱应用。</p>
  
	<h2>做任务</h2>
	<p>只需要在手机上点击就能完成的任务，比如装应用、发微博、分享</p>

	<h2>赚金币</h2>
	<p>通过完成各种任务赚取金币，一分钟可以赚够金币兑换1Q币</p>

	<h2>换奖品</h2>
	<p>金币可以换话费充值卡、Q币和支付宝等奖品；<span class="yellow">近一周已成功兑出6560个Q币、8百多张话费充值卡、8千多元的支付宝充值</span></p>
	
	<p>腾讯认证的官方微信号：test<br/>
新浪认证的官方微博：<a href="/weibo.com/mizhuan" target="_blank" class="graytext">/weibo.com/mizhuan</a><br/>
  欢迎和我们互动，我们会努力解答你的各种疑问。
  </p>
</div>
</div>
<div class="footer">@2011-2014 重庆瓦力网络科技有限公司版权所有<br />京ICP备111111号-3</div>

</body>
</html>
