﻿<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>帮助 - 有钱赚 手机赚钱 - 学生赚钱</title>
<title>有钱赚 - 中国手机赚钱平台!安卓和iPhone都能用!</title>
<meta name="keywords" content="有钱赚,安卓赚钱,苹果赚钱,网络赚钱,安卓手机赚钱,苹果手机赚钱,android手机赚钱,android赚钱,iPhone赚钱" /> 
<meta name="description" content="" /> 
<link rel="stylesheet" type="text/css" href="<%=basePath%>/css/mini.css" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>

</head>
<body>
	<div class="container">
		<div class="logo"><h1><a id="link1" href="<%=basePath%>/" title="有钱赚"><img src="<%=basePath%>/images/logo.png" border="0"/></a></h1></div>
		<div class="menu">
			<ul>
				<li><a id="link2" href="<%=basePath%>/">首页</a></li>
				<li ><a id="link8" href="<%=basePath%>/download.jsp">下载</a></li>
				<li class="current"><a id="link3" href="<%=basePath%>/help.jsp">帮助</a></li>
				<li><a id="link4" href="<%=basePath%>/feedback.jsp">问题反馈</a></li>
				<li><a id="link7" href="<%=basePath%>/promote.jsp">推广有钱赚</a></li>
			</ul>
		</div>
	</div>
	
	<div class="content03">
	<div class="askBox">
		<div class="title01">
			<h2>帮助</h2>
		</div>
		<div class="help">
			<h2>什么是有钱赚?</h2>
			<p>一款让你在玩手机的同时还能赚到钱的手机应用，支持Android手机和苹果iPhone,iPad,iPod设备。通过有钱赚，你可以通过安装我们推荐的优秀应用、评论和分享应用、完成指定任务等方式赚取大米，还可以把大米变现成充值卡和Q币等。</p>

			<h2>有钱赚是怎么运作的，为什么能给我赚钱呢?</h2>
			<p>你在寻找好应用的时候，好应用也在找你。商家愿意给活跃的手机用户物质激励来加速应用的传播，也愿意听到来自你们的使用反馈。有钱赚是你和商家之间的桥梁。</p>

			<h2>有钱赚安全吗?</h2>
			<p>是的！有钱赚绝对安全。如果你熟悉Android的权限机制的话，你会发现有钱赚只要求了以下几个权限：网络->所有联网应用都需要; 检索当前应用程序->有钱赚需要知道你是不是成功安装了应用才能给你相应的奖励; 手机状态和身份->用于知道手机的唯一码，防止有些人作弊。</p>
			<p>有钱赚所推荐的应用也是我们测试挑选的，你可以尽管放心安装使用。</p>

			<h2>能赚到很多钱吗?</h2>
			<p>试一下你就知道了。上手三分钟换一个Q币绝对没问题。</p>

			<h2>安装应用没有获得大米? </h2>
			<p>原因是手机的剩余内存不足，处理方式如下：<br/>
			1)卸载已经安装但未获大米的应用<br/>
			2)关掉一些后台服务：设置->应用程序->正在运行的服务<br/>
			3)重新下载安装，安装完成后等待程序自动启动</p>

			<h2>为什么才安装了几个应用就提示“今日赠送大米已达上限”?</h2>
			<p>我们限制了每个手机24小时内能够下载的应用个数。主要的目的是希望用户有足够的时间体验已经安装完成的应用。</p>

			<h2>为什么过了一天还提示“今日赠送大米已达上限”，无法继续装应用?</h2>
			<p>我们的限制是24小时，所以请耐心等待。有钱赚希望带给大家这样的体验，每天只用一点时间玩有钱赚和玩应用的同时还能有小收入。</p>
			
			<h2>下载应用赚大米如何节省流量?</h2>
			<p>建议大家请用wifi下载应用。使用手机流量套餐的用户请自己把握套餐的剩余额度。应用的大小不等，在下载之前的页面可以看到应用大小。</p>

			<h2>我没有wifi怎么办?</h2>
			<p>告诉大家一个笔记本做无线热点的方法，这样用有钱赚的时候就不用心疼流量了。

			<h2>请问怎样才能快速赚更多大米?</h2>
			<p>除了装应用外，最快速的方式是邀请其他用户加入。除了一次性的大米奖励外，被邀请用户的后续使用还能带给你分成。另外，有钱赚还提供了幸运竞猜的玩法，你可以试试运气。有钱赚只能让你每天只用一点时间玩有钱赚和玩应用的同时还能有小收入。</p>

			<h2>怎么老是提示第三方积分错误，无法更新积分?</h2>
			<p>出现这样的提示一般是网络故障，建议多尝试几次。</p>

			<h2>为什么有些应用明明写了送大米，但是安装了之后却得不到大米?</h2>
			<p>这是因为系统记录此手机之前已经安装过相关应用，即使写了送大米，也算重复安装，得不到大米。</p>

			<div class="clear"></div>
		</div>
	</div>
	
	
	
	</div>

    <div id="footer">
        <div class="limit">
           <div class="footer-nav">
                  <a id="link5" href="<%=basePath%>about.jsp">关于有钱赚</a>
                <a id="link6" href="<%=basePath%>jobs.jsp">招贤纳士</a>
                
           </div>
           <div class="copyright">&copy; 2011-2014 重庆瓦力网络科技有限公司 版权所有 (京ICP备11018675号-3) </div>
        </div>
    </div>
  

</body>
</html>
