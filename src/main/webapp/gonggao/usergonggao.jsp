<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		parent.$.messager.progress('close');
		$('#form').form({
			url : '${pageContext.request.contextPath}/gonggaoController/eidtegonggao',
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
                var isValid = $(this).form('validate');
                if (!isValid) {
                    parent.$.messager.progress('close');
                }
				return isValid;
			},
			success : function(result) {
				parent.$.messager.progress('close');

				result = $.parseJSON(result);

				if (result.success) {
					parent.$.messager.alert('提示', result.msg, 'info');
					parent.$.modalDialog.handler.dialog('close');
                    parent.$.modalDialog.openner_dataGrid.datagrid('reload');
				} else {
					parent.$.messager.alert('错误', result.msg, 'error');
				}
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<c:if test="${sessionInfo.name == null}">
			<img src="${pageContext.request.contextPath}/style/images/blue_face/bluefaces_35.png" alt="" />
			<div>登录已超时，请重新登录，然后再刷新本功能！</div>
			<script type="text/javascript" charset="utf-8">
				try {
					parent.$.messager.progress('close');
				} catch (e) {
				}
			</script>
		</c:if>
		<c:if test="${sessionInfo.name != null}">
			<form id="form" method="post">
				<table class="table table-hover table-condensed">
					<tr>
						<th>标题</th>
						<td><input value="${gonggao.title}" name="title"></td>
					</tr>
					<tr>
						<th>内容</th>
						<td>
                            <textarea rows="10" cols="5" name="content">${gonggao.content}</textarea>
						</td>
					</tr>
                    <input type="hidden" name="id" value="${gonggao.id}">
				</table>

			</form>
		</c:if>
	</div>
</div>