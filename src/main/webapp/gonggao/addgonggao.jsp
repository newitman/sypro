<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function() {
        parent.$.messager.progress('close');

        $('#form').form({
            url : '${pageContext.request.contextPath}/gonggaoController/publicgonggao',
            onSubmit : function() {
                parent.$.messager.progress({
                    title : '提示',
                    text : '数据处理中，请稍后....'
                });
                var isValid = $(this).form('validate');
                if (!isValid) {
                    parent.$.messager.progress('close');
                }
                return isValid;
            },
            success : function(result) {
                parent.$.messager.progress('close');
                result = $.parseJSON(result);
                if (result.success) {
                    parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_treeGrid这个对象，是因为role.jsp页面预定义好了
                    parent.$.modalDialog.handler.dialog('close');
                }
            }
        });



    });


</script>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;">
        <form id="form" method="post">
            <table class="table table-hover table-condensed">
                <tr>
                    <th>标题</th>
                    <td><input name="title" id="title" type="text"  class="span2" data-options="required:true" ></td>
                </tr>
                <tr>
                    <th>内容</th>
                    <td>
                        <textarea cols="10" rows="5" id="content" name="content"></textarea>
                    </td>
                </tr>
            </table>

           <!-- <input type="button" value="发布" id="publishbuttion">--->
        </form>
    </div>
</div>