<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>发布互动广场信息</title>
<meta name="keywords" content="有钱赚,安卓赚钱,苹果赚钱,网络赚钱,安卓手机赚钱,苹果手机赚钱,android手机赚钱,android赚钱,iPhone赚钱" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="<%=basePath%>css/mini.css?v=2014" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<!-- 配置文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="<%=basePath%>interactadmin/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-2.0.0.js"></script>
<script type="text/javascript">

    parent.parent.$.messager.progress('close');

	$(function(){
        parent.$.messager.progress('close');

		$("#submitbutton").click(function(){
			var cotent=editor.getContent();
			var imageurl=getimagurl(cotent);
			var title=$("#title").val();
			
			if(title===""||cotent===""){
				alert("标题或内容不能为空@!@");
			}else{
				$.post("<%=basePath%>interactController/uploadcontent",{random:Math.random(),content:cotent,imageurl:imageurl,title:title},function(rs){
					var msg=eval("("+rs+")");
			
					alert(msg.msg);
                    editor.setContent("");
					$("#title").val("");
				});
			}
					
		});
		
		$("#cleartbutton").click(function(){

			$("#title").val("");
			editor.setContent("");
		});
	});
	
	function getimagurl(cotent){
		
		  var re=/(http(s)?\:\/\/)?(www\.)?(\w+\:\d+)?(\/\w+)+\.(swf|gif|jpg|bmp|jpeg|png)/gi;
		  var arr=cotent.match(re);


		
		if(arr!=null&&arr!=undefined){
			if(arr.length>0){
				return arr[0];
			}
		}
		
		return "";
	}
</script>
</head>
<body >
	<script id="container" name="content" type="text/plain">
    	${interact.content}
	</script>
	<script type="text/javascript">
	    var editor = UE.getEditor('container')
	</script>
	<div>标题：</div>
	<div>
		<textarea rows="3" cols="30" id="title">${interact.title}</textarea>
	</div>
	<input type="button" id="submitbutton" value="发布"><input type="button" id="cleartbutton" value="清空">
</body>
</html>
