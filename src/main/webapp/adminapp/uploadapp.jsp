<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	$(function() {
		
		parent.$.messager.progress('close');
		
		$("#submitbuttion").click(function(){
			ajaxFileUpload();
		});
		
		$("#flash").click(function(){
			//parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
			reload();
		});
	});
	function ajaxFileUpload(){
		
	    $.ajaxFileUpload(
	        {
	     url:'${pageContext.request.contextPath}/appController/appupload?appname='+$("#appname").val()+'&integral='+$("#integral").val()+'&description='+$("#description").val(),            //需要链接到服务器地址
	     secureuri:false,
	     fileElementId:'myupfileid',                        //文件选择框的id属性
	     dataType: 'json',                                     //服务器返回的格式，可以是json, xml
	     success: function (data, status)            //相当于java中try语句块的用法
	     {      
	    	 parent.$.messager.alert('添加成功');
	    	 reset();
	    	 reload();
	     },
	     error: function (data, status, e)            //相当于java中catch语句块的用法
	     {
	    	 parent.$.messager.alert('添加失败');
	    	 reset();
	    	 reload();
	     }
	 });
	}
	
	function reset(){
		$("#appname").val("");
		$("#integral").val("");
		$("#description").val("");
	}
	
	function reload(){
		var dataGrid = $('#dataGrid').datagrid({
			url : '${pageContext.request.contextPath}/appController/dataGrid',
			fit : true,
			fitColumns : true,
			border : false,
			pagination : true,
			idField : 'id',
			pageSize : 10,
			pageList : [ 10, 20, 30, 40, 50 ],
	
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : false,
			frozenColumns : [ [ {
				field : 'id',
				title : '编号',
				width : 150,
				checkbox : true
			}, {
				field : 'appname',
				title : 'APP名字',
				width : 80,
				sortable : false,
				formatter : function(value, row, index) {
					return "<img width='160' height='58' src='${pageContext.request.contextPath}"+row.imagePath+"'></img>";
				}
			},{
				field : 'appAddress',
				title : 'APP地址',
				width : 80,
				sortable : false,
				
			} ,{
				field : 'integral',
				title : '下载积分',
				width : 80,
				sortable : false,
				formatter : function(value, row, index) {
					return "可获得"+row.integral+"积分";
				}
			}] ],
			columns : [ [ {
				field : 'description',
				title : 'APP描述',
				width : 60,
				
			}, {
				field : 'action',
				title : '操作',
				width : 100,
				formatter : function(value, row, index) {
					var str = '';
					
					if ($.canDelete) {
						str += $.formatString('<img onclick="deleteFun(\'{0}\');" src="{1}" title="删除"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/cancel.png');
					}
					
					return str;
				}
			} ] ],
			toolbar : '#toolbar',
			onLoadSuccess : function() {
				$('#searchForm table').show();
				parent.$.messager.progress('close');

				$(this).datagrid('tooltip');
			},
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll').datagrid('uncheckAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});
	}
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<form id="form" method="post">
			<table class="table table-hover table-condensed">
				<tr>
					<th>编号</th>
					<td><input name="id" type="text" class="span2" value="" readonly="readonly"></td>
					<th>APP名称</th>
					<td><input name="appname" id="appname" type="text" placeholder="请输入APP名称" class="easyui-validatebox span2" data-options="required:true" value=""></td>
				</tr>
				<tr>
					<th>下载积分</th>
					<td><input name="integral" id="integral" value="100" class="easyui-numberspinner" style="width: 140px; height: 29px;" required="required" ></td>
					<th>文件</th>
					<td><input name="files" type="file" id="myupfileid" class="span2"/></td>
				</tr>
				<tr>
					<th>APP描述</th>
					<td colspan="3"><textarea name="description" rows="" cols="" class="span5" id="description"></textarea></td>
				</tr>
				<tr>
					<td colspan="3"><input type="button" id="submitbuttion" value="添加"></input></td>
					<td colspan="3"><input type="button" id="flash" value="刷新"></input></td>
				</tr>
			</table>
		</form>
	</div>
</div>