<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="<%=basePath%>css/mini.css?v=2014" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<!-- 配置文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="<%=basePath%>interactadmin/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="<%=basePath%>interactadmin/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="<%=basePath%>jslib/jquery-2.0.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jslib/ajaxfileupload2.js"></script>

<script type="text/javascript">
    var editor;
	$(function() {
		parent.$.messager.progress('close');
        $("#submitbutton").click(function(){
            var cotent=editor.getContent();
            var imageurl=getimagurl(cotent);
            var cotent=cotent.replace(/\"/g, "\'");

            if(!isNum($("#integral").val())){
                alert("积分必须为整数！");
                return;
            }

            uploadOrder(cotent);

        });
        function isNum(num){
            var reNum =/^\d*$/;
            return (reNum.test(num));
        }
        function ajaxFileUpload(imageurl){

            $.ajaxFileUpload(
                    {
                        url:'${pageContext.request.contextPath}/appController/appupload?appname='+$("#appname").val()+'&integral='+$("#integral").val()+'&description='+$("#description").val()+"&imageurl="+imageurl,            //需要链接到服务器地址
                        secureuri:false,
                        fileElementId:'myupfileid',                        //文件选择框的id属性
                        dataType: 'json',                                     //服务器返回的格式，可以是json, xml
                        success: function (data, status)            //相当于java中try语句块的用法
                        {
                            parent.$.messager.alert('添加成功');

                            editor.setContent("");
                            $("#appname").val("");
                            $("#integral").val("");
                            $("#description").val("");

                        },
                        error: function (data, status, e)            //相当于java中catch语句块的用法
                        {
                            parent.$.messager.alert('添加失败');

                        }
                    });
        }

        function uploadOrder(cotent) {
            //var r = $("#orderUploadForm").form('validate');
            //if(!r) {
            // return ;
            //}
            var uplist = $("input[name^=files]");
            var arrId = [];
            for ( var i = 0; i < uplist.length; i++) {
                if (uplist[i].value) {
                    arrId[i] = uplist[i].id;

                }
            }

            $.ajaxFileUpload({
                url : '${pageContext.request.contextPath}/appController/appupload?appname='+$("#appname").val()+'&integral='+$("#integral").val()+'&description='+cotent+"&version="+$("#version").val(), //用于文件上传的服务器端请求地址
                secureuri : false,//一般设置为false
                fileElementId : ['appfile','applogo'],//文件上传空间的id属性  <input type="file" id="file" name="file" />
                dataType : 'json',//返回值类型 一般设置为json
                success : function(data, status) //服务器成功响应处理函数
                {
                    if (data) {
                      //  $.messager.alert("提示", data.result, "info");
                    } else {
                     //   $.messager.alert("提示", data.result, "error");
                    }
                    alert("发布成功");
                },
                error : function(data, status, e)//服务器响应失败处理函数
                {
                   alert("发布成功");
                }
            });
        }

        $("#cleartbutton").click(function(){

            editor.setContent("");
            $("#appname").val("");
            $("#integral").val("");
            $("#description").val("");
        });

	});

    function getimagurl(cotent){

        var re=/(http(s)?\:\/\/)?(www\.)?(\w+\:\d+)?(\/\w+)+\.(swf|gif|jpg|bmp|jpeg|png)/gi;
        var arr=cotent.match(re);

        // for(var i=0;i<arr.length;i++){
        //   document.getElementById("txt").value+=arr[i]+"\n";
        //  }

        if(arr!=null&&arr!=undefined){
            if(arr.length>0){
                return arr[0];
            }
        }

        return "";
    }

</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">

			<table class="table table-hover table-condensed">
                <tr>
                    <th style="width:100px;">APP名称</th>
                    <td><input name="appname" id="appname" type="text" placeholder="请输入APP名称" style="width: 300px; height: 29px;" class="easyui-validatebox span2" data-options="required:true" value=""></td>

                </tr>
                <tr>
                    <th>下载积分</th>
                    <td><input name="integral" id="integral" value="100" class="easyui-numberspinner" style="width: 300px; height: 29px;" required="required" ></td>

                </tr>
                <tr>
                    <th>安装包</th>
                    <td><input name="appfile" type="file" id="appfile" class="span2" style="width:300px;height: 29px;"/></td>
                </tr>
                <tr>
                    <th>Logo</th>
                    <td><input name="applogo" type="file"  id="applogo" class="span2" style="width:300px;height: 29px;"/></td>
                </tr>
                <tr>
                    <th style="width:100px;">版本号</th>
                    <td colspan="3"><input name="version" class="span5" id="version"></td>
                </tr>
                <tr>
                    <th>
                        描述
                    </th>
                    <td>
                        <script id="container" name="content" type="text/plain">
                            ${tad.content}
                        </script>
                        <script type="text/javascript">
                            if(editor==null||editor==undefined){
                                editor = UE.getEditor('container');
                            }else{
                                editor.destroy();
                                editor = UE.getEditor('container');
                            }

                        </script>
                    </td>
                </tr>

			</table>
            <input type="hidden" id="adid" name="id" value="${tad.id}">
            <input type="button" id="submitbutton" value="发布"><input type="button" id="cleartbutton" value="清空">

	</div>
</div>