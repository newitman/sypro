<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		parent.$.messager.progress('close');
		$('#form').form({
			url : '${pageContext.request.contextPath}/appController/edit',
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				var isValid = $(this).form('validate');
				if (!isValid) {
					parent.$.messager.progress('close');
				}
				return isValid;
			},
			success : function(result) {
				
				var msg=eval("("+result+")");
				
				parent.$.messager.progress('close');
			
				parent.$.messager.alert("更新成功");
				parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为user.jsp页面预定义好了
				parent.$.modalDialog.handler.dialog('close');
			
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<form id="form" method="post">
			<table class="table table-hover table-condensed">
				<tr>
					<th>编号</th>
					<td><input name="id" type="text" class="span2" value="${appinfo.id }" readonly="readonly"></td>
					<th>APP名称</th>
					<td><input name="appname" type="text" placeholder="请输入APP名称" class="easyui-validatebox span2" data-options="required:true" value="${appinfo.appname}"></td>
				</tr>
				<tr>
					<th>可获得积分</th>
					<td><input name="integral" type="text" class="easyui-numberspinner" value="${appinfo.integral }"></td>
					<th>APP描述</th>
					<td><input name="description" type="text" placeholder="请输入APP描述" class="easyui-validatebox span2" data-options="required:true" value="${appinfo.description}"></td>
				</tr>
			</table>
		</form>
	</div>
</div>