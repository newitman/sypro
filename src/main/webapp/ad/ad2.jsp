<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link href="<%=basePath%>style/sypro/codiqa.ext.css" rel="stylesheet">
  <link href="<%=basePath%>jslib/sypro/jquery.mobile-1.4.0.css" rel="stylesheet">

  <script src="<%=basePath%>jslib/sypro/jquery.min.js"></script>
  <script src="<%=basePath%>jslib/sypro/jquery.mobile-1.4.0.min.js"></script>
  <script src="<%=basePath%>jslib/sypro/codiqa.ext.js"></script>


   <script type="text/javascript" >
   
   
   	$(function(){

	
		$("#hdjs").click(function(){
			
			$("#pagediv1").show();
			$("#pagediv2").hide();
			$("#pagediv3").hide();
		});
		$("#tglj").click(function(){
			$("#pagediv1").hide();
			$("#pagediv2").show();
			$("#pagediv3").hide();
		});
		$("#hjpm").click(function(){
			$("#pagediv1").hide();
			$("#pagediv2").hide();
			$("#pagediv3").show();
		
		});
		
		//获得获奖排名
		
		
	});
   
   </script>
   
   function getcontent(url){
   
	   $.post(url,{random:Math.random()},function(rs){
	   
			return rs;
	   });
   		
   }
   
</head>
<body>
<div data-role="page" data-control-title="Home" id="page1">
    <div role="main" class="ui-content">
        <div style="width: auto; height: 212px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img  src="<%=basePath%>ad/images/ad2.png" alt="image" style="position: absolute; ">
        </div>
        <div data-role="navbar" data-iconpos="top">
            <ul>
                <li>
                    <a href="javascript:void(0);" data-transition="none" data-theme="" data-icon="" id="hdjs">
                        活动介绍
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" data-transition="fade" data-theme="" data-icon="" id="tglj">
                        推广链接
                    </a>
                </li>
                <li style="display: none;">
                    <a href="javascript:void(0);" data-transition="fade" data-theme="" data-icon="" id="hjpm">
                        获奖排名
                    </a>
                </li>
            </ul>
        </div>
		
		<div class="cotent" id="contentid">
				
	<div  data-control-title="huodong" id="pagediv1">
    <div role="main" class="ui-content">
        <div data-controltype="textblock">
            <p>
                <span style="color: rgb(255, 0, 0); font-size: large;">
                    <b>
                        <span id="_mce_caret" data-mce-bogus="true" style="border-style: solid; border-radius: 15px;background:red;">
                            ﻿<font color="white">活动时间</font>
                        </span>
                        
                    </b>
                </span>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <b>
                    2014年3月1日00:00~2014年3月31日24:00
                </b>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <span style="color: rgb(255, 0, 0); font-size: large;" style="border-style: solid; border-radius: 15px;background:red;">
                    <b>
					 <span id="_mce_caret" data-mce-bogus="true" style="border-style: solid; border-radius: 15px;background:red;">
                            ﻿<font color="white"> 活动规则</font>
                        </span>
                       
                    </b>
                </span>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <strong>
                    在活动期间内，邀请好友安装有钱赚并成功兑换一次计一次有效邀请，邀请的好友如果是苹果设备注册的，那么记作一个苹果有效邀请。主办方会对
                    <span style="color: rgb(255, 0, 0);" data-mce-style="color: #ff0000;">
                        邀请苹果有效用户
                    </span>
                    和
                    <span style="color: rgb(255, 0, 0);" data-mce-style="color: #ff0000;">
                        邀请安卓有效用户
                    </span>
                    分别进行排序，分别取前30名进行奖励
                </strong>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <span >
                      <b>
					  <span id="_mce_caret" data-mce-bogus="true" style=" border-radius: 15px;background:red;">
                            ﻿<font color="white">﻿ 奖项设置</font>
                        </span>
                       
                    </b>
					
					 
                </span>
         
                       
                   
                </span>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <b>
                    1.苹果榜单和安卓榜单第一名分别奖iPhone 5S 一台；
                </b>
            </p>
            <p>
                <b>
                    2.苹果榜单和安卓榜单第二名分别奖励ipad air一台；
                </b>
            </p>
            <p>
                <b>
                    3.苹果榜单和安卓榜单第三名分别奖励iPod touch一台；
                </b>
            </p>
            <p>
                <b>
                    4.苹果榜单和安卓榜单第四名~10名中分别奖励iPod Shuffle一台
                </b>
            </p>
            <p>
                <b>
                    5.苹果榜单和安卓榜单第11~30名中分别抽取2位送ipad mini 一台，未被抽中的奖励2000万学币
                </b>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <span style="font-size: large; color: rgb(255, 0, 0);" style="border-style: solid; border-radius: 15px;background:red;">
                    <b>
                        
						 <span id="_mce_caret" data-mce-bogus="true" style="border-style: solid; border-radius: 15px;background:red;">
                            ﻿<font color="white">﻿抽奖规则</font>
                        </span>
                    </b>
                </span>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <strong>
                    活动截至后，统计4月1日（第2014035期）双色球中奖结果，从中奖结果的第一个数开始看，如果这个数在11~30区间内，则该号码为中奖号码，以此选出2个中奖号码假如第2014035期开奖号码为：05
                    07 08 20 31 33 11 。则中奖用户为第20期和11名若2014035期未能成功选出2个中奖号码，则从下一期开奖号码中重新选取，以此类推。
                </strong>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <span style="font-size: large; color: rgb(255, 0, 0);" style="border-style: solid; border-radius: 15px;background:red;">
                    <b>
					 <span id="_mce_caret" data-mce-bogus="true" style="border-style: solid; border-radius: 15px;background:red;">
                            ﻿<font color="white">﻿ 奖品预览</font>
                        </span>
                       
                    </b>
                </span>
            </p>
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img width="288" height="200" src="<%=basePath%>ad/images/1.png" alt="image" style="position: absolute;">
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img width="288" height="200" src="<%=basePath%>ad/images/2.png" alt="image" style="position: absolute; ">
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img width="288" height="200" src="<%=basePath%>ad/images/3.png" alt="image" style="position: absolute;">
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img width="288" height="200" src="<%=basePath%>ad/images/t.png" alt="image" style="position: absolute; ">
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img width="288" height="200" src="<%=basePath%>ad/images/h.png" alt="image" style="position: absolute; ">
        </div>
        <div data-controltype="textblock">
            <p style="text-align: center;">
                <strong>
                    其他说明
                </strong>
            </p>
            <p style="text-align: left;">
                1.活动主办方有钱赚的员工及其家属不得参与本次活动，以示公允。
            </p>
            <p style="text-align: left;">
                2.为了保证活动的公平性，凡在本活动中存在以技术手段进行任何作弊行为，主办方有权直接取消其参与活动的资格，严重者直接进行封号处理。
            </p>
            <p style="text-align: left;">
                3.在法律允许范围内，主办方享受本次活动最终解释权。
            </p>
        </div>
    </div>
</div>
<div  data-control-title="tuiguanglianjie" id="pagediv2">
    <div role="main" class="ui-content">
        <div data-controltype="textblock">
            <p>
                <span style="font-size: large; color: rgb(255, 0, 0);">
                    <b>
                        您的专属二维码：
                    </b>
                </span>
            </p>
        </div>
        <div style="width: 288px; height: 200px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;"
        data-controltype="image">
            <img src="https://codiqa.com/static/images/v2/image.png" alt="image" style="position: absolute; top: 50%; left: 50%; margin-left: -16px; margin-top: -18px">
        </div>
        <div data-controltype="textblock">
            <p>
                <span style="font-size: large; color: rgb(255, 0, 0);">
                    <b>
                        您的专属推广链接：
                    </b>
                </span>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <b>
                    http://www.schoolhcg.com
                </b>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <strong>
                    <span style="font-size: large; color: rgb(255, 0, 0);" data-mce-style="font-size: large; color: #ff0000;">
                        一键安装推广链接：
                    </span>
                </strong>
            </p>
        </div>
        <div data-controltype="textblock">
            <p>
                <span style="background-color: rgb(136, 136, 136); color: rgb(255, 0, 0);">
                    <b>
                        http://www.schoolhcg.com
                    </b>
                </span>
            </p>
        </div>
    </div>
</div>
<div  data-control-title="huojiangpaiming" id="pagediv3">
    <div role="main" class="ui-content">
        <div data-controltype="textblock">
            <p style="text-align: center;">
                <span style="font-size: large;">
                    <b>
                        邀请苹前30名
                    </b>
                </span>
            </p>
        </div>
        <div class="ui-grid-b">
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
        </div>
        <div data-controltype="textblock">
            <p style="text-align: center;">
                <span style="font-size: large;">
                    <b>
                        邀请安卓前30名
                    </b>
                </span>
            </p>
        </div>
        <div class="ui-grid-b">
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
            <div class="ui-block-a">
            </div>
            <div class="ui-block-b">
            </div>
            <div class="ui-block-c">
            </div>
        </div>
    </div>
</div>
			
		
		</div>
		
    </div>
	
</div>

</body>
</html>