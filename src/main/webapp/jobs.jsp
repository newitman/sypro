﻿<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>l">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>招贤纳士 - 米赚 手机赚钱</title>
<meta name="keywords" content="招贤纳士,米赚招聘" /> 
<meta name="description" content="米赚招聘职位列表" /> 
<link rel="stylesheet" type="text/css" href="/css/mini.css" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>

</head>
<body>
	<div class="container">
		<div class="logo"><h1><a id="link1" href="<%=basePath%>/" title="米赚"><img src="<%=basePath%>/imageslogo.png" border="0"/></a></h1></div>
		<div class="menu">
			<ul>
				<li class="current"><a id="link2" href="<%=basePath%>">首页</a></li>
				<li ><a id="link8" href="<%=basePath%>download.jsp">下载</a></li>
				<li><a id="link3" href="<%=basePath%>help.jsp">帮助</a></li>
				<li><a id="link4" href="<%=basePath%>feedback.jsp">问题反馈</a></li>
				<li><a id="link7" href="<%=basePath%>promote.jsp">推广米赚</a></li>
			</ul>
		</div>
	</div>
	
	<div class="content03">
	<div class="askBox">
		<div class="title01">
			<h2>招贤纳士</h2>
		</div>
		<div class="help">
		  <p>请发送简历到：hr@mizhuan.me</p>
      <h2>[ Android应用开发工程师/高级工程师 ]</h2>
      <p><strong>职位要求</strong></p>
      <p style="line-height:150%">
1、CS/EE或相关专业，专业知识扎实;<br />
2、有实际Android应用开发工作经验，有多个演示作品;<br />
3、精通Java语言，精通Android开发;<br />
4、熟悉MySQL, PHP, HTML，CSS， subversion，bugzilla（trac）者优先;<br />
5、可以使用Linux环境;<br />
6、有其他移动平台开发经验者优先: iPhone，Symbian;<br />
7、关注WEB2.0应用和移动平台技术(Android, iphone, webkit)发展;<br />
8、有创业激情！求知欲强，学习能力强，有高度责任感和集体荣誉感;
      </p>
			<h2>[产品推广专员]</h2>
      <p><strong>职责描述</strong></p>
      <p style="line-height:150%">
1、配合技术和产品部门，制订相关产品的推广方案，并熟练运用多种推广手段执行； <br />
2、联系公司跨部门相应外界资源丰富产品合作对象； <br />
3、与市场推广、公关同事配合，提供准确的产品宣传基础内容； <br />
4、定期跟踪产品流量等后台数据，及时反馈并改进推广方法。
       </p>
       <p><strong>职位要求</strong></p>
       <p style="line-height:150%">
1、大学本科及以上学历，中文表达流利； <br />
2、具备优秀的数据分析和逻辑分析能力、推广策划能力以及沟通表达能力； <br />
3、工作积极主动，乐观开朗，善于接受挑战，能够承受一定的工作压力，具有良好的团队合作精神。 
       </p>
			<h2>[校园推广兼职]</h2>
      <p><strong>职责描述</strong></p>
      <p style="line-height:150%">
1、配合产品，制定校园内的推广方案，并熟练运用多种推广手段执行； <br />
2、定期跟踪产品流量等后台数据，及时反馈并改进推广方法。
       </p>
       <p><strong>职位要求</strong></p>
       <p style="line-height:150%">
1、大学本科及以上学历； <br />
2、具备优秀的数据分析和逻辑分析能力、推广策划能力以及沟通表达能力； <br />
3、工作积极主动，乐观开朗，善于接受挑战，能够承受一定的工作压力，具有良好的团队合作精神。 
       </p>
		<div class="clear"></div>
		</div>
	</div>
	
	
	</div>

    <div id="footer">
        <div class="limit">
           <div class="footer-nav">
                <a id="link5" href="<%=basePath%>about.jsp">关于有钱赚</a>
                <a id="link6" href="<%=basePath%>jobs.jsp">招贤纳士</a>
                
           </div>
           <div class="copyright">&copy; 2011-2014 北京米拓世纪科技有限公司 版权所有 (京ICP备11018675号-3) </div>
        </div>
    </div>
	  
</body>
</html>
