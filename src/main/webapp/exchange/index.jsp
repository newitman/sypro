<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<title>兑换记录管理</title>
<jsp:include page="../inc.jsp"></jsp:include>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/editPage')}">
	<script type="text/javascript">
		$.canEdit = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/delete')}">
	<script type="text/javascript">
		$.canDelete = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/grantPage')}">
	<script type="text/javascript">
		$.canGrant = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/editPwdPage')}">
	<script type="text/javascript">
		$.canEditPwd = true;
	</script>
</c:if>
<script type="text/javascript">
	var dataGrid;
	var userLoginCombobox;
	$(function() {
	  
		dataGrid = $('#dataGrid').datagrid({
			url : '${pageContext.request.contextPath}/texchangeController/dataGrid',
			fit : true,
			fitColumns : true,
			border : false,
			pagination : true,
			idField : 'id',
			pageSize : 10,
			pageList : [ 10, 20, 30, 40, 50 ],
			sortName : 'userid',
			sortOrder : 'asc',
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : false,
			frozenColumns : [[{
				field : 'id',
				title : 'other',
				align : 'center',
				width : 80,
				sortable : true,
				checkbox : true
			},{
				field : 'nicknamecp',
				title : '用户名称',
				align : 'center',
				width :80,
				sortable : true
			} ] ],
			columns : [ [ {
				field : 'useridcp',
				align : 'center',
				title : '用户ID',
				width : 80
			},{
				field : 'cellPhone',
				align : 'center',
				title : '电话',
				width : 80
			}, {
				field : 'alipay',
				title : '支付宝',
				align : 'center',
				width : 80
			}, {
				field : 'qq',
				title : 'QQ',
				align : 'center',
				width : 80
			}, {
				field : 'goods',
				title : '兑换数量',
				align : 'center',
				width : 50,
				sortable : true
			}, {
				field : 'exchangecontent',
				title : '兑换内容',
				align : 'center',
				width : 80
			}, {
				field : 'exchangetime',
				title : '兑换时间',
				align : 'center',
				width : 80,
				sortable : true
			}, {
				field : 'changestatus',
				title : '兑换状态',
				align : 'center',
				width : 80,
				sortable : true,
					formatter : function(value, row, index) {
					var str="";
					if(value == 1){
						str = "<lable style='color:green;font-weight:bold'>审核成功</lable>";
					}else if(value == 0){
					 	str = "<lable style='color:#999999;font-weight:bold'>待审核</lable>";
					}else if(value == 3){
						str = "<lable style='color:red;font-weight:bold'>审核失败</lable>";
					}else{
					
					}
					return str;
				} 
			},{
				field : 'action',
				title : '操作',
				width : 50,
				formatter : function(value, row, index) {
					var str = '';
					str += '&nbsp;&nbsp;';
					if(row.changestatus == 0 ){
					    if(row.type == 1){//Q币
					    	str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.qq,row.score,row.userid);
					    }else if(row.type == 2){//qq业务
					    	str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.qq,row.score,row.userid);
					    }else if(row.type == 3){//支付宝
					   		str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.alipay,row.score,row.userid);
					    }else if(row.type == 4){//话费
					   		str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.cellPhone,row.score,row.userid);
					    }else if(row.type == 5){//财付通
					    	str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.type,row.score,row.userid);
					    }else if(row.type == 6){//苹果
					    	str += $.formatString('<img onclick="checkStatu(\'{0}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');" src="{1}" title="审核"/>', row.id, '${pageContext.request.contextPath}/style/images/extjs_icons/disk.png',row.type,row.type,row.score,row.userid);
					    }
					}
					return str;
				}
			} ] ],
			toolbar : '#toolbar',
			onLoadSuccess : function() {
				$('#searchForm table').show();
				parent.$.messager.progress('close');
				$(this).datagrid('tooltip');
			},
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll').datagrid('uncheckAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});
		
			userLoginCombobox = $('#userLoginCombobox').combobox({
			url : '${pageContext.request.contextPath}/texchangetypeController/Combobox',
			valueField : 'typeid',
			textField : 'typecontent',
			required : true,
			panelHeight : 'auto',
			delay : 500
		});
		  $('#userLoginCombobox').combobox('setValue', '0');
	});

	function checkStatu(id,type,typenum,score,sessionid) {
		var warn = "";
		var param = 0;
		if (id == undefined) {//点击右键菜单才会触发这个
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {			  //点击操作里面的删除图标会触发这个
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.messager.confirm('温馨提示', "确定审核该用户", function(b) {
			if (b) {
					parent.$.messager.progress({
						title : '提示',
						text : '数据处理中，请稍后....'
					});
					$.post('${pageContext.request.contextPath}/texchangeController/checkexchange',
					 {id:id,type:type,typenum:typenum,score:score,sessionid:sessionid},function(result) {
					 	parent.$.messager.alert('提示', result.msg, 'info');
						dataGrid.datagrid('reload');
						parent.$.messager.progress('close');
					}, 'JSON');
			}
		});
	}
	function editPwdFun(id) {
		dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		parent.$.modalDialog({
			title : '编辑用户密码',
			width : 500,
			height : 300,
			href : '${pageContext.request.contextPath}/userController/editPwdPage?id=' + id,
			buttons : [ {
				text : '编辑',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#form');
					f.submit();
				}
			} ]
		});
	}

	function deleteFun(id) {
		if (id == undefined) {//点击右键菜单才会触发这个
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {//点击操作里面的删除图标会触发这个
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.messager.confirm('询问', '您是否要删除当前用户？', function(b) {
			if (b) {
				var currentUserId = '${sessionInfo.id}';/*当前登录用户的ID*/
				if (currentUserId != id) {
					parent.$.messager.progress({
						title : '提示',
						text : '数据处理中，请稍后....'
					});
					$.post('${pageContext.request.contextPath}/userController/delete', {
						id : id
					}, function(result) {
						if (result.success) {
							parent.$.messager.alert('提示', result.msg, 'info');
							dataGrid.datagrid('reload');
						}
						parent.$.messager.progress('close');
					}, 'JSON');
				} else {
					parent.$.messager.show({
						title : '提示',
						msg : '不可以删除自己！'
					});
				}
			}
		});
	}

	function batchDeleteFun() {
		var rows = dataGrid.datagrid('getChecked');
		var ids = [];
		if (rows.length > 0) {
			parent.$.messager.confirm('确认', '您是否要删除当前选中的项目？', function(r) {
				if (r) {
					parent.$.messager.progress({
						title : '提示',
						text : '数据处理中，请稍后....'
					});
					for (var i = 0; i < rows.length; i++) {
							ids.push(rows[i].id);
					}
					$.getJSON('${pageContext.request.contextPath}/texchangeController/batchDelete', {
						ids : ids.join(',')
					}, function(result) {
					if (result.success) {
						dataGrid.datagrid('load');
						dataGrid.datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
					}
					
				    parent.$.messager.alert('提示', result.msg, 'info');
					parent.$.messager.progress('close');
				});
			   }
			});
		} else {
			parent.$.messager.show({
				title : '提示',
				msg : '请勾选要删除的记录！'
			});
		}
	}

	function editFun(id) {
		if (id == undefined) {
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.modalDialog({
			title : '编辑用户',
			width : 500,
			height : 300,
			href : '${pageContext.request.contextPath}/userController/editPage?id=' + id,
			buttons : [ {
				text : '编辑',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#form');
					f.submit();
				}
			} ]
		});
	}

	function addFun() {
		parent.$.modalDialog({
			title : '添加用户',
			width : 500,
			height : 300,
			href : '${pageContext.request.contextPath}/userController/addPage',
			buttons : [ {
				text : '添加',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#form');
					f.submit();
				}
			} ]
		});
	}

	function batchGrantFun() {
		var rows = dataGrid.datagrid('getChecked');
		var ids = [];
		if (rows.length > 0) {
			for ( var i = 0; i < rows.length; i++) {
				ids.push(rows[i].id);
			}
			parent.$.modalDialog({
				title : '用户授权',
				width : 500,
				height : 300,
				href : '${pageContext.request.contextPath}/userController/grantPage?ids=' + ids.join(','),
				buttons : [ {
					text : '授权',
					handler : function() {
						parent.$.modalDialog.openner_dataGrid = dataGrid;//因为授权成功之后，需要刷新这个dataGrid，所以先预定义好
						var f = parent.$.modalDialog.handler.find('#form');
						f.submit();
					}
				} ]
			});
		} else {
			parent.$.messager.show({
				title : '提示',
				msg : '请勾选要授权的记录！'
			});
		}
	}

	function grantFun(id) {
		dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		parent.$.modalDialog({
			title : '用户授权',
			width : 500,
			height : 300,
			href : '${pageContext.request.contextPath}/userController/grantPage?ids=' + id,
			buttons : [ {
				text : '授权',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为授权成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#form');
					f.submit();
				}
			} ]
		});
	}

	function searchFun() {
		dataGrid.datagrid('load', $.serializeObject($('#searchForm')));
	}
	function cleanFun() {
		$('#searchForm input').val('');
		dataGrid.datagrid('load', {});
	}
</script>
</head>
<body>
	<div class="easyui-layout" data-options="fit : true,border : false">
		<div data-options="region:'north',title:'查询条件',border:false,collapsed : true" style="height: 200px; overflow: hidden;">
				<form id="searchForm">
					<table class="table table-hover table-condensed" style="display: none;">
						<tr>
							<th>兑换类型</th>
							<td><input id="userLoginCombobox" name="type"  type="text" data-options="required:true" placeholder="请输入兑换类型" class="easyui-validatebox"   style="height: 29px;"></td>
							<th>用户名称</th>
							<td><input   name="nicknamecp"  type="text" placeholder="请输入用户名称" class="easyui-validatebox"    style="height: 29px;"></td>
						</tr>
						<tr>
							<th>QQ号</th>
							<td><input   name="qq"  type="text" placeholder="请输入QQ号" class="easyui-validatebox"   style="height: 29px;"></td>
							<th>支付宝</th>
							<td><input   name="alipay"  type="text" placeholder="请输入支付宝" class="easyui-validatebox"   style="height: 29px;"></td>
						</tr>
						<tr>
							<th>电话</th>
							<td><input   name="cellPhone"  type="text" placeholder="请输入电话" class="easyui-validatebox"    style="height: 29px;"></td>
							<th>用户ID</th>
							<td><input   name="useridcp"  type="text" placeholder="请输入用户ID" class="easyui-validatebox"    style="height: 29px;"></td>
						 </tr>
						</table>
				</form>
			</div>
			<div data-options="region:'center',border:false">
				<table id="dataGrid"></table>
			</div>
	</div>
	<div id="toolbar" style="display: none;">
		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'brick_add',plain:true" onclick="searchFun();">过滤条件</a><a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'brick_delete',plain:true" onclick="cleanFun();">清空条件</a>
		<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/batchDelete')}">
			<a onclick="batchDeleteFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'delete'">批量删除</a>
		</c:if>
	</div>

	<div id="menu" class="easyui-menu" style="width: 120px; display: none;">
		<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/addPage')}">
			<div onclick="addFun();" data-options="iconCls:'pencil_add'">增加</div>
		</c:if>
		<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/delete')}">
			<div onclick="deleteFun();" data-options="iconCls:'pencil_delete'">删除</div>
		</c:if>
		<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/editPage')}">
			<div onclick="editFun();" data-options="iconCls:'pencil'">编辑</div>
		</c:if>
	</div>
</body>
</html>