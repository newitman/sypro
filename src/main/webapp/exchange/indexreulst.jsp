<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<title>兑换记录管理</title>
<jsp:include page="../inc.jsp"></jsp:include>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/editPage')}">
	<script type="text/javascript">
		$.canEdit = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/delete')}">
	<script type="text/javascript">
		$.canDelete = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/grantPage')}">
	<script type="text/javascript">
		$.canGrant = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/userController/editPwdPage')}">
	<script type="text/javascript">
		$.canEditPwd = true;
	</script>
</c:if>
<script type="text/javascript">
	var dataGrid;
	var userLoginCombobox;
	$(function() {
	  
		dataGrid = $('#dataGrid').datagrid({
			url : '${pageContext.request.contextPath}/texchangeController/dataGrid',
			fit : true,
			fitColumns : true,
			border : false,
			pagination : true,
			idField : 'id',
			pageSize : 10,
			pageList : [ 10, 20, 30, 40, 50 ],
			sortName : 'userid',
			sortOrder : 'asc',
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : false,
			frozenColumns : [[{
				field : 'id',
				title : 'other',
				align : 'center',
				width : 80,
				sortable : true,
				checkbox : true
			},{
				field : 'nicknamecp',
				title : '用户名称',
				align : 'center',
				width :80,
				sortable : true
			} ] ],
			columns : [ [ {
				field : 'useridcp',
				align : 'center',
				title : '用户ID',
				width : 80
			},{
				field : 'cellPhone',
				align : 'center',
				title : '电话',
				width : 80
			}, {
				field : 'alipay',
				title : '支付宝',
				align : 'center',
				width : 80
			}, {
				field : 'qq',
				title : 'QQ',
				align : 'center',
				width : 80
			}, {
				field : 'goods',
				title : '兑换数量',
				align : 'center',
				width : 50,
				sortable : true
			}, {
				field : 'exchangecontent',
				title : '兑换内容',
				align : 'center',
				width : 80
			}, {
				field : 'exchangetime',
				title : '兑换时间',
				align : 'center',
				width : 80,
				sortable : true
			}, {
				field : 'changestatus',
				title : '兑换状态',
				align : 'center',
				width : 80,
				sortable : true,
					formatter : function(value, row, index) {
					var str="";
					if(value == 1){
						str = "<lable style='color:green;font-weight:bold'>审核成功</lable>";
					}else if(value == 0){
					 	str = "<lable style='color:#999999;font-weight:bold'>待审核</lable>";
					}else if(value == 3){
						str = "<lable style='color:red;font-weight:bold'>审核失败</lable>";
					}else{
					
					}
					return str;
				} 
			} ] ],
			toolbar : '#toolbar',
			onLoadSuccess : function() {
				$('#searchForm table').show();
				parent.$.messager.progress('close');
				$(this).datagrid('tooltip');
			},
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll').datagrid('uncheckAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});
	});
	function searchFun() {
		dataGrid.datagrid('load', $.serializeObject($('#searchForm')));
	}
	function cleanFun() {
		$('#searchForm input').val('');
		dataGrid.datagrid('load', {});
	}
</script>
</head>
<body>
	<div class="easyui-layout" data-options="fit : true,border : false">
		<div data-options="region:'north',title:'查询条件',border:false,collapsed : true" style="height: 70px; overflow: hidden;">
				<form id="searchForm">
					<table class="table table-hover table-condensed" style="display: none;">
						<tr>
							<th>兑换类型</th>
							<td>
								<select  name="changestatus" class="easyui-validatebox" >
									<option value="">全部</option>
									<option value="0">待审核</option>
									<option value="1">审核成功</option>
									<option value="3">审核失败</option>
								</select>
							</td>
							<th></th>
							<td></td>
						</tr>
						</table>
				</form>
			</div>
			<div data-options="region:'center',border:false">
				<table id="dataGrid"></table>
			</div>
	</div>
	<div id="toolbar" style="display: none;">
		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'brick_add',plain:true" onclick="searchFun();">过滤条件</a><a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'brick_delete',plain:true" onclick="cleanFun();">清空条件</a>
	</div>
</body>
</html>