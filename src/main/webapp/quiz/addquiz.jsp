<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		parent.$.messager.progress('close');
		
		//$("#txtendTime").datetimepicker({ showSecond: true, timeFormat: 'hh:mm:ss' });// 同时显示时间
		
	});
	
	//   var btime = $("#txtStartTime").datebox("getValue");
	function formatDate(date) {
        return date.getFullYear() + "-" + (date.getMonth() +1) + "-" + date.getDate();
	}
	
	$(function() {
		parent.$.messager.progress('close');
		$('#form').form({
			url : '${pageContext.request.contextPath}/quizmanagerController/publisquiz',
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				var isValid = $(this).form('validate');
				if (!isValid) {
					parent.$.messager.progress('close');
				}
				return isValid;
			},
			success : function(result) {
				parent.$.messager.progress('close');
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为user.jsp页面预定义好了
					parent.$.modalDialog.handler.dialog('close');
				} else {
					parent.$.messager.alert('错误', result.msg, 'error');
				}
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<form id="form" method="post" >
			<table class="table table-hover table-condensed">
				<tr>	
				<th>中奖号</th>
					<td><input name="number" type="text"  class="easyui-numberbox" data-options="required:true" ></td>
				</tr>
				
				<th>金币数量</th>
					<td><input name="gold" type="text"  class="easyui-numberbox" data-options="required:true" ></td>
				</tr>
				<tr>	
					<th>开始时间</th>
					<td>
					<input name="startDate" type="text" id="txtstartDate" class="easyui-datebox" style="width:90px"/>
					<input name="startTime" type="text" id="txstartTime" class="easyui-timespinner" style="width:100px"/>
					</td>
					
				</tr>
				<tr>
					<th>结束时间</th>
					<td>
					<input name="endDate" type="text" id="txtendDate" class="easyui-datebox" style="width:90px"/>
					<input name="endTime" type="text" id="txtendTime" class="easyui-timespinner" style="width:100px"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>